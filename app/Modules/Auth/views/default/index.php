<?= $this->extend('main/layout2'); ?>
<?= $this->section('content'); ?>
<style media="screen">
  body {
    background: #f2f4f8;
  }
</style>
<div class="divtengah">
  <div class="card" style="width:27%;">
    <div class="card-header" style="padding:1px;background:#4F94CD;"></div>

    <?php if (!empty(session()->getFlashdata('msg'))) { ?>
      <div class="alert alert-merah text-danger" id="alertnya">
        <?= session()->getFlashdata('msg'); ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <?php } ?>

    <div class="card-body" style="padding:13px 27px 20px 27px;">
      <img src="<?= $this->mks::base('logo.png'); ?>" class="mt-1 rounded-circle elevation-1" width="100">
      <div class="mt-2" style="font-size:23px;font-weight:bold;margin-top:0px;padding-top:0px;">
        <span class="text-biru">TOKO</span><span style="font-size:17px;" class="text-warning">BUKU</span>
      </div>
      <div class="p-1"></div>
      <form class="form text-left" action="<?= $this->mks::base('auth/proses'); ?>" method="post">
        <label style="color:#888888;">Username</label>
        <input type="text" class="form-control form-control-sm" name="username" placeholder="Username" required autofocus>
        <label style="color:#888888;">Password</label>
        <input type="password" class="form-control form-control-sm" name="password" id="password" placeholder="Password" required>
        <div class="input-group mb-2">
          <input type="checkbox" id="ceklist" onclick="centang()">&ensp;
          <label class="mt-2" style="font-weight:normal;">Show password</label>
        </div>
        <hr>
        <button type="submit" class="btn btn-sm btn-block bg-biru text-light">
          Masuk
        </button>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
  function centang() {
    let cek = document.getElementById("ceklist")
    if (cek.checked === true) {
      document.getElementById('password').type = 'text';
    } else {
      document.getElementById('password').type = 'password';
    }
  }
</script>
<?= $this->endSection(); ?>