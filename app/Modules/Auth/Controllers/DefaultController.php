<?php

namespace App\Modules\Auth\Controllers;

use App\Models\UserLogin;

class DefaultController extends \App\Controllers\BaseController
{

    public function __construct()
    {
        $this->model = new UserLogin();
        $this->session = session();
    }

    public function index()
    {
        return mks()::to('auth/login');
    }

    public function login()
    {
        if (session('mksLogin')) {
            return redirect()->to('');
        }
        return $this->mks::view('index')->mod($this::class, '!');
    }

    public function proses()
    {
        if ($this->mks::$isPost) {
            $username   = htmlspecialchars($this->request->getVar('username'));
            $password   = htmlspecialchars($this->request->getVar('password'));
            $data       = $this->model->where('username', $username)->first();
            if ($data) {
                $pass = $data['password'];
                $verify_pass = password_verify($password, $pass);
                if ($verify_pass && $data['status'] == 1) {
                    $datatambahan = $this->model->getDataTambahan($data['id']);
                    $session_data = [
                        'id'            => $data['id'],
                        'kodeuser'      => $datatambahan['kode'],
                        'username'      => $data['username'],
                        'gudang'        => $data['gudang'],
                        'nama'          => $data['nama'],
                        'level'         => $data['level'],
                        'namalevel'     => $datatambahan['namalevel'],
                        'foto'          => $datatambahan['foto'],
                        'terdaftar'     => $datatambahan['terdaftar'],
                        'mksLogin'      => true
                    ];
                    $this->session->set($session_data);
                    $this->loglogin($data['id']);
                    return mks()::to('');
                } else {
                    $this->session->setFlashdata('msg', '<i class="fas fa-info-circle mr-2"></i>Kata sandi salah');
                    return mks()::to('auth/login');
                }
            } else {
                $this->session->setFlashdata('msg', '<i class="fas fa-info-circle mr-2"></i>Data tidak ditemukan');
                return mks()::to('auth/login');
            }
        }
    }

    private function loglogin($id)
    {
        $model = new \App\Models\Loglogin;
        $uAgen = $this->request->getUserAgent();
        $udevices = $uAgen->isMobile() ? 'Mobile <strong>(' . $uAgen->getMobile() . ')</strong>' : 'Komputer <strong>(' . $uAgen->getPlatform() . ')</strong>';
        $model->insert([
            'ip' => $this->request->getIPAddress(),
            'waktu' => $this->mks::tglIndo(date('Y-m-d')) . ' pukul ' . date('H:i:s'),
            'perangkat' => $udevices . ', Browser <strong>(' . $uAgen->getBrowser() . ')</strong>',
            'user_id' => $id,
        ]);
    }

    public function logout()
    {
        $this->session->destroy();
        return mks()::to('auth/login');
    }
}
