<div class="modal fade" id="tambahdata" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-light" style="padding:5px">
        <h6 class="ml-3 pt-1">Form Tambah Role Akses</h6>
        <button type="button" class="mr-3 close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" enctype="multipart/form-data">
          <div class="col-sm-12">
            <label for="">Role Akses</label>
            <select class="form-control form-control-sm">
              <option selected disabled>--Pilih Role--</option>
              <option>Super Admin</option>
              <option>Karyawan</option>
              <option>Gudang</option>
            </select>
            <label for="">Status</label>
            <select class="form-control form-control-sm">
              <option selected disabled>--Pilih Status--</option>
              <option>Aktif</option>
              <option>Tidak Aktif</option>
            </select>
          </div>
          <div class="col-sm-12 text-right">
            <hr style="margin-top:5px;margin-bottom:10px;">
            <button type="submit" class="btn btn-xs btn-secondary" style="border-radius: 10px;">
              <i class="fa fa-save mr-1"></i>
              Simpan Data
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>