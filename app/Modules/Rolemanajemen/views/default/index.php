<?=$this->extend('main/layout');?>
<?=$this->section('content');?>
<!-- konten disini -->

<section class="content">
  <div class="container-fluid">
    <div class="form-group">
      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-header bg-light" style="padding:1px">
              <div class="row">
                <div class="pl-3 pt-2 mr-5 pr-5">
                  <h6>Data Role Akses</h6>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table id="tabel" class="table table-sm table-striped">
                  <thead class="bg-secondary">
                    <tr>
                      <th class="text-center" style="width:5px;">No</th>
                      <th>Nama Level</th>
                      <th class="text-center" style="width:35px;">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($level as $key => $val) { ?>
                      <tr>
                        <td class="text-center"><?= $key+1 ?></td>
                        <td><?= $val['namalevel'] ?></td>
                        <td class="text-center"><?= $val['status'] == 1 ? '<span class="badge badge-info">Aktif</span>' : '<span class="badge badge-danger">Non Aktif</span>' ?></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include "tambahdata.php"; ?>

<script>
  function tambahdata(){
    $('#tambahdata').modal('show')
  }
</script>
<?=$this->endSection();?>
