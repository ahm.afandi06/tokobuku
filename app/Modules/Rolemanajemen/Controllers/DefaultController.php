<?php namespace App\Modules\Rolemanajemen\Controllers;

use App\Models\MLevel;

class DefaultController extends \App\Controllers\BaseController {

  public function __construct(){
    $this->level = new MLevel();
  }

  public function index(){
    if($this->mks::$isPost){
      // nanti proses login disini
      session()->set(['mksLogin' => true]); // set session sementara
      return mks()::to(''); // <== sementara
    }
    $level = $this->level->getLevel();

    return $this->mks::view('index', [
      'level' => $level,
    ])->mod($this::class, '@');
  }

}