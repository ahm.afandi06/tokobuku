<?php
namespace App\Modules\Aksesmenu\Controllers;

class DefaultController extends \App\Controllers\BaseController {

  public function index(){
    if(session('mksLogin') && session('level') == "1" && session('developer') != ""){
      $qlevel = "select * from m_level where status = 1";
      $levelterpilih = "";
      $zmenu = [];

      $level = $this->request->getVar('level');
      if($level != ""){
        $levelterpilih = $level;
        if($levelterpilih != ""){
          $qmenu = "select
          m.id, d.id as iddetail, m.namamenu, if(d.level is not null, 'Y', 'N') as sudahdiatur
          from z_menu m
          left join z_menudetail d on d.menu_id = m.id and d.level = '".$levelterpilih."'";
          $zmenu = $this->query($qmenu);
        }
      }

      return $this->mks::view('index', [
        'level' => $this->query($qlevel),
        'lvl' => $levelterpilih,
        'zmenu' => $zmenu
      ])
      ->mod($this::class, '@');
    }
    return redirect()->to('auth/login');
  }

  public function popup(){
    if(session('mksLogin') && $this->request->getPost() && session('developer') != ""){
      $kata = $this->request->getPost('kata');
      $sql = "select  d.level, m.* from z_menudetail d
      join z_menu m on m.id = d.menu_id
      where d.status = 1 and m.status = 1 and d.level = '".session('level')."'";
      $qlengkap = $sql;
      $sql .= " and m.namamenu like '%".$kata."%' order by m.parent, m.urutan";
      $data = $this->db->query($sql)->getResultArray();
      $datalengkap = $this->db->query($qlengkap)->getResultArray();

      return json_encode(['status' => true, 'data' => $data, 'datalengkap' => $datalengkap]);
    }
    return json_encode(['status' => false]);
  }

  public function eksekusi(){
    if(session('mksLogin') && $this->request->getPost() && session('developer') != ""){
      $kode = $this->request->getPost('kode');
      $menu_id = $this->request->getPost('menu_id');
      $level = $this->request->getPost('level');
      $iddetail = $this->request->getPost('iddetail');
      $m = new \App\Models\ZMenudetail;

      if($kode == "tambah"){
        $data = [
          'menu_id' => $menu_id,
          'level' => $level,
          'status' => 1
        ];
        $m->insert($data);
      }else if($kode == "hapus"){
        $m->delete(['id' => $iddetail]);
      }
      return json_encode(['status' => true]);
    }
    return json_encode(['status' => false]);
  }

  private function query($sql){
    return $this->db->query($sql)->getResultArray();
  }
}
