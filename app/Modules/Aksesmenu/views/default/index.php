<?=$this->extend('main/layout');?>
<?=$this->section('content');?>
<?php if (session()->get('berhasil') !== null) { ?>
  <div class="alert alert-hijau text-success" id="alertnya">
    <span><?= session()->get('pesannya'); ?></span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
<?php } else if (session()->get('gagal') !== null) { ?>
  <div class="alert alert-merah text-danger" id="alertnya">
    <span><?= session()->get('pesannya'); ?></span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
<?php } ?>
<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col-sm-4">
        <form class="form" action="<?=$this->mks::base('aksesmenu');?>" method="get">
          <strong>&nbsp;Level Pengguna</strong>
          <select class="form-control form-control-sm" id="level" name="level" onchange="this.form.submit()">
            <option value="">-Pilih Level-</option>
          <?php
            foreach($level as $l):
              $selek = $l['id'] == $lvl ? 'selected' : '';
          ?>
            <option value="<?=$l['id'];?>" <?=$selek;?>><?=$l['namalevel'];?></option>
          <?php endforeach; ?>
          </select>
        </form>
      </div>
      <div class="col-sm-8"></div>
      <div class="col-sm-12"><hr></div>
      <div class="col-sm-6">
        <div class="card">
          <div class="card-body card-outline card-primary">
            <table class="table table-sm table-striped table-hover">
              <thead>
                <tr>
                  <th>Nama Menu</th>
                  <th class="text-right">#</th>
                </tr>
              </thead>
              <tbody>
              <?php if(count($zmenu) > 0): foreach($zmenu as $m): ?>
              <?php if($m['sudahdiatur'] == "N"): ?>
                <tr>
                  <td><?=$m['namamenu'];?></td>
                  <td class="text-right">
                    <a href="#" onclick="aksi('<?=$m['id'];?>', '<?=$lvl;?>', '<?=$m['iddetail'];?>', 'tambah')">
                      <i class="fa fa-chevron-circle-right fa-lg text-primary"></i>
                    </a>
                  </td>
                </tr>
              <?php endif; ?>
              <?php endforeach; endif; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card">
          <div class="card-body card-outline card-success">
            <table class="table table-sm table-striped table-hover">
              <thead>
                <tr>
                  <th class="text-left">#</th>
                  <th>Nama Menu</th>
                </tr>
              </thead>
              <tbody>
              <?php if(count($zmenu) > 0): foreach($zmenu as $m): ?>
              <?php if($m['sudahdiatur'] == "Y"): ?>
                <tr>
                  <td class="text-left">
                    <a href="#" onclick="aksi('<?=$m['id'];?>', '<?=$lvl;?>', '<?=$m['iddetail'];?>', 'hapus')">
                      <i class="fa fa-chevron-circle-left fa-lg text-danger"></i>
                    </a>
                  </td>
                  <td><?=$m['namamenu'];?></td>
                </tr>
              <?php endif; ?>
              <?php endforeach; endif; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function aksi(id, idlevel, iddetail, kode){
    var data = [];
    data['kode'] = kode;
    data['menu_id'] = id;
    data['level'] = idlevel;
    data['iddetail'] = iddetail;
    kirim(data, 'aksesmenu/eksekusi', function(obj){
      if(obj.status){
        location.reload()
      }
    })
  }
</script>
<?=$this->endSection();?>
