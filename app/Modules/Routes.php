<?php

namespace App\Modules;

$uri_str = uri_string();

if (strlen($uri_str) > 1) {

    $routes->group('auth', ['namespace' => mks()::NameSpace($uri_str)], function ($routes) {
        $routes->get('/', 'DefaultController::index');
        $routes->get('login', 'DefaultController::login');
        $routes->post('login', 'DefaultController::login');
        $routes->post('proses', 'DefaultController::proses');
        $routes->get('logout', 'DefaultController::logout');
    });

    $routes->group('aksesmenu', ['namespace' => mks()::NameSpace($uri_str)], function ($routes) {
        $routes->get('/', 'DefaultController::index');
        $routes->post('popup', 'DefaultController::popup');
        $routes->post('eksekusi', 'DefaultController::eksekusi');
    });

    // -------------------------------------- master -------------------------------------------
    $routes->group('databuku', ['namespace' => mks()::NameSpace($uri_str)], function ($routes) {
        $routes->get('/', 'DefaultController::index');
        $routes->get('databuku_json', 'DefaultController::databuku_json');
        $routes->post('edit', 'DefaultController::edit');
        $routes->post('update', 'DefaultController::update');
        $routes->post('hapus', 'DefaultController::hapus');
        $routes->post('tambah', 'DefaultController::tambah');
        $routes->post('simpan', 'DefaultController::simpan');
    });

    // -------------------------------------- kasir -------------------------------------------
    $routes->group('kasir', ['namespace' => mks()::NameSpace($uri_str)], function ($routes) {
        $routes->get('create', 'DefaultController::create');
        $routes->post('pilihproduk', 'DefaultController::pilihproduk');
        $routes->post('cekbykode', 'DefaultController::cekbykode');
        $routes->get('getharga', 'DefaultController::getharga');
        $routes->post('simpan_create', 'DefaultController::simpan_create');
        $routes->post('fetch_jenis', 'DefaultController::fetch_jenis');
        $routes->get('cetakpenjualanpdf', 'DefaultController::cetakStrukPdf');
        $routes->get('index', 'DefaultController::index');
        $routes->get('index_json', 'DefaultController::index_json');
        $routes->get('edit/(:any)', 'DefaultController::edit/$1');
        $routes->post('simpan_edit', 'DefaultController::simpan_edit');
        $routes->post('retur', 'DefaultController::retur');
    });

    $routes->group('returpenjualan', ['namespace' => mks()::NameSpace($uri_str)], function ($routes) {
        $routes->get('/', 'DefaultController::index');
        $routes->get('detail/(:any)', 'DefaultController::detail/$1');
        $routes->post('simpanretur', 'DefaultController::simpanretur');
        $routes->post('cetak', 'DefaultController::cetak');
    });

    $routes->group('profil', ['namespace' => mks()::NameSpace($uri_str)], function ($routes) {
        $routes->get('/', 'DefaultController::index');
        $routes->post('ubahpass', 'DefaultController::ubahpass');
    });
}
