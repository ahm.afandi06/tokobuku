<?php echo form_open_multipart('databuku/update', ['autocomplete' => 'off']); ?>
<div class="modal-header">
    <h4 class="modal-title text-lg"><?= strtoupper($title) . ": " . $row['kode'] ?></h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body text-sm">
    <div class="row p-0">
        <input type="hidden" name="id" value="<?= $id ?>">
        <div class="col-md-2">
            <div class="form-group">
                <label>Kode</label>
                <input type="text" name="kode" id="kode" class="form-control form-control-sm text-center" value="<?= $row['kode'] ?>" required />
            </div>
        </div>
        <div class="col-md-10">
            <div class="form-group">
                <label>Judul</label>
                <input type="text" name="judul" id="judul" class="form-control form-control-sm text-left" value="<?= $row['judul'] ?>" required />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Kelas</label>
                <select name="kelas" id="kelas" class="form-control form-control-sm select2bs4">
                    <?php
                    echo "<option value=''>Pilih Kelas</option>";
                    for ($i = 1; $i <= 12; $i++) {
                        if ($i == $row['kelas'])
                            echo "<option value='" . $i . "' selected>" . $i . "</option>";
                        else
                            echo "<option value='" . $i . "'>" . $i . "</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Untuk</label>
                <select name="untuk" id="untuk" class="form-control form-control-sm select2bs4">
                    <option value=""></option>
                    <option value="SISWA" <?= ($row['untuk'] == "SISWA") ? "selected" : ""; ?>>SISWA</option>
                    <option value="GURU" <?= ($row['untuk'] == "GURU") ? "selected" : ""; ?>>GURU</option>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Hal</label>
                <input type="text" name="hal" id="hal" class="form-control form-control-sm text-left" value="<?= $row['hal'] ?>" required />
            </div>
        </div>
        <p class="lead col-md-12 text-bold mt-3 mb-2" style="text-decoration: underline;">#DAFTAR HARGA</p>
        <div class="col-md-2">
            <div class="form-group">
                <label>Zona I</label>
                <input type="text" name="zona1" id="zona1" class="form-control form-control-sm text-right" value="<?= $row['zona1'] ?>" required />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Zona II</label>
                <input type="text" name="zona2" id="zona2" class="form-control form-control-sm text-right" value="<?= $row['zona2'] ?>" required />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Zona III</label>
                <input type="text" name="zona3" id="zona3" class="form-control form-control-sm text-right" value="<?= $row['zona3'] ?>" required />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Zona IV</label>
                <input type="text" name="zona4" id="zona4" class="form-control form-control-sm text-right" value="<?= $row['zona4'] ?>" required />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Zona V/A</label>
                <input type="text" name="zona5a" id="zona5a" class="form-control form-control-sm text-right" value="<?= $row['zona5a'] ?>" required />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Zona V/B</label>
                <input type="text" name="zona5b" id="zona5b" class="form-control form-control-sm text-right" value="<?= $row['zona5b'] ?>" required />
            </div>
        </div>
    </div>
</div>
<div class="modal-footer justify-content-between">
    <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
    <button type="submit" class="btn btn-sm btn-outline-primary"><i class="fa fa-save"></i> Simpan</button>
</div>
<?php echo form_close(); ?>