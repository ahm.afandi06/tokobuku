<?php echo form_open_multipart('databuku/simpan', ['autocomplete' => 'off']); ?>
<div class="modal-header">
    <h4 class="modal-title text-lg"><?= strtoupper($title) ?></h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body text-sm">
    <div class="row p-0">
        <div class="col-md-2">
            <div class="form-group">
                <label>Kode</label>
                <input type="text" name="kode" id="kode" class="form-control form-control-sm text-center" value="" required />
            </div>
        </div>
        <div class="col-md-10">
            <div class="form-group">
                <label>Judul</label>
                <input type="text" name="judul" id="judul" class="form-control form-control-sm text-left" value="" required />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Kelas</label>
                <select name="kelas" id="kelas" class="form-control form-control-sm select2bs4">
                    <?php
                    echo "<option value=''>Pilih Kelas</option>";
                    for ($i = 1; $i <= 12; $i++) {
                        echo "<option value='" . $i . "'>" . $i . "</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Untuk</label>
                <select name="untuk" id="untuk" class="form-control form-control-sm select2bs4">
                    <option value=""></option>
                    <option value="SISWA">SISWA</option>
                    <option value="GURU">GURU</option>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Hal</label>
                <input type="text" name="hal" id="hal" class="form-control form-control-sm text-left" value="" required />
            </div>
        </div>
        <p class="lead col-md-12 text-bold mt-3 mb-2" style="text-decoration: underline;">#DAFTAR HARGA</p>
        <div class="col-md-2">
            <div class="form-group">
                <label>Zona I</label>
                <input type="text" name="zona1" id="zona1" class="form-control form-control-sm text-right" value="" required />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Zona II</label>
                <input type="text" name="zona2" id="zona2" class="form-control form-control-sm text-right" value="" required />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Zona III</label>
                <input type="text" name="zona3" id="zona3" class="form-control form-control-sm text-right" value="" required />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Zona IV</label>
                <input type="text" name="zona4" id="zona4" class="form-control form-control-sm text-right" value="" required />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Zona V/A</label>
                <input type="text" name="zona5a" id="zona5a" class="form-control form-control-sm text-right" value="" required />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Zona V/B</label>
                <input type="text" name="zona5b" id="zona5b" class="form-control form-control-sm text-right" value="" required />
            </div>
        </div>
    </div>
</div>
<div class="modal-footer justify-content-between">
    <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
    <button type="submit" class="btn btn-sm btn-outline-primary"><i class="fa fa-save"></i> Simpan</button>
</div>
<?php echo form_close(); ?>