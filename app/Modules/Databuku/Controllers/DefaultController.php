<?php

namespace App\Modules\Databuku\Controllers;

use Config\Database;
use Ozdemir\Datatables\Datatables;
use Ozdemir\Datatables\DB\Codeigniter4Adapter;

class DefaultController extends \App\Controllers\BaseController
{
    protected $helpers = [];
    private $idgudangtoko = 2;

    public function __construct()
    {
        helper(['form']);
    }

    public function index()
    {
        return $this->mks::view('list')->mod(__CLASS__, '!');
    }

    public function databuku_json()
    {
        $dt = new Datatables(new Codeigniter4Adapter);
        $dt->query("SELECT a.id, a.kode, a.judul, a.kelas, a.untuk, a.hal, a.zona1, a.zona2, a.zona3, a.zona4, a.zona5a, a.zona5b, a.`status` FROM mst_buku a WHERE `status`='1' ORDER BY a.id");

        $dt->add("actions", function ($record) {
            $defaultActions = [];
            $defaultActions[] = '<a href="#" class="btn btn-xs btn-warning" onclick="load_edit(' . $record['id'] . ')" data-toggle="modal" data-target="#modal-lg">
                      <i class="fas fa-edit"></i> 
                    </a>';
            $defaultActions[] = '<a href="#" class="btn btn-xs btn-outline-danger" data-toggle="modal" data-target="#modalhapus" onclick="modalHapus(' . $record['id'] . ')">
                      <i class="fas fa-trash"></i>
                    </a>';

            return "<span class='btn-group'>" . implode("", $defaultActions) . "</span>";
        });

        return $this->response->setJSON($dt->generate()->toJson());
    }

    public function edit()
    {

        $id     = $this->request->getPost('id');

        $model = new \App\Models\Databuku();
        $row = $model->whereBarang($id)->getRowArray();
        $data = [
            'title'             => 'Edit',
            'id'                => $id,
            'row'               => $row,
        ];

        return $this->mks::view('edit', $data)->mod(__CLASS__, '!');
    }

    public function update()
    {

        $id     = $this->request->getPost('id');

        $model = new \App\Models\Databuku();
        $data = [
            'kode'          => $this->request->getPost('kode'),
            'judul'         => $this->request->getPost('judul'),
            'kelas'         => $this->request->getPost('kelas'),
            'untuk'         => $this->request->getPost('untuk'),
            'hal'           => $this->request->getPost('hal'),
            'zona1'         => $this->request->getPost('zona1'),
            'zona2'         => $this->request->getPost('zona2'),
            'zona3'         => $this->request->getPost('zona3'),
            'zona4'         => $this->request->getPost('zona4'),
            'zona5a'        => $this->request->getPost('zona5a'),
            'zona5b'        => $this->request->getPost('zona5b'),
        ];

        $simpan = $model->ubah($data, $id);
        if ($simpan) {
            session()->setFlashdata(['berhasil' => 'ok', 'pesannya' => 'Berhasil mengubah data.']);
        } else {
            session()->setFlashdata(['gagal' => 'ok', 'pesannya' => 'Gagal mengubah data.']);
        }
        return redirect()->to('databuku');
    }

    public function hapus()
    {

        $id     = $this->request->getPost('id');

        $model = new \App\Models\Databuku();
        $data = [
            'status'          => '0',
        ];

        $simpan = $model->ubah($data, $id);
        if ($simpan) {
            session()->setFlashdata(['berhasil' => 'ok', 'pesannya' => 'Berhasil menghapus data.']);
        } else {
            session()->setFlashdata(['gagal' => 'ok', 'pesannya' => 'Gagal menghapus data.']);
        }
        return redirect()->to('databuku');
    }

    public function tambah()
    {
        $data = [
            'title'             => 'Tambah data',
        ];

        return $this->mks::view('tambah', $data)->mod(__CLASS__, '!');
    }

    public function simpan()
    {
        $model = new \App\Models\Databuku();
        $data = [
            'kode'          => $this->request->getPost('kode'),
            'judul'         => $this->request->getPost('judul'),
            'kelas'         => $this->request->getPost('kelas'),
            'untuk'         => $this->request->getPost('untuk'),
            'hal'           => $this->request->getPost('hal'),
            'zona1'         => $this->request->getPost('zona1'),
            'zona2'         => $this->request->getPost('zona2'),
            'zona3'         => $this->request->getPost('zona3'),
            'zona4'         => $this->request->getPost('zona4'),
            'zona5a'        => $this->request->getPost('zona5a'),
            'zona5b'        => $this->request->getPost('zona5b'),
        ];

        $simpan = $model->simpan($data);
        if ($simpan) {
            session()->setFlashdata(['berhasil' => 'ok', 'pesannya' => 'Berhasil menyimpan data.']);
        } else {
            session()->setFlashdata(['gagal' => 'ok', 'pesannya' => 'Gagal menyimpan data.']);
        }
        return redirect()->to('databuku');
    }
}
