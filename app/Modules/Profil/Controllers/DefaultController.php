<?php
namespace App\Modules\Profil\Controllers;

class DefaultController extends \App\Controllers\BaseController {

  public function index(){
    if(session('mksLogin')){
      $qlog = "select * from loglogin where user_id = '".session('id')."' order by id desc limit 1";
      $log = $this->db->query($qlog)->getResultArray();

      if(count($log) < 1){
        $log['ip'] = '-';
        $log['waktu'] = '-';
        $log['perangkat'] = '-';
      }else{
        $log = $log[0];
      }

      return $this->mks::view('index', [
        'log' => $log
      ])
      ->mod($this::class, '@');
    }
    return redirect()->to('auth/login');
  }

  public function ubahpass(){
    if(session('mksLogin') && $this->request->getPost()){
      $m = new \App\Models\UserLogin;
      $pass = password_hash($this->request->getPost('pass'), PASSWORD_BCRYPT);
      $m->ubahdata(['password' => $pass], session('id'));
      session()->setFlashdata(['berhasil' => 'ok', 'pesannya' => 'Berhasil merubah password.']);
      return redirect()->to('profil');
    }
    return redirect()->to('auth/login');
  }
}
