<?=$this->extend('main/layout');?>
<?=$this->section('content');?>
<?php if (session()->get('berhasil') !== null) { ?>
  <div class="alert alert-hijau text-success" id="alertnya">
    <span><?= session()->get('pesannya'); ?></span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
<?php } else if (session()->get('gagal') !== null) { ?>
  <div class="alert alert-merah text-danger" id="alertnya">
    <span><?= session()->get('pesannya'); ?></span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
<?php } ?>
<div class="row">
  <div class="col-sm-12">
    <div class="pb-3 text-muted text-center" style="font-weight:bold;font-size:16px;">
      <span class="text-warning">PRO</span><span class="text-muted">FIL PENGGUNA</span>
    </div>
    <div class="card">
      <div class="card-body pt-3">
        <div class="row">
          <div class="col-sm-4">
            <div class="card">
              <div class="card-header bg-biru" style="padding:1px;"></div>
              <div class="card-body">
                <div class="fotouser text-center" style="margin-bottom:13.5px;margin-top:12px;">
                  <a href="#" class="ubahFoto" id="ubahFoto" data-toggle="modal" data-target="#modalfoto">
                    <img src="<?=$this->mks::base(session('foto'));?>" class="rounded-circle" width="120" height="120" alt="">
                  </a>
                </div>
                <div class="infouser text-center pt-1">
                  <span class="text-araki" style="font-size:20px;"><?=session('nama');?></span>
                  <p class="text-muted"><?=session('namalevel');?></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-8">
            <div class="card">
              <div class="card-header bg-warning" style="padding:1px;"></div>
              <div class="card-body">
                <h5 class="text-muted"><strong>Detail Pengguna</strong></h5>
                <table class="table table-striped">
                  <tr>
                    <td class="text-muted" style="width:30%;">ID</td>
                    <td style="width:3%;">&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><?=session('id');?></td>
                  </tr>
                  <tr>
                    <td class="text-muted">Username</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><?=session('username');?></td>
                  </tr>
                  <tr>
                    <td class="text-muted">Nama Lengkap</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><?=session('nama');?></td>
                  </tr>
                  <tr>
                    <td class="text-muted">Level</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><?=session('namalevel');?></td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-12 pl-0 pr-0">
          <div class="card">
            <div class="card-header bg-danger" style="padding:1px;"></div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-5">
                  <h5 class="text-muted"><strong>Info Akun</strong></h5>
                  <table class="table">
                    <tr>
                      <td class="text-muted" style="width:30%;">Username&nbsp;<span class="font-weight-bold text-danger">*</span></td>
                      <td style="width:3%;">&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                      <td><?=session('username');?></td>
                    </tr>
                    <tr>
                      <td class="text-muted">Password</td>
                      <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                      <td>
                        ********&nbsp;
                        <a href="#" class="badge badge-warning" data-toggle="modal" data-target="#modalpass">
                          <i class="fas fa-pencil-alt"></i>
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td class="text-muted">Terdaftar</td>
                      <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                      <?php $dft = explode(" ", session('terdaftar')); ?>
                      <td><?=$this->mks::tglIndo($dft[0]);?></td>
                    </tr>
                  </table>
                </div>
                <div class="col-sm-7 pl-3">
                  <h5 class="text-muted"><strong>Riwayat LogIn</strong></h5>
                  <table class="table">
                    <tr>
                      <td class="text-muted" style="width:15%;">Alamat IP</td>
                      <td style="width:3%;">&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                      <td><?=$log['ip'];?></td>
                    </tr>
                    <tr>
                      <td class="text-muted">Waktu</td>
                      <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                      <td><?=$log['waktu'];?></td>
                    </tr>
                    <tr>
                      <td class="text-muted">Perangkat</td>
                      <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                      <td><?=$log['perangkat'];?></td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalpass" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <strong>UBAH PASSWORD</strong>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form autocomplete="off" class="form" action="<?=$this->mks::base('profil/ubahpass');?>" method="post" enctype="multipart/form-data">
      <div class="modal-body">
        <div>
          <label for="">Password Baru</label>
          <input oninput="cekpass(this.value, 'pass')" type="password" name="pass" id="pass" class="mb-0 form-control form-control-sm" placeholder="Masukkan password baru" required>
          <small id="tpass" class="text-danger" style="display:none;">password tidak cocok.</small>
        </div>
        <div class="mt-2">
          <label for="">Konfirmasi Password</label>
          <input oninput="cekpass(this.value, 'pass1')" type="password" name="pass1" id="pass1" class="mb-0 form-control form-control-sm" placeholder="Konfirmasi password" required>
          <small id="tpass1" class="text-danger" style="display:none;">password tidak cocok.</small>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-outline-secondary" id="btnpass">
          <i class="fa fa-save mr-1"></i>
          Simpan Password Baru
        </button>
      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  function cekpass(isi, id){
    var sebaliknya = id == "pass1" ? 'pass' : 'pass1';
    if($('#'+id).val() == $('#'+sebaliknya).val() && isi != ""){
      $('#btnpass').prop('type', 'submit')
      $('#btnpass').removeClass('btn-outline-secondary')
      $('#btnpass').addClass('btn-warning')
      $('#t'+id).hide()
      $('#t'+sebaliknya).hide()
    }else{
      $('#btnpass').prop('type', 'button')
      $('#btnpass').removeClass('btn-warning')
      $('#btnpass').addClass('btn-outline-secondary')
      $('#t'+id).show()
      $('#t'+sebaliknya).hide()
    }
  }
</script>

<?=$this->endSection();?>
