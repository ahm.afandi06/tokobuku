<?php
namespace App\Modules\Returpenjualan\Controllers;

use App\Models\Stokbarang;
use App\Models\StokOpname;

class DefaultController extends \App\Controllers\BaseController {

  protected $stokbarang;
  protected $stokopname;

  public function __construct(){
    $this->stokbarang = new Stokbarang();
    $this->stokopname = new StokOpname();
  }

  public function index(){
    if(session('mksLogin')){
      $penjualan = $this->db->query('select a.*, b.namagudang, c.nama 
        from penjualan a
        join m_gudang b on b.idgudang = a.gudang
        join user_login c on c.id = a.kasir
        where a.gudang = '.session('gudang').'
      ')->getResultArray();
      $r_penjualan = $this->db->query('select a.*, b.kodebarang, b.namabarang, b.foto, c.namagudang, d.nama
        from retur_penjualan a
        join m_barang b on b.idbarang = a.id_barang
        join m_gudang c on c.idgudang = a.gudang
        join user_login d on d.id = a.kasir
      ')->getResultArray();

      return $this->mks::view('index', [
        'penjualan' => $penjualan,
        'r_penjualan' => $r_penjualan,
      ])->mod($this::class, '@');
    }
    return redirect()->to('auth/login');
  }

  public function cetak(){
    if($this->mks::$isPost && session('mksLogin')){
      $formatTanggal = $this->request->getPost('tgl');
      $gudang = $this->request->getPost('gudang');
      $filename = 'berkas/'.$formatTanggal.'_laporan_retur_penjualan.pdf';
      $q = "select a.*, d.kodebarang, d.namabarang, e.nama as nama_kasir, b.harga
        from retur_penjualan a
        join penjualan_detail b on b.id = a.id_penjualan_detail
        join penjualan c on c.id = a.id_penjualan
        join m_barang d on d.idbarang = b.barang
        join user_login e on e.id = a.kasir
        where a.gudang = ".$gudang."";
      $q_cari = $this->db->query($q)->getResultArray();

      $view =  $this->mks::view('cetak', ['q_cari' => $q_cari])->mod($this::class, '@');
      $mpdf = new \Mpdf\Mpdf([
        'mode' => 'utf-8',
        'format' => 'A4',
        'orientation' => 'L',
        'margin_left' => 15,
        'margin_right' => 15,
        'margin_top' => 10,
        'margin_bottom' => 10
      ]);
      $mpdf = new \Mpdf\Mpdf([
        'default_font_size' => 9,
        'default_font' => 'arial'
      ]);
      $mpdf->writeHTML($view);
      $mpdf->Output($filename, 'F');

      return json_encode(['dest' => $filename]);
      exit;
    }
    return json_encode(['status' => false]);
  }

  public function detail($id){
    if(session('mksLogin')){
      $q = $this->db->query('select a.*, b.kodebarang, b.namabarang, b.foto, c.namasatuan, d.keterangan, 
      e.no_faktur, f.id as idstok_barang, f.jmlstok
      from penjualan_detail a
      join m_barang b on b.idbarang = a.barang
      join m_satuan c on c.idsatuan = a.satuan
      join m_jenis_sumber_barang d on d.id = a.jenis_sumber_barang
      join penjualan e on e.id = a.penjualan
      join stok_barang f on f.barang = a.barang and milik_gudang = '.session('gudang').'
      where penjualan = '.$id.' ')->getResultArray();
      return $this->mks::view('detail',[
        'data' => $q
      ])->mod($this::class, '@');
    }
    return redirect()->to('auth/login');
  }

  public function simpanretur(){
    if(session('mksLogin') && $this->mks::$isPost){
      // retur penjualan
      $MRetur_pembelian = new \App\Models\Returpenjualan;
      $data_retur_penjualan = [
        'id_penjualan'  => $this->request->getPost('idpenjualan'),
        'id_penjualan_detail'  => $this->request->getPost('idpenjualan_detail'),
        'no_faktur'  => $this->request->getPost('faktur'),
        'gudang'  => session('gudang'),
        'id_barang'  => $this->request->getPost('barang'),
        'kasir'  => session('id'),
        'jenis_sumber_barang'  => $this->request->getPost('jenis_sumber_barang'),
        'sumber_id_pemilik'  => $this->request->getPost('sumber_id_pemilik'),
        'qty'  => $this->request->getPost('qty'),
        'waktu_jual'  => $this->request->getPost('waktu_jual'),
        'waktu_retur'  => date('Y-m-d H:i:s'),
        'status'  => 1
      ];
      $MRetur_pembelian->insert($data_retur_penjualan);

      // penjualan detail
      $data_penjualan_detail = [
        'status'  => 2
      ];
      $this->db->table('penjualan_detail')->update($data_penjualan_detail, ['id' => $this->request->getPost('idpenjualan_detail')]);

      // stok barang
      $stok_saat_ini = (int)$this->request->getPost('stok_awal') + (int)$this->request->getPost('qty');
      $data_stok_barang = [
        'jmlstok' => $stok_saat_ini
      ];
      $s_data_stok_barang = $this->stokbarang->ubah($data_stok_barang, $this->request->getPost('idstok_barang'));

      // stok opname
      $data_stokopname = [
        'milik_gudang'  => session()->get('gudang'),
        'barang'  => $this->request->getPost('barang'),
        'satuan'  => $this->request->getPost('satuan'),
        'jml_transaksi' => $this->request->getPost('qty'),
        'jml_sebelumnya'  => $this->request->getPost('stok_awal'),
        'jml_saat_ini'  => $stok_saat_ini,
        'waktu_transaksi' => date('Y-m-d H:i:s'),
        'sebagai_barang'  => 7,
        'keterangan'  => "Retur penjualan barang",
        'status'  => 1
      ];
      $s_data_stokopname = $this->stokopname->simpan($data_stokopname);

      if($s_data_stok_barang && $s_data_stokopname){
        session()->setFlashdata(['berhasil' => 'ok', 'pesannya' => 'Berhasil menyimpan data.']);
      }else{
        session()->setFlashdata(['gagal' => 'ok', 'pesannya' => 'Gagal menyimpan data.']);
      }
      return json_encode(['status' => true]);
    }
    return redirect()->to('auth/login');
  }

  public function simpanretu2r(){
    if($this->mks::$isPost){
      $id_reqpo = $this->request->getPost('id_reqpo');
      $id_tmp_dis = $this->request->getPost('id_tmp_dis');
      $id_distribusi_detail = $this->request->getPost('id_distribusi_detail');
      $id_tmp_dis_detail = $this->request->getPost('id_tmp_dis_detail');
      $no_faktur_retur = $this->request->getPost('no_faktur_retur');
      $qty_retur = $this->request->getPost('qty_retur');
      $tujuan_gudang = $this->request->getPost('tujuan_gudang');
      $total_kredit_before = $this->request->getPost('total_kredit_before');
      $total_kredit = $this->request->getPost('total_kredit');
      $total_pengembalian = (int)$total_kredit_before - (int)$total_kredit;
      $tujuan_gudang = $this->request->getPost('tujuan_gudang');
      $jmlstok_skrg = $this->request->getPost('jmlstok_skrg');
      $jmlstok_saat_ini = (int)$jmlstok_skrg + $qty_retur;
      $id_stokopname = $this->request->getPost('id_stokopname');
      $idstokbarang2 = $this->request->getPost('idstokbarang2');
      $stokawal = $this->request->getPost('stokawal');
      $stokakhir = (int)$stokawal - (int)$qty_retur;
      $barang = $this->request->getPost('barang');
      $satuan = $this->request->getPost('satuan');

      // retur pembelian
      $MRetur_pembelian = new \App\Models\Returpembelian;
      $data_retur = [
        'tmp_distribusi_id' => $id_tmp_dis,
        'tmp_distribusi_detail_id' => $id_tmp_dis_detail,
        'no_faktur_retur' => $no_faktur_retur,
        'total_pengembalian' => $total_pengembalian,
        'ke_gudang' => $tujuan_gudang,
        'gudang_pengirim' => session()->get('gudang'),
        'qty' => $qty_retur,
        'keterangan' => $this->request->getPost('keterangan_retur'),
        'tanggal_retur' => $this->request->getPost('waktu_retur'),
        'status'  => 1
      ];
      $MRetur_pembelian->insert($data_retur);

      // stok barang
      $data_stok_barang = [
        'jmlstok' => $stokakhir
      ];
      $s_data_stok_barang = $this->stokbarang->ubah($data_stok_barang, $idstokbarang2);

      // stok opname
      $data_stokopname = [
        'milik_gudang'  => session()->get('gudang'),
        'barang'  => $barang,
        'satuan'  => $satuan,
        'jml_transaksi' => $qty_retur,
        'jml_sebelumnya'  => $stokawal,
        'jml_saat_ini'  => $stokakhir,
        'waktu_transaksi' => date('Y-m-d H:i:s'),
        'sebagai_barang'  => 2,
        'keterangan'  => "Barang Keluar",
        'status'  => 1
      ];
      $s_data_stokopname = $this->stokopname->simpan($data_stokopname);

      // Notifikasi
      $MNotifikasi = new \App\Models\Notifikasi;
      $data_notif = [
          'id_user_gudang' => session('gudang'),
          'ke_user_gudang' => $tujuan_gudang,
          'path'    => 'requestretur',
          'id_retur'    => $MRetur_pembelian->getInsertID(),
          'keterangan'  => 'Barang Retur ('.$no_faktur_retur.')',
          'waktu'   => date('Y-m-d H:i:s'),
          'status'  => 1
      ];
      $MNotifikasi->insert($data_notif);

      if($MRetur_pembelian && $data_stok_barang && $data_stokopname && $MNotifikasi){
        session()->setFlashdata(['berhasil' => 'ok', 'pesannya' => 'Berhasil menyimpan data.']);
      }else{
        session()->setFlashdata(['gagal' => 'ok', 'pesannya' => 'Gagal menyimpan data.']);
      }
      return redirect()->to('returpembelian/retur/'.$id_reqpo.'/'.$id_tmp_dis.' ');
    }
  }

}
