<?=$this->extend('main/layout');?>
<?=$this->section('content');?>
<!-- konten disini -->
<div class="col-12">
  <div class="card">
    <div class="card-header">
        <span class="h5 m-auto align-middle">Detail Barang Retur</span>
    </div>        
  </div>
</div>
<div class="col-12">
  <div class="card">
    <div class="card-body">

      <?php if (session()->get('berhasil') !== null) { ?>
          <div class="alert alert-hijau text-success" id="alertnya">
              <span><?= session()->get('pesannya'); ?></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
      <?php } else if (session()->get('gagal') !== null) { ?>
          <div class="alert alert-merah text-danger" id="alertnya">
              <span><?= session()->get('pesannya'); ?></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
      <?php } ?>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header bg-warning" style="padding:1px;"></div>
              <div class="card-body">
                <a href="<?= $this->mks::base('returpenjualan') ?>" class="btn btn-xs btn-secondary mb-3"><i class="fas fa-chevron-circle-left mr-1"></i>Kembali</a>
                <table id="tabel" class="table table-hover">
                  <thead class="alert-kuning">
                    <tr>
                      <th class="text-center">No</th>
                      <th>Foto</th>
                      <th>Kode Barang</th>
                      <th>Nama Barang</th>
                      <th class="text-center">Harga Jual</th>
                      <th class="text-center">Satuan</th>
                      <th class="text-center">Qty</th>
                      <th class="text-center">Waktu Jual</th>
                      <th class="text-center">Keterangan Barang</th>
                      <th class="text-center" style="width:10%;"><i class="fas fa-cog"></i></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($data as $key => $val) { ?>
                      <tr>
                        <td class="text-center align-middle"><?= $key+1 ?></td>
                        <td class="text-center"><img src="<?= $this->mks::base(''.$val['foto'].'') ?>" width="50" height="50"></td>
                        <td class="align-middle"><?= $val['kodebarang'] ?></td>
                        <td class="align-middle"><?= $val['namabarang'] ?></td>
                        <td class="text-center align-middle"><?= $val['harga'] ?></td>
                        <td class="text-center align-middle"><?= $val['namasatuan'] ?></td>
                        <td class="text-center align-middle"><?= $val['qty'] ?></td>
                        <td class="text-center align-middle"><?= $val['waktu_jual'] ?></td>
                        <td class="text-center align-middle"><?= $val['keterangan'] ?></td>
                        <td class="text-center align-middle">
                          <?php if($val['status'] == 1) { ?>
                            <a href="javascript:void(0)" class="btn btn-xs btn-danger" onclick="retur('<?= $val['id'] ?>', '<?= $val['barang'] ?>', 
                              '<?= $val['penjualan'] ?>', '<?= $val['no_faktur'] ?>', '<?= $val['jenis_sumber_barang'] ?>',
                              '<?= $val['sumber_id_pemilik'] ?>', '<?= $val['qty'] ?>', '<?= $val['waktu_jual'] ?>',
                              '<?= $val['idstok_barang'] ?>', '<?= $val['jmlstok'] ?>', '<?= $val['satuan'] ?>')">
                              <i class="fas fa-retweet mr-1"></i>Void barang
                            </a>
                          <?php }else{?>
                            <span class="badge bg-success">Barang telah diretur</span>
                          <?php }?>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  function retur(idpenjualan_detail, barang, idpenjualan, faktur, jenis_sumber_barang, sumber_id_pemilik, qty, waktu_jual, idstok_barang, jmlstok, satuan){
    let data = []
    data['idpenjualan_detail'] = idpenjualan_detail
    data['barang'] = barang
    data['idpenjualan'] = idpenjualan
    data['faktur'] = faktur
    data['jenis_sumber_barang'] = jenis_sumber_barang
    data['sumber_id_pemilik'] = sumber_id_pemilik
    data['qty'] = qty
    data['waktu_jual'] = waktu_jual
    data['idstok_barang'] = idstok_barang
    data['stok_awal'] = jmlstok
    data['satuan'] = satuan

    if(confirm("Apakah anda ingin melakukan void pada barang ini ?")){
      kirim(data, 'returpenjualan/simpanretur', function(obj){
        location.reload()
      })
    }else{
      console.log("Data tetap")
    }
  }
</script>

<?=$this->endSection();?>
