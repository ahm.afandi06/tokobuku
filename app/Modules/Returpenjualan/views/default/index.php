<?=$this->extend('main/layout');?>
<?=$this->section('content');?>
<!-- konten disini -->
<div class="col-12">
  <div class="card">
    <div class="card-header">
      <span class="h5 m-auto align-middle">Data Penjualan</span>
    </div>
  </div>
</div>
<div class="col-12">
  <div class="card">
    <div class="card-header bg-biru" style="padding:1px;"></div>
    <div class="card-body">
      <?php if (session()->get('berhasil') !== null) { ?>
        <div class="alert alert-hijau text-success" id="alertnya">
          <span><?= session()->get('pesannya'); ?></span>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      <?php } else if (session()->get('gagal') !== null) { ?>
        <div class="alert alert-merah text-danger" id="alertnya">
          <span><?= session()->get('pesannya'); ?></span>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      <?php } ?>
      <table class="table table-sm table-bordered" id="tabel">
        <thead class="bg-biru text-light text-center">
          <tr>
            <th class="text-center" style="width:5%;">No</th>
            <th class="text-center">Faktur</th>
            <th class="text-center">Nama Gudang</th>
            <th class="text-center">Nama Kasir</th>
            <th class="text-center">Total Harga</th>
            <th class="text-center">Tunai Bayar</th>
            <th class="text-center">Sisa Bayar</th>
            <th class="text-center">Waktu Jual</th>
            <th class="text-center"><i class="fas fa-cog"></i></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($penjualan as $key => $p) { ?>
            <tr>
              <td class="text-center" style="width:5px;"><?= $key+1 ?></td>
              <td class="text-center"><?= $p['no_faktur'] ?></td>
              <td class="text-center"><?= $p['namagudang'] ?></td>
              <td class="text-center"><?= $p['nama'] ?></td>
              <td class="text-center"><?= $p['total'] ?></td>
              <td class="text-center"><?= $p['tunai_bayar'] ?></td>
              <td class="text-center"><?= $p['sisa_bayar'] ?></td>
              <td class="text-center"><?= $p['waktu_jual'] ?></td>
              <td class="text-center" style="width:10%">
                <a href="<?= $this->mks::base('returpenjualan/detail/'.$p['id']) ?>" class="btn btn-xs btn-warning"><i class="fas fa-info-circle mr-1"></i>Detail penjualan</a>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<hr>
<div class="col-12">
  <div class="card">
    <div class="card-header">
      <span class="h5 m-auto align-middle">Data Retur Penjualan</span>
    </div>
  </div>
</div>
<div class="col-12">
  <div class="card">
    <div class="card-header bg-merah" style="padding:1px;"></div>
    <div class="card-body">
      <a href="javascript:void(0)" class="btn btn-xs btn-outline-danger mb-2" onclick="cetak()"><i class="fa fa-file-pdf mr-1"></i>Cetak Retur Penjualan</a>
      <table class="table table-sm table-bordered" id="tabel2">
        <thead class="bg-merah text-light text-center">
          <tr>
            <th class="text-center" style="width:5%;">No</th>
            <th class="text-center">Faktur</th>
            <th class="text-center">Kode Barang</th>
            <th class="text-center">Nama Barang</th>
            <th class="text-center">Nama Gudang</th>
            <th class="text-center">Nama Kasir</th>
            <th class="text-center">Qty Retur</th>
            <th class="text-center">Waktu Retur</th>
            <th class="text-center">Foto</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($r_penjualan as $key => $rp) { ?>
            <tr>
              <td class="text-center align-middle" style="width:5px;"><?= $key+1 ?></td>
              <td class="text-center align-middle"><?= $rp['no_faktur'] ?></td>
              <td class="text-center align-middle"><?= $rp['kodebarang'] ?></td>
              <td class="text-center align-middle"><?= $rp['namabarang'] ?></td>
              <td class="text-center align-middle"><?= $rp['namagudang'] ?></td>
              <td class="text-center align-middle"><?= $rp['nama'] ?></td>
              <td class="text-center align-middle"><?= $rp['qty'] ?></td>
              <td class="text-center align-middle"><?= $rp['waktu_retur'] ?></td>
              <td class="text-center"><img src="<?= $this->mks::base(''.$rp['foto'].'') ?>" width="50" height="50"></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="overlay-wrapper" id="loading" style="display:none;">
  <div class="overlay"><i class="fas fa-3x fa-sync-alt fa-spin"></i><div class="text-bold pt-2 ml-2">Loading...</div></div>
</div>

<script>
  function cetak(){
    let base = "<?= $this->mks::base('') ?>"
    let tgl = "<?= date('YmdHis') ?>"
    let form = new FormData()
    let gudang = "<?= session('gudang') ?>"

    form.append("tgl", tgl)
    form.append("gudang", gudang)

    $.ajax({
      url: base + 'returpenjualan/cetak',
      type: "POST",
      data: form,
      beforeSend: function(){
        $("#loading").show()
      },
      complete: function(){
        $("#loading").hide()
      },
      processData: false,
      contentType: false,
      success: function(hasil) {
        window.open(base+'berkas/'+tgl+'_laporan_retur_penjualan.pdf')
      },
      error: function(error){
        alert("error");
        console.error(error);
      }
    })
  }
</script>

<?=$this->endSection();?>
