<?=$this->extend('main/layout');?>
<?=$this->section('content');?>
<div class="col-12">
  <div class="card">
    <div class="card-header">
      <span class="h5 m-auto align-middle">Detail Barang</span>
    </div>
  </div>
</div>
<div class="col-12">
  <div class="card">
    <div class="card-body">

    <?php if (session()->get('berhasil') !== null) { ?>
        <div class="alert alert-hijau text-success" id="alertnya">
            <span><?= session()->get('pesannya'); ?></span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php } else if (session()->get('gagal') !== null) { ?>
        <div class="alert alert-merah text-danger" id="alertnya">
            <span><?= session()->get('pesannya'); ?></span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php } ?>

    <form action="<?= $this->mks::base('returpembelian/simpanretur') ?>" method="POST" role="form" enctype="multipart/form-data">
      <div class="row">
        <div class="col-6">
          <div class="card">
            <div class="card-header bg-info" style="padding:1px;"></div>
            <div class="card-body">
              <div class="col-12">
                <label>Nomor Request PO</label>
                <input type="text" class="form-control form-control-sm" value="<?= $data['no_req_po'] ?>" readonly>
              </div>
              <div class="col-12">
                <label>Nomor Surat Jalan</label>
                <input type="text" class="form-control form-control-sm" value="<?= $data['surat_jalan'] ?>" readonly>
              </div>
              <div class="col-12">
                <div class="row">
                  <div class="col-6">
                    <label>Nomor Faktur Terima</label>
                    <input type="text" class="form-control form-control-sm" name="no_faktur_terima" value="<?= $data['surat_jalan'] ?>-FT<?= session()->get('id') ?>" readonly>
                  </div>
                  <div class="col-6">
                    <label>Nomor Faktur Retur</label>
                    <input type="text" class="form-control form-control-sm" name="no_faktur_retur" value="<?= $data['surat_jalan'] ?>-RT<?= session()->get('id') ?>" readonly>
                  </div>
                </div>
              </div>
              <div class="col-12">
                <div class="row">
                  <div class="col-6">
                    <label>Tanggal Kirim</label>
                    <input type="text" class="form-control form-control-sm" value="<?= $data['tgl_do'] ?>" readonly>
                  </div>
                  <div class="col-6">
                    <label>Waktu Terima</label>
                    <input type="text" class="form-control form-control form-control-sm" name="waktu_terima" value="<?= $data['waktu_terima'] ?>" readonly>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="card">
            <div class="card-header bg-success" style="padding:1px;"></div>
            <div class="card-body">
              <div class="row">

                <div class="col-12">
                  <label>Qty Request</label>
                  <input type="text" class="form-control form-control-sm" id="qty_request" value="<?= $data['qty_req'] ?>" readonly>
                </div>
                <div class="col-12">
                  <label>Harga Pokok</label>
                  <input type="text" class="form-control form-control-sm" id="harga_pokok" value="<?= $data['harga_pokok'] ?>" readonly>
                </div>
                <div class="col-12">
                  <label>Harga Total</label>
                  <input type="text" class="form-control form-control-sm" value="<?= $data['harga_total'] ?>" readonly>
                </div>
                <div class="col-12">

                </div>
              </div>
              <div class="row">
                <div class="col-6">
                  <label>After Qty</label>
                  <input type="text" class="form-control form-control-sm" id="qty_after" name="qty_terima" style="background-color: #FFFEC6;" readonly>
                </div>
                <div class="col-6">
                  <label>Before Qty</label>
                  <input type="text" class="form-control form-control-sm" id="qty_before" value="<?= $data['qty_req'] ?>" readonly>
                </div>
                  <!-- <label>PPN Persen</label> -->
                  <input type="text" class="form-control form-control-sm" id="ppn_persen" value="<?= $data['ppn_persen'] ?>" hidden>
                <div class="col-6">
                  <!-- <label>PPN Nominal</label> -->
                  <input type="text" class="form-control form-control-sm" id="ppn_nominal" value="<?= $data['ppn_nominal'] ?>" hidden>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div><hr>
      <div class="col-12">
        <div class="card">
          <div class="card-header bg-danger" style="padding:1px;"></div>
          <div class="card-body">
            <div class="row">
              <div class="col-6">
                <div class="row">
                  <div class="col-6">
                    <label>Qty Retur</label>
                    <input type="number" min="1" max="<?= $data['qty_req'] ?>" class="form-control form-control-sm" id="qty_retur" name="qty_retur" autofocus>
                  </div>
                  <div class="col-6">
                    <label>Waktu Retur</label>
                    <input type="date" class="form-control form-control form-control-sm" value="<?= date('Y-m-d') ?>" id="waktu_retur" name="waktu_retur" required>
                  </div>
                  <div class="col-12">
                    <label>Keterangan Retur</label>
                    <textarea class="form-control form-control-xs" name="keterangan_retur"></textarea>
                  </div>
                </div>
                <div class="row">
                  <div class="col-7 mt-2">
                    <span class="badge bg-danger">*Gunakan Tab untuk Total Kredit</span>
                  </div>
                  <div class="col-5 mt-2 text-right">
                    <small>NB: nominal dalam satuan rupiah</small>
                  </div>
                  <!-- SEMENTARA -->
                  <!-- <div class="col-4 mt-2">
                    < ?php if($data['ppn_persen'] == 0){ ?>
                      <div class="form-check text-right">
                        <input class="form-check-input" type="checkbox" value="12" id="myCheckbox" onclick="ppn()">
                        <label class="form-check-label" for="myCheckbox">
                          *Barang Kena PPN
                        </label>
                      </div>
                    < ?php }else{ ?>
                      <div class="form-check text-right">
                        <input class="form-check-input" type="checkbox" value="< ?= $data['ppn_persen'] ?>" id="myCheckbox" checked disabled>
                        <label class="form-check-label" for="myCheckbox">
                          *Barang Kena PPN
                        </label>
                      </div>
                    < ?php } ?>
                  </div> -->
                  <!-- END -->
                </div>
              </div>
              <div class="col-6">
                <!-- <label>Harga Total Datang</label> -->
                <input type="text" class="form-control form-control-sm" name="total_kredit_before" value="<?= $data['total_kredit'] ?>" readonly hidden>
                <label>Total Kredit</label>
                <input type="text" class="form-control form-control-sm" id="total_kredit" name="total_kredit" onfocus="eronly('#total_kredit')">
                <textarea class="form-control form-control-xs mt-3" id="nominal" readonly style="font-size: 15px; height:85px;"></textarea>
              </div>
            </div>
          </div>
        </div>
        <input type="text" class="form-control" name="id_reqpo" value="<?= $data['id_reqpo'] ?>" hidden>
        <input type="text" class="form-control" name="id_tmp_dis" value="<?= $data['id_tmp_dis'] ?>" hidden>
        <input type="text" class="form-control" name="id_tmp_dis_detail" id="id_tmp_dis_detail" value="<?= $data['id_tmp_dis_detail'] ?>" hidden>
        <input type="text" class="form-control" name="id_distribusi_detail" id="id_distribusi_detail" value="<?= $data['id_distribusi_detail'] ?>" hidden>
        <input type="text" class="form-control" name="id_distribusi" value="<?= $data['id_distribusi'] ?>" hidden>
        <input type="text" class="form-control" name="id_stokbarang" id="id_stokbarang" value="<?= $data['id_stokbarang'] ?>" hidden>
        <input type="text" class="form-control" name="tujuan_gudang" value="<?= $data['gudang_pengirim'] ?>" hidden>
        <input type="text" class="form-control" name="idstokbarang2" value="<?= $data['idstokbarang2'] ?>" hidden>
        <input type="text" class="form-control" name="stokawal" value="<?= $data['jmlstok'] ?>" hidden>
        <input type="text" class="form-control" name="barang" value="<?= $data['barang'] ?>" hidden>
        <input type="text" class="form-control" name="satuan" value="<?= $data['satuan'] ?>" hidden>
        <div class="btn-group">
          <a class="btn btn-secondary btn-xs mr-2" href="javascript:history.back()"><i class="fas fa-arrow-alt-circle-left mr-1"></i>Kembali</a>
          <button disabled type="submit" class="btn btn-primary btn-xs mr-2" id="btn-simpan-retur"><i class="fas fa-save mr-1"></i>Simpan</button>
        </div>
      </div>
    </form>
    </div>
  </div>
</div>

<script>
  function penyebut(nilai) {
    nilai = Math.abs(nilai);
    var huruf = ["", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"];
    var temp = "";
    if (nilai < 12) {
      temp = " " + huruf[nilai];
    } else if (nilai < 20) {
      temp = penyebut(nilai - 10) + " belas";
    } else if (nilai < 100) {
      temp = penyebut(Math.floor(nilai / 10)) + " puluh" + penyebut(nilai % 10);
    } else if (nilai < 200) {
      temp = " seratus" + penyebut(nilai - 100);
    } else if (nilai < 1000) {
      temp = penyebut(Math.floor(nilai / 100)) + " ratus" + penyebut(nilai % 100);
    } else if (nilai < 2000) {
      temp = " seribu" + penyebut(nilai - 1000);
    } else if (nilai < 1000000) {
      temp = penyebut(Math.floor(nilai / 1000)) + " ribu" + penyebut(nilai % 1000);
    } else if (nilai < 1000000000) {
      temp = penyebut(Math.floor(nilai / 1000000)) + " juta" + penyebut(nilai % 1000000);
    } else if (nilai < 1000000000000) {
      temp = penyebut(Math.floor(nilai / 1000000000)) + " milyar" + penyebut(nilai % 1000000000);
    } else if (nilai < 1000000000000000) {
      temp = penyebut(Math.floor(nilai / 1000000000000)) + " trilyun" + penyebut(nilai % 1000000000000);
    }
    return temp;
  }

  function terbilang(nilai) {
    if(nilai < 0) {
      var hasil = "minus " + penyebut(nilai).trim();
    } else {
      var hasil = penyebut(nilai).trim();
    }
    return hasil + " rupiah";
  }

  $("#qty_retur").on("keydown keyup",  function(event) {
    if(event.keyCode == 9 || event.keyCode == 13){
      let harga_pokok = $('#harga_pokok').val()
      let qty_terima = $('#qty_terima').val()
      let ppn_persen = $('#ppn_persen').val()
      let qty_before = $('#qty_before').val()
      let qty_after = $('#qty_after').val()
      let qty_retur = $('#qty_retur').val()
      let total_kredit_before = $('#total_kredit_before').val()
      let after = parseInt(qty_before) - parseInt(qty_retur)
      let total_harga = parseInt(harga_pokok) * parseInt(after == 0 ? qty_retur : after)
      let ppn = (harga_pokok * (ppn_persen / 100))

      if(parseInt(qty_retur) > parseInt(qty_before)){
        alert('Jumlah barang retur harus kurang dari sama dengan barang yang diterima')
        location.reload()
      }else{
        $('#qty_after').val(parseInt(after))
        $('#harga_total_datang').val(parseInt(total_harga) + parseInt(ppn_persen <= 0 ? 0 : ppn))
        $('#total_kredit').val(parseInt(total_harga) + parseInt(ppn_persen <= 0 ? 0 : ppn))
        $('#nominal').val(terbilang($('#total_kredit').val()))
      }
    }
  })
</script>
<script>
  function ppn(){
    let ppn = $('#myCheckbox').val()
    let id_distribusi_detail = $('#id_distribusi_detail').val()
    let id_tmp_dis_detail = $('#id_tmp_dis_detail').val()
    let id_stokbarang = $('#id_stokbarang').val()
    let harga_pokok = $('#harga_pokok').val()
    let qty_request = $('#qty_request').val()
    let data = []
    data['ppn'] = ppn
    data['id_distribusi_detail'] = id_distribusi_detail
    data['id_tmp_dis_detail'] = id_tmp_dis_detail
    data['id_stokbarang'] = id_stokbarang
    data['harga_pokok'] = harga_pokok
    data['qty_request'] = qty_request

    kirim(data, 'barangmasuk/ubahppn', function(obj){
      location.reload()
    })
  }

  $('#myCheckbox').on('change', function(){
    if (checkbox.checked) {
      checkbox.disabled = true;
    } else {
      checkbox.disabled = false;
    }
  })

  $(document).on("change", "#qty_retur", function(){
    let qty_after = $("#qty_after").val();
    let total_kredit = $("#total_kredit").val();
    if(Number(qty_after) != 0 && Number(total_kredit) != 0){
      $("#btn-simpan-retur").prop("disabled", false);
    }
  });

  // const checkbox = document.getElementById("myCheckbox");
  // checkbox.addEventListener("change", function() {
  //   if (checkbox.checked) {
  //     checkbox.disabled = true;
  //   } else {
  //     checkbox.disabled = false;
  //   }
  // })
</script>

<?=$this->endSection();?>
