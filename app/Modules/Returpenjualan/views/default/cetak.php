<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Preview Retur Penjualan</title>
  </head>
  <body>
    <div style="text-align: center; ">
      <h2 style="font-weight:bold">Data Retur Penjualan</h2>
    </div>
    <table border="1" class="table table-striped table-bordered" style="border-collapse: collapse;" cellpadding="5" width="100%">
      <thead class="bg-biru text-light">
        <tr>
          <th style="text-align: center;">No</th>
          <th style="text-align: center;">Tanggal Transaksi</th>
          <th style="text-align: center;">Tanggal Retur</th>
          <th>Kode Barang</th>
          <th>Nama Barang</th>
          <th>Harga Barang</th>
          <th>Qty Retur</th>
          <th>Kasir</th>
        </tr>
      </thead>
      <tbody>
        <?php
        ini_set('pcre.backtrack_limit', '10000000');
        ini_set('max_execution_time', 300);
        foreach($q_cari as $key => $val) { ?>
          <tr>
            <td style="text-align: center;"><?= $key+1 ?></td>
            <td style="text-align: center;"><?= $this->mks::tglIndo($val['waktu_jual']) ?></td>
            <td style="text-align: center;"><?= $this->mks::tglIndo($val['waktu_retur']) ?></td>
            <td><?= $val['kodebarang'] ?></td>
            <td><?= $val['namabarang'] ?></td>
            <td><?= $this->mks::rupiah($val['harga']) ?></td>
            <td style="text-align: center;"><?= $val['qty'] ?></td>
            <td><?= $val['nama_kasir'] ?></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </body>
</html>