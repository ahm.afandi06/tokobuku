<?=$this->extend('main/layout');?>
<?=$this->section('content');?>
<!-- konten disini -->
<div class="col-12">
  <div class="card">
    <div class="card-header">
        <span class="h5 m-auto align-middle">Data Barang Retur</span>
    </div>        
  </div>
</div>
<div class="col-12">
  <div class="card">
    <div class="card-body">

      <?php if (session()->get('berhasil') !== null) { ?>
          <div class="alert alert-hijau text-success" id="alertnya">
              <span><?= session()->get('pesannya'); ?></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
      <?php } else if (session()->get('gagal') !== null) { ?>
          <div class="alert alert-merah text-danger" id="alertnya">
              <span><?= session()->get('pesannya'); ?></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
      <?php } ?>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header bg-warning" style="padding:1px;"></div>
              <div class="card-body">
                <table id="tabel" class="table table-hover">
                  <thead class="alert-kuning">
                    <tr>
                      <th class="text-center">No</th>
                      <th>Kode Barang</th>
                      <th>Nama Barang</th>
                      <th class="text-center">Harga Total</th>
                      <th class="text-center">Qty Request</th>
                      <th class="text-center">Qty Retur</th>
                      <th class="text-center">Keterangan</th>
                      <th class="text-center" style="width:10%;"><i class="fas fa-cog"></i></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($data as $key => $val) { ?>
                      <tr>
                        <td class="text-center"><?= $key+1 ?></td>
                        <td><?= $val['kdbarang'] ?></td>
                        <td><?= $val['namabarang'] ?></td>
                        <td class="text-center"><?= $val['harga_total'] ?></td>
                        <td class="text-center"><?= $val['qty_req'] ?></td>
                        <td class="text-center"><?= $val['qty_retur'] == null ? '-' : '<span class="badge bg-warning">'.$val['qty_retur'].'</span> ' ?></td>
                        <td class="text-center"><?= $val['keterangan'] == null ? '-' : $val['keterangan'] ?></td>
                        <td class="text-center" style="width:10%;">
                          <?php if($val['status_retur'] == null) { ?>
                            <a href="<?= $this->mks::base('returpembelian/detailbarang/'.$val['id_reqpo'].'/'.$val['id_tmp_dis'].'/'.$val['kdbarang']) ?>" class="btn badge bg-warning"><i class="fas fa-info-circle mr-1"></i>Detail Barang</a>
                          <?php }elseif($val['status_retur'] == 1){ ?>
                            <span class="badge bg-danger"><i class="fas fa-people-carry mr-1"></i>Barang Diretur</span>
                          <?php }elseif($val['status_retur'] == 2) { ?>
                            <span class="badge bg-success"><i class="fas fa-people-carry mr-1"></i>Barang Diterima Gudang</span>
                          <?php } ?>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $("#qty_terima").on("keydown", function(event) {
    if (event.keyCode === 9) {
      let hargapokok = $('#harga_pokok').val()
      let qtyterima = $('#qty_terima').val()
      let ppn_persen = $('#ppn_persen').val()
      let total_harga = hargapokok * qtyterima
      let tppn = (hargapokok * qtyterima) * (ppn_persen <= 0 ? 1 : ppn_persen / 100)

      $('#ppn_nominal').val(ppn_persen <= 0 ? 0 : tppn)      
      $('#total_harga').val(total_harga)
      $('#total_kredit').val(parseInt(total_harga) + parseInt(ppn_persen <= 0 ? 0 : tppn))
    }
  })
</script>

<?=$this->endSection();?>
