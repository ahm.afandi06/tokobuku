<?= $this->extend('main/layout'); ?>
<?= $this->section('content'); ?>
<style>
    .form-control {
        font-size: 12px;
        margin-bottom: 3px;
    }

    .form-table {
        font-size: 14px;
        border-radius: unset;
        margin: -6px -4px px;
        border: 0px;
        background-image: none;
        background-color: white;
    }
</style>
<?php if (session()->get('berhasil') !== null) { ?>
    <div class="alert alert-hijau text-success" id="alertnya">
        <span><?= session()->get('pesannya'); ?></span>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php } else if (session()->get('gagal') !== null) { ?>
    <div class="alert alert-merah text-danger" id="alertnya">
        <span><?= session()->get('pesannya'); ?></span>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php } ?>
<!-- konten disini -->
<form id="formKasirSimpan" action="<?= base_url('kasir/simpan_edit') ?>" method="post">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <!-- <div class="card-header">
                            <span class="h5 m-auto align-middle">K A S I R
                        </div> -->
                        <div class="card-body p-2">
                            <?php if (!empty($errors)) : ?>
                                <div class="alert alert-danger">
                                    <ul class="m-0 px-3">
                                        <?php foreach (array_values($errors) as $error) : ?>
                                            <li><?= $error ?></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card card-outline card-info" id="cardgaris">
                                        <!-- <div class="card-header" style="padding-top: 5px;padding-bottom: 0px;">
                                            <label style="margin-bottom: 1px;">TOTAL:</label>
                                        </div> -->
                                        <div class="card-body pl-1 pr-1" style="margin-bottom: -16px;">
                                            <div class="row">
                                                <div class="col-sm-2 pr-0" style="margin-top: -15px;">
                                                    <div>
                                                        <small class="float-sm-left"><strong>DATA PENJUALAN</strong>
                                                        </small><small class="float-sm-right">&nbsp;</small>
                                                    </div>
                                                    <br>
                                                    <hr class="mb-2 mt-0">
                                                    <div>
                                                        <small class="float-sm-left"><strong>No. Transaksi</strong>
                                                        </small><small class="float-sm-right"><?= $penjualan['no_faktur'] ?></small>
                                                        <input type="hidden" name="no_faktur" id="no_faktur" value="<?= $penjualan['no_faktur'] ?>">
                                                        <input type="hidden" name="id" id="id" value="<?= $id ?>">
                                                    </div>
                                                    <br>
                                                    <hr class="mb-2 mt-0">
                                                    <div>
                                                        <small class="float-sm-left"><strong>Tanggal</strong>
                                                        </small><small class="float-sm-right"><?= $penjualan['tanggal'] ?></small>
                                                    </div>
                                                    <br>
                                                    <hr class="mb-2 mt-0">
                                                </div>
                                                <div class="col-md-10" style="margin-top: -15px;">
                                                    <input type="text" name="total" id="total" class="form-control form-control-lg text-right" style="font-size: 76px;height: 80px;" readonly placeholder="0">
                                                    <input type="hidden" id="totalxxx">
                                                </div>
                                                <div class="col-sm-12" style="padding:2.5px;"></div>
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <label class="mb-1">Nama <strong>(F2)</strong></label> <br>
                                                            <input type="text" name="nama_customer" id="nama_customer" class="form-control form-control-sm" value="<?= $penjualan['pelanggan'] ?>" required>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label class="mb-1">Alamat</label> <br>
                                                            <input type="text" name="alamat" id="alamat" class="form-control form-control-sm" value="<?= $penjualan['alamat'] ?>" required>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <label class="mb-1">Kelas</label> <br>
                                                            <select name="kelas" id="kelas" class="form-control form-control-sm" required>
                                                                <option value="">Pilih Kelas</option>
                                                                <?php for ($i = 1; $i <= 12; $i++) {
                                                                    echo "<option value='" . $i . "'>" . $this->mks->numberToRoman($i) . "</option>";
                                                                } ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <label class="mb-1">Jenis</label> <br>
                                                            <select name="jenis" id="jenis" class="form-control form-control-sm" required>
                                                                <option value="">Pilih Jenis</option>
                                                                <option value="GURU">GURU</option>
                                                                <option value="SISWA">SISWA</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <label class="mb-1">Zona</label> <br>
                                                            <select name="zona" id="zona" class="form-control form-control-sm" disabled>
                                                                <option value="zona1" <?= ($penjualan['zona'] == "zona1") ? "selected" : ""; ?>>ZONA I</option>
                                                                <option value="zona2" <?= ($penjualan['zona'] == "zona2") ? "selected" : ""; ?>>ZONA II</option>
                                                                <option value="zona3" <?= ($penjualan['zona'] == "zona3") ? "selected" : ""; ?>>ZONA III</option>
                                                                <option value="zona4" <?= ($penjualan['zona'] == "zona4") ? "selected" : ""; ?>>ZONA IV</option>
                                                                <option value="zona5a" <?= ($penjualan['zona'] == "zona5a") ? "selected" : ""; ?>>ZONA V/A</option>
                                                                <option value="zona5b" <?= ($penjualan['zona'] == "zona5b") ? "selected" : ""; ?>>ZONA V/B</option>
                                                            </select>
                                                            <input type="hidden" name="zonatext" id="zonatext" value="<?= $penjualan['zona'] ?>">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label class="mb-1">Barang <strong>(ins)</strong></label>
                                                            <input type="text" name="caribarang" id="caribarang" class="form-control form-control-sm">
                                                            <input type="hidden" name="cari_nama_produk" id="cari_nama_produk" class="enterable form-control form-control-sm nama_produk produkx${index}">
                                                            <input type="hidden" name="cari_idbarang" id="cari_idbarang" class="enterable form-control form-control-sm idbarang">
                                                            <input type="hidden" name="cari_idsatuan" id="cari_idsatuan" class="enterable form-control form-control-sm idsatuan">
                                                            <input type="hidden" name="cari_kode_barang" id="cari_kode_barang" class="enterable form-control form-control-sm">
                                                            <input type="hidden" name="cari_harga" id="cari_harga" class="enterable form-control form-control-sm cari_harga">
                                                            <input type="hidden" name="cari_qty" id="cari_qty" class="enterable form-control form-control-sm cari_qty">
                                                            <input type="hidden" name="cari_ppnpersen" id="cari_ppnpersen" class="enterable form-control form-control-sm cari_ppnpersen">
                                                            <input type="hidden" name="cari_ppn" id="cari_ppn" class="enterable form-control form-control-sm cari_ppn">
                                                            <input type="hidden" name="cari_milikgudang" id="cari_milikgudang" class="enterable form-control form-control-sm cari_milikgudang">
                                                            <input type="hidden" name="cari_jenis_sumber_barang" id="cari_jenis_sumber_barang" class="enterable form-control form-control-sm cari_jenis_sumber_barang">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <!-- </div> -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card card-outline card-warning">
                                        <table class="table table-bordered table-hover table-striped table-condensed table-sm m-0" id="data-item">
                                            <thead class="bg-biru text-light">
                                                <tr>
                                                    <th width="100px" class="text-center">Kelas</th>
                                                    <th width="100px" class="text-center">Code</th>
                                                    <th width="100px" class="text-center">Jenis</th>
                                                    <th class="text-center">Judul Buku</th>
                                                    <th width="150px" class="text-center">HET</th>
                                                    <th width="100px" class="text-center">Qty</th>
                                                    <th width="100px" class="text-center">Disc(%)</th>
                                                    <th width="150px" class="text-center">Harga (Disc)</th>
                                                    <th width="150px" class="text-center">JUMLAH</th>
                                                    <th width="35px" class="text-center">#</th>
                                                </tr>
                                            </thead>
                                            <tbody id="listitemx">
                                                <?php
                                                $rr = 0;
                                                foreach ($penjualandetail as $r) {
                                                    $rr++; ?>
                                                    <tr id="baris<?= $rr ?>">
                                                        <td class="p-0 m-0"><input type="text" name="items[<?= ($rr - 1) ?>][kelasx]" id="kelasx" value="<?= $this->mks->numberToRoman($r['kelas']) ?>" class="enterable text-right form-control text-center form-control-sm form-table kelasx kelasx-input-barangnya" style="background-color:white;" readonly></td>
                                                        <td class="p-0 m-0"><input type="text" name="items[<?= ($rr - 1) ?>][kode_barang]" id="kode_barang" value="<?= $r['barang'] ?>" class="enterable text-center form-control form-control-sm form-table kelas kode_barang kode-input-barangnya" style="background-color:white;" readonly></td>
                                                        <td class="p-0 m-0"><input type="tezt" name="items[<?= ($rr - 1) ?>][jenis]" id="jenis" value="<?= $r['untuk'] ?>" class="enterable text-center form-control form-control-sm form-table jenis jenis-input-barangnya" style="background-color:white;" readonly></td>
                                                        <td class="p-0 m-0">
                                                            <input type="hidden" name="items[<?= ($rr - 1) ?>][idbarang]" id="idbarang" class="enterable form-control form-control-sm idbarang idbarang-barangnya" value="<?= $r['barang'] ?>">
                                                            <input type="hidden" name="items[<?= ($rr - 1) ?>][zona]" id="zona" class="enterable form-control form-control-sm zona zona-barangnya" value="<?= $penjualan['zona'] ?>">
                                                            <input type="hidden" name="items[<?= ($rr - 1) ?>][kelas]" id="kelas" class="enterable form-control form-control-sm kelas kelas-barangnya" value="<?= $r['kelas'] ?>">
                                                            <input readonly type="text" name="items[<?= ($rr - 1) ?>][nama_produk]" id="nama_produk" class="enterable form-control form-control-sm nama_produk form-table produkx<?= ($rr - 1) ?>" value="<?= $r['judul'] ?>" style="background-color:white;">
                                                        </td>
                                                        <td class="p-0 m-0"><input name="items[<?= ($rr - 1) ?>][harga]" id="harga" class="enterable form-control form-control-sm harga text-right form-table money harga-input-barangnya" style="background-color:white;" value="<?= $r['harga'] ?>" readonly></td>
                                                        <td class="p-0 m-0"><input type="number" name="items[<?= ($rr - 1) ?>][qty]" id="qty" value="<?= $r['qty'] ?>" class="enterable text-right form-control form-control-sm form-table qty qty-input-barangnya"></td>
                                                        <td class="p-0 m-0"><input type="number" name="items[<?= ($rr - 1) ?>][diskon]" id="diskon" class="enterable text-right form-control form-control-sm form-table diskon" value="<?= $r['disc'] ?>"></td>
                                                        <td class="p-0 m-0"><input readonly name="items[<?= ($rr - 1) ?>][hargadisc]" class="form-control form-control-sm form-table text-right money hargadisc hargadisc-input-barangnya" style="background-color:white;" value="<?= $r['hargadisc'] ?>"></td>
                                                        <td class="p-0 m-0"><input readonly name="items[<?= ($rr - 1) ?>][total]" class="form-control form-control-sm form-table text-right money total total-input-barangnya" style="background-color:white;" value="<?= $r['jumlah'] ?>"></td>
                                                        <td class="p-0 m-0" valign="middle" align="center" style="vertical-align:middle;"><span style="cursor:pointer;" class="fas fa-times text-danger delete" onclick="hapusbaris('<?= $rr ?>')"></span></td>
                                                        <!-- <td class="p-0 m-0" valign="middle" align="center" style="vertical-align:middle;"><span style="cursor:pointer;" class="fas fa-undo text-primary retur" onclick="load_retur(<? //= $r['id'] 
                                                                                                                                                                                                                                        ?>)" data-toggle="modal" data-target="#modalx"></span></td> -->
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer row pl-2 pr-2">
                                <div class="col-sm-12 text-right">
                                    <a href="/kasir/edit/<?= $id ?>" class="btn btn-sm btn-outline-warning">
                                        <i class="fa fa-refresh"></i>
                                        Muat Ulang (F5)
                                    </a>
                                    <a href="#" class="btn btn-sm btn-outline-danger" onclick="tutupkasir()">
                                        <i class="fa fa-times-circle"></i>
                                        Tutup Kasir
                                    </a>
                                    <button id="submitModal" class="btn btn-sm btn-primary" type="button" onclick="formmodalkasir()">
                                        <i class="fa fa-credit-card"></i>
                                        Bayar (F9)
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header bg-navy">
                    <h4 class="modal-title text-lg">Pembayaran Kasir</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-sm">
                    <input type="hidden" name="idbarangkeluar" id="idbarangkeluar" value="">
                    <div class="row p-0">
                        <div class="col-12 divkodeedc" style="display:none;">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <?php
                                    echo form_label('Masukkan Kode Pembayaran EDC');
                                    $no_bukti = [
                                        'type'  => 'text',
                                        'name'  => 'kodeedc',
                                        'id'    => 'kodeedc',
                                        'value' => '',
                                        'class' => 'form-control form-control-md fieldnya text-lg',
                                        'placeholder' => '',
                                        'style' => 'text-transform:uppercase',
                                        'min' => '1'
                                    ];
                                    echo form_input($no_bukti);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-12" id="cash">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <?php
                                    echo form_label('Total');
                                    $no_bukti = [
                                        'type'  => 'number',
                                        'name'  => 'totaltransaksi',
                                        'id'    => 'totaltransaksi',
                                        'value' => '',
                                        'class' => 'form-control form-control-md fieldnya text-right text-lg',
                                        'readonly' => 'true',
                                        'placeholder' => '0',
                                        'min' => '1'
                                    ];
                                    echo form_input($no_bukti);
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <?php
                                    echo form_label('Pembayaran');
                                    $no_bukti = [
                                        'type'  => 'number',
                                        'name'  => 'bayar',
                                        'id'    => 'bayar',
                                        'value' =>  $penjualan['bayar'],
                                        'class' => 'form-control form-control-md fieldnya text-right text-lg',
                                        'placeholder' => '0',
                                        'min' => '1'
                                    ];
                                    echo form_input($no_bukti);
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <?php
                                    echo form_label('Kembali');
                                    $no_bukti = [
                                        'type'  => 'number',
                                        'name'  => 'kembali',
                                        'id'    => 'kembali',
                                        'value' => '',
                                        'class' => 'form-control form-control-md fieldnya text-right text-lg',
                                        'readonly' => 'true',
                                        'placeholder' => '0',
                                        'min' => '1'
                                    ];
                                    echo form_input($no_bukti);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                    <button id="submitPembayaran" type="submit" class="btn btn-sm btn-outline-primary"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="modal fade" id="modalx">
    <div class="modal-dialog modal-md" id="ini">
        <div class="modal-content">

        </div>
    </div>
</div>

<!-- Modal peringatan -->
<div class="modal fade" id="modperingatan" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Peringatan!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <h3 id="judulperingatan">. . .</h3>
                <img src="<?= $this->mks::base('sedih.gif'); ?>" width="150">
                <p></p>
                <div id="teksperingatan">. . .</div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Tutup -->
<div class="modal fade" id="modtutup" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Closing Kasir</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form" action="<?= $this->mks::base('kasir/tutupkasir'); ?>" method="POST">
                <div class="modal-body">
                    <label>Masukkan Jumlah Uang Tunai Dikasir (saat ini)</label>
                    <input type="hidden" name="uniq_periode" value="">
                    <input type="number" name="uang_tunai_close" class="form-control form-control-sm" placeholder="Jumlang Uang Tunai Dikasir" required>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-outline-danger">
                        <i class="fa fa-save"></i>
                        Simpan & Tutup Kasir
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<link rel="stylesheet" href="<?= $this->mks::base('plugins/jquery-ui-1.12.1/jquery-ui.css'); ?>">

<script>
    function formmodalkasir(kode) {
        if (!$('#submitModal').is(":disabled")) {
            $('#bayar').prop('readonly', false)
            $('#jenispembayaran').append('<option value="0">Tunai</option>')
            $('#modal-lg').modal('show')
        } else {
            modaltidakboleh("Ooops", "Belum ada data penjualan yang terdaftar!");
        }
    }

    function tutupkasir() {
        $('#modtutup').modal('show')
    }

    $(function() {
        $("#tanggal").datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true
        });

        enable_cb();
        $("#c_umum").click(enable_cb);

        $('#jenispembayaran').change(function() {
            if ($(this).val() == "0") {
                $("#cash").show();
                $(".divkodeedc").hide();
            } else {
                $("#cash").hide();
                $(".divkodeedc").show();
            }
        });
    });

    var idang = "";

    function enable_cb() {
        if ($("#c_umum").is(':checked')) {
            idang = "0";
            $("input#nama_customer").attr("readonly", true);
            $("input#nama_customer").val('Pelanggan Umum');
            $("input#caribarang").focus();
            $('#cardgaris').removeClass('card-info')
            $('#cardgaris').addClass('card-danger')
            $('#teksindikator').prop('style', 'font-size:35px;height:50px;background:#dc3545;border:solid 1px #dc3545 !important;color:#f7f7f7;')
            $('#teksindikator').val('Umum')
        } else {
            idang = "";
            $("input#nama_customer").removeAttr("readonly");
            // $("input#nama_customer").val('');
            $("input#caribarang").focus();
            $('#cardgaris').removeClass('card-danger')
            $('#cardgaris').addClass('card-info')
            $('#teksindikator').prop('style', 'font-size:35px;height:50px;background:#17a2b8;border:solid 1px #17a2b8 !important;color:#f7f7f7;')
            $('#teksindikator').val('Anggota')
        }
        // $("#listitemx").empty();
        subtotalx();
        grandTotal();

    }

    function hapusbaris(idbaris) {
        const index = $('#data-item > tbody > tr').length;
        // console.log('baris'+idbaris);
        if (index == 1) {
            // addItem();
        } else {
            reindex();
        }
        $('#baris' + idbaris).remove()
        grandTotal();
        $('#caribarang').focus()
    }

    $('#modperingatan').on('hidden', function() {
        // console.log('kesini');
        $('#caribarang').focus()
    })

    function load_retur(id) {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('kasir/retur'); ?>",
            data: {
                id: id,
            },
            success: function(response) {
                // console.log(response);
                document.getElementById("ini").className = "modal-dialog modal-md";
                $(".modal-content").html(response);
                // $(".preloader").fadeOut();
            }
        });
    }

    function addItem(kelas, jenis, zona, cari_nama_produk, cari_idbarang, cari_kode_barang, cari_harga, cari_qty, cari_ppnpersen, cari_ppn, cari_milikgudang, cari_jenis_sumber_barang) {
        const index = $('#data-item > tbody > tr').length;
        const found = $('.kode_barang').map(function(i, e) {
            if ($(this).val() == cari_kode_barang) {
                return e;
            }
        });

        if (found.length > 0) {
            console.log(cari_harga);
            const findOne = $(found[0]).parent().parent().find(".qty-input-barangnya");
            const findHarga = $(found[0]).parent().parent().find(".harga-input-barangnya");
            const findHargadisc = $(found[0]).parent().parent().find(".hargadisc-input-barangnya");
            // const findSubtotal = $(found[0]).parent().parent().find(".subtotal-input-barangnya");
            // const findtotal = $(found[0]).parent().parent().find(".total-input-barangnya");
            const value = parseInt($(findOne).val()) + 1;
            // const valueSubtotal = parseInt($(findSubtotal).val()) + parseInt(cari_harga);
            // const valuetotal = parseInt($(findtotal).val()) + parseInt(cari_harga);
            $(findOne).val(value);
            $(findHarga).val(cari_harga);
            $(findHargadisc).val(cari_harga);
            // $(findSubtotal).val(valueSubtotal);
            // $(findtotal).val(valuetotal);
            subtotalx()
            return;
        }

        // const st = cari_qty * cari_harga;
        // console.log(index);
        var kelasx = romanize(kelas);
        $('#data-item > tbody').append(`
            <tr id="baris${cari_idbarang}">
                <td class="p-0 m-0"><input type="text" name="items[${index}][kelasx]" id="kelasx" value="${kelasx}" class="enterable text-right form-control text-center form-control-sm form-table kelasx kelasx-input-barangnya" style="background-color:white;" readonly></td>
                <td class="p-0 m-0"><input type="text" name="items[${index}][kode_barang]" id="kode_barang" value="${cari_kode_barang}" class="enterable text-center form-control form-control-sm form-table kelas kode_barang kode-input-barangnya" style="background-color:white;" readonly></td>
                <td class="p-0 m-0"><input type="tezt" name="items[${index}][jenis]" id="jenis" value="${jenis}" class="enterable text-center form-control form-control-sm form-table jenis jenis-input-barangnya" style="background-color:white;" readonly></td>
                <td class="p-0 m-0">
                    <input type="hidden" name="items[${index}][idbarang]" id="idbarang" class="enterable form-control form-control-sm idbarang idbarang-barangnya" value="${cari_idbarang}">
                    <input type="hidden" name="items[${index}][zona]" id="zona" class="enterable form-control form-control-sm zona zona-barangnya" value="${zona}">
                    <input type="hidden" name="items[${index}][kelas]" id="kelas" class="enterable form-control form-control-sm kelas kelas-barangnya" value="${kelas}">
                    <input readonly type="text" name="items[${index}][nama_produk]" id="nama_produk" class="enterable form-control form-control-sm nama_produk form-table produkx${index}" value="${cari_nama_produk}" style="background-color:white;">
                </td>
                <td class="p-0 m-0"><input name="items[${index}][harga]" id="harga" class="enterable form-control form-control-sm harga text-right form-table money harga-input-barangnya" style="background-color:white;" value="${cari_harga}" readonly></td>
                <td class="p-0 m-0"><input type="number" name="items[${index}][qty]" id="qty" value="${cari_qty}" class="enterable text-right form-control form-control-sm form-table qty qty-input-barangnya"></td>
                <td class="p-0 m-0"><input type="number" name="items[${index}][diskon]" id="diskon" class="enterable text-right form-control form-control-sm form-table diskon" value="0"></td>
                <td class="p-0 m-0"><input readonly name="items[${index}][hargadisc]" class="form-control form-control-sm form-table text-right money hargadisc hargadisc-input-barangnya" style="background-color:white;" value="${cari_harga}"></td>
                <td class="p-0 m-0"><input readonly name="items[${index}][total]" class="form-control form-control-sm form-table text-right money total total-input-barangnya" style="background-color:white;" value="${Number(cari_harga)}"></td>
                <td class="p-0 m-0" valign="middle" align="center" style="vertical-align:middle;"><span style="cursor:pointer;" class="fas fa-times text-danger delete" onclick="hapusbaris('${cari_idbarang}')"></span></td>
            </tr>
      `);
        // $('#data-item > tbody > tr:last > td > input.money').inputmask({
        //     alias: 'numeric',
        //     groupSeparator: ',',
        //     placeholder: '0',
        //     removeMaskOnSubmit: true,
        // })
        // $('#data-item > tbody > tr:last > td > input:eq(0)').focus();
        // $('#data-item > tbody > tr:last > td > input.produkx' + index).focus();

        // $('#data-item > tbody > tr').find(".nama_produk").autocomplete({
    }

    function romanize(num) {
        var lookup = {
                M: 1000,
                CM: 900,
                D: 500,
                CD: 400,
                C: 100,
                XC: 90,
                L: 50,
                XL: 40,
                X: 10,
                IX: 9,
                V: 5,
                IV: 4,
                I: 1
            },
            roman = '',
            i;
        for (i in lookup) {
            while (num >= lookup[i]) {
                roman += i;
                num -= lookup[i];
            }
        }
        return roman;
    }

    function reindex() {
        $('#data-item > tbody > tr').each(function(index) {
            $(this).find('input,select').each(function() {
                const attrName = $(this).attr('name').replace(/\[[0-9]+\]/, '[' + index + ']');
                $(this).attr('name', attrName);
            });
        });
    }

    function subtotalx() {
        $('#data-item > tbody > tr').each(function(i, e) {
            const harga = $(this).find('input.harga').val()
            const qty = $(this).find('input.qty').val()
            // const ppnnominalreal = $(this).find('input.ppnnominalreal').val()
            // const ppn = ppnnominalreal != "0" ? $(this).find('input.ppnpersen').val() : ppnnominalreal;
            // const ppnnominal = Number(harga) * Number(ppn) / 100;
            // const totppnnominal = Number(qty) * Number(ppnnominal);
            const diskon = $(this).find('input.diskon').val()
            // const subtotal = Number(qty) * (Number(harga)+Number(ppnnominal));
            const subtotal = Number(qty) * (Number(harga));
            const hargadisc = Number(harga) - (Number(harga) * (diskon / 100))
            const nilaiDiskon = subtotal * (diskon / 100)
            // $(this).find('input.ppn').val(totppnnominal + ' (' + $(this).find('input.ppnpersen').val() + '%)')
            $(this).find('input.hargadisc').val(hargadisc)
            $(this).find('input.total').val(subtotal - nilaiDiskon)
        });
    }

    function grandTotal() {
        var total = 0
        var subTotal = 0
        var qty = $('#qty').val()
        var harga = $('#harga').val()
        var bayar = $('#bayar').val()
        $('#data-item > tbody > tr').find('input.total,input.subtotal').each(function() {
            if ($(this).hasClass("total")) {
                nilai = $(this).val()
                total += parseInt(nilai)
            } else if ($(this).hasClass("subtotal")) {
                nilai = $(this).val()
                subTotal += parseInt(nilai)
            }
        });

        var limitKredit = ($("#sisa_limit_anggota").val() ?? 0)


        if (bayar > total) {
            bayar = total
            $('#bayar').val(total)
        }

        $('#totaldiskon').val(subTotal - total)
        $('#potongan').html("Potongan: Rp." + formatRupiah((subTotal - total) + ""))
        $('#subtotal').val(total)
        $('#sisa-pembayaran').val(total - bayar)
        $('#total').val(formatRupiah(total + ""))
        $('#totalxxx').val(total)
        $('#totaltransaksi').val(formatRupiah(total + ""))

        var tmptotal = Number($('#total').val().replace(/\./g, ""))
        if (tmptotal == 0) {
            $('#submitModal').prop('disabled', true)
        }
    }

    function cetakStruk() {
        var no_faktur = $("#no_faktur").val().replace(/\//g, "_");
        window.open(`/kasir/cetakpenjualanpdf?no_faktur=${no_faktur}`, "_blank");
    }

    $(document).ready(function() {
        $("#nama_customer").focus()
        if ($('#data-item > tbody > tr').length == 0) {
            // addItem();
            // $('#data-item > tbody').html('<center><small><i class="fas fa-info-circle text-info"></i> Belum ada data barang yang dipilih.</small></center>')
        } else {
            reindex();
            grandTotal()
        }

        $("#submitPembayaran").click(function(e) { // disini submit bayar
            e.preventDefault();
            var totaltransaksix = $("#totaltransaksi").val().replace(".", "");
            if (Number($("#bayar").val()) <= 0) { // jika nol
                $('#modal-lg').modal('hide')
                modalperingatan("Terjadi Kesalahan", "Pembayaran tidak boleh 0");
                return;
            } else if (Number($("#bayar").val()) < Number(totaltransaksix)) { // jika kurang
                $('#modal-lg').modal('hide')
                modalperingatan("Terjadi Kesalahan", "Pembayaran kurang");
                return;
            } else {
                // document.querySelector("#formKasirSimpan").submit()
                // cetakStruk();
                var formData = $("#formKasirSimpan").serialize();
                $.ajax({
                        type: 'POST',
                        url: $("#formKasirSimpan").attr('action'),
                        data: formData
                    })
                    .done(function(response) {
                        cetakStruk();
                        location.reload()
                    })
            }
        });

        $("#zona").on("change", function(e) {
            var zona = $(this).val()
            var kode = $("#cari_kode_barang").val()
            if (kode !== "") {
                $.ajax(`<?= base_url("kasir/getharga") ?>?zona=${zona}&kode=${kode}`, {
                    dataType: "JSON",
                    success: function(data, status, jqXhr) {
                        $("#cari_harga").val(data.harga);
                        $("#zonatext").val(zona);
                    },
                    error: function(err, status, jqXhr) {
                        console.error("Tidak dapat mengambil data barang")
                    }
                })

                return
            }
        })

        $('#data-item > tbody').on('keypress', '.enterable', function(e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                // addItem();
            }
        })

        $('#data-item > tbody').on('keypress', '#nama_produk', function(e) {
            const row = $(this).parents('tr')
            if (e.keyCode == 13) {
                e.preventDefault();
                $(row).find('input#qty').focus()
            }
        })

        // $('#data-item > tbody').on('change', '#qty', function(e) {
        //     const row = $(this).parents('tr')
        //     console.log('sjhfdhfkskdfhksdfh');
        // })

        $(document).on('keyup, change, input', '#bayar', function() {
            var totaltransaksi = $("#totaltransaksi").val()
            var bayar = $("#bayar").val()
            if (parseInt(bayar) - parseInt(totaltransaksi.replace(".", "")) <= 0) {
                $("#kembali").val(0);
            } else {
                $("#kembali").val(formatRupiah(parseInt(bayar) - parseInt(totaltransaksi.replace(".", "")) + ""))
            }
        })

        $('#data-item > tbody').on('keyup, change, input', 'input.harga,input.qty,input.diskon', function() {
            const row = $(this).parents('tr')
            const qty = $(row).find('input.qty').val()
            const idbaris = $(row).find('input.idbarang').val()
            if (Number(qty) > 0) {
                const harga = $(row).find('input.harga').val()
                // const ppnnominalreal = $(row).find('input.ppnnominalreal').val()
                // const ppnpersen = ppnnominalreal != "0" ? $(row).find('input.ppnpersen').val() : ppnnominalreal;
                // const ppnnominal = Number(harga) * Number(ppnpersen) / 100
                // const totppnnominal = ppnnominal * Number(qty);
                const diskon = $(row).find('input.diskon').val()
                // const subtotal = (harga * qty) + totppnnominal;
                const subtotal = (harga * qty);
                const nilaiDiskon = subtotal * (diskon / 100)
                const hargadisc = harga - (harga * (diskon / 100))
                // console.log(nilaiDiskon);
                // $(row).find('input.ppn').val(totppnnominal + ' (' + $(row).find('input.ppnpersen').val() + '%)')
                $(row).find('input.hargadisc').val(hargadisc)
                $(row).find('input.total').val(subtotal - nilaiDiskon)
                grandTotal()
            } else {
                $('#baris' + idbaris).remove()
                grandTotal()
            }
        })

        // $('#data-item > tbody').on('click', '.delete', function(e) {
        //     e.preventDefault();
        //     const index = $('#data-item > tbody > tr').length;
        //     $(this).parents('tr').remove();
        //     if (index == 1) {
        //         addItem();
        //     } else {
        //         reindex();
        //     }
        //     grandTotal();
        // })

        $(document).on('keydown', 'body', function(e) {
            var charCode = (e.which) ? e.which : event.keyCode;

            if (e.keyCode == 113) {
                $("#nama_customer").focus()
            }

            if (e.keyCode == 45) {
                $("#caribarang").focus()
            }
        })

        var dataawal = [];
        var tampung = [];
        var dataitem = [];

        $('#caribarang').on('keypress', function(e) {
            var kelas = $("#kelas").val()
            var jenis = $("#jenis").val()
            var zona = $("#zona").val()
            var cari_nama_produk = $("#cari_nama_produk").val()
            var cari_idbarang = $("#cari_idbarang").val()
            var cari_idsatuan = $("#cari_idsatuan").val()
            var cari_kode_barang = $("#cari_kode_barang").val()
            var cari_harga = $("#cari_harga").val()
            var cari_qty = $("#cari_qty").val()
            var cari_ppnpersen = $("#cari_ppnpersen").val()
            var cari_ppn = $("#cari_ppn").val()
            var cari_milikgudang = $("#cari_milikgudang").val()
            var cari_jenis_sumber_barang = $("#cari_jenis_sumber_barang").val()

            if (e.keyCode == 13) {
                e.preventDefault();
                if ($("#nama_customer").val() != "") {
                    if ($(this).val() != "") {
                        if (cari_idbarang != "") { // enter manual
                            //cari_nama_produk, cari_idbarang, cari_kode_barang, cari_harga, cari_qty, cari_ppnpersen, cari_ppn, cari_milikgudang, cari_jenis_sumber_barang
                            addItem(kelas, jenis, zona, cari_nama_produk, cari_idbarang, cari_kode_barang, cari_harga, 1, cari_ppnpersen, cari_ppn, cari_milikgudang, cari_jenis_sumber_barang);
                            grandTotal()
                            if (idang != "" && idang != "0") {
                                getmaxkredit(idang, $('#totalxxx').val(), function(bolehbelanja) {
                                    if (bolehbelanja) {

                                    } else {
                                        $('#baris' + cari_idbarang).remove()
                                        grandTotal()
                                    }
                                })
                            }
                            $("#cari_nama_produk").val('')
                            $("#cari_idbarang").val('')
                            $("#cari_satuan").val('')
                            $("#cari_kode_barang").val('')
                            $("#cari_harga").val('')
                            $("#cari_qty").val('')
                            $("#cari_ppnpersen").val('')
                            $("#cari_ppn").val('')
                            $("#cari_milikgudang").val('')
                            $("#cari_jenis_sumber_barang").val('')
                        } else { // enter otomatis
                            var kodenya = $(this).val()
                            $.ajax({
                                url: "<?= base_url('kasir/cekbykode') ?>",
                                type: 'post',
                                dataType: "json",
                                data: {
                                    search: $(this).val(),
                                    zona: $("#zona").val()
                                },
                                success: function(datax) {
                                    // response(data);
                                    if (Object.keys(datax).length <= 0) {
                                        modalperingatan('Data Tidak Ditemukan', 'Barang dengan kode <strong>' + kodenya + '</strong> tidak ditemukan')
                                    } else {
                                        addItem(kelas, jenis, zona, datax[0].namabarang, datax[0].idbarang, datax[0].kodebarang, datax[0].harga, 1, datax[0].ppn, datax[0].ppn_nominal, datax[0].milik_gudang, datax[0].jenis_sumber_barang);
                                        grandTotal()
                                        if (idang != "" && idang != "0") {
                                            getmaxkredit(idang, $('#totalxxx').val(), function(bolehbelanja) {
                                                if (bolehbelanja) {

                                                } else {
                                                    $('#baris' + datax[0].idbarang).remove()
                                                    grandTotal()
                                                    $('#caribarang').focus()
                                                }
                                            })
                                        }
                                        $("#cari_nama_produk").val('')
                                        $("#cari_idbarang").val('')
                                        $("#cari_satuan").val('')
                                        $("#cari_kode_barang").val('')
                                        $("#cari_harga").val('')
                                        $("#cari_qty").val('')
                                        $("#cari_ppnpersen").val('')
                                        $("#cari_ppn").val('')
                                        $("#cari_milikgudang").val('')
                                        $("#cari_jenis_sumber_barang").val('')
                                    }
                                },
                                error: function(errMsg) {
                                    console.log('error');
                                }
                            });
                        }
                    }
                    grandTotal()
                    $(this).val('')
                    $(this).focus()
                    $('#submitModal').prop('disabled', false)
                    $('#zona').prop('disabled', true)
                } else {
                    modalperingatan('Nama Pelanggan..!!!', 'Anda belum menuliskan nama pelanggan')
                    $("#caribarang").val('')
                    $('#nama_customer').focus()
                }
            }
        })

        function getmaxkredit(idang, belanja, callBack) {
            var data = [];
            data['nokartu'] = idang;
            kirim(data, 'kasir/getmaxkredit', function(obj) {
                if (obj.status && Object.keys(obj.data).length > 0) {
                    obj.data.forEach(x => {
                        var maxlimit = Number(x.maxlimit);
                        var totalbelanjaku = Number(x.belanjaku) + Number(belanja)
                        if (maxlimit <= totalbelanjaku) {
                            modalperingatan('Limit Kredit', x.nama + ' telah mencapai batas limit kredit')
                            callBack(false)
                        } else {
                            callBack(true)
                        }
                    })
                }
            })
        }

        function modalperingatan(judul, teks) {
            $('#judulperingatan').html(judul)
            $('#teksperingatan').html(teks)
            $('.close').show()
            $('#modperingatan').modal('show')
        }

        $("#caribarang").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "<?= base_url('kasir/pilihproduk') ?>",
                    type: 'post',
                    dataType: "json",
                    data: {
                        search: request.term,
                        zona: $("#zona").val(),
                        kelas: $("#kelas").val(),
                        jenis: $("#jenis").val(),
                    },
                    success: function(data) {
                        response(data);
                        // console.log(data.harga);
                    }
                });
            },
            select: function(event, ui) {
                // addItem()
                var rows = $('#data-item > tbody > tr').closest('tr');
                $(this).val(ui.item.label); // display the selected text
                $('#cari_nama_produk').val(ui.item.label); // display the selected text
                $('#cari_kode_barang').val(ui.item.value); // save selected id to input
                $('#cari_harga').val(ui.item.harga);
                $('#cari_idbarang').val(ui.item.idbarang);
                $('#cari_idsatuan').val(ui.item.idsatuan);
                $('#cari_harga').val(ui.item.harga);
                $('#cari_qty').val(ui.item.qty);
                $('#cari_ppnpersen').val(ui.item.ppn);
                $('#cari_ppn').val(ui.item.ppn_nominal);
                $('#cari_milikgudang').val(ui.item.milik_gudang);
                $('#cari_jenis_sumber_barang').val(ui.item.jenis_sumber_barang);

                // rows.find('.nama_produk').val(ui.item.label);
                // rows.find('.kode_barang').val(ui.item.value);
                // rows.find('.harga').val(ui.item.harga);
                return false;
            },
            focus: function(event, ui) {
                var rows = $('#data-item > tbody > tr').closest('tr');
                $(this).val(ui.item.label);
                $("#cari_nama_produk").val(ui.item.label);
                $("#cari_kode_barang").val(ui.item.value);
                $("#cari_harga").val(ui.item.harga);
                $("#cari_idbarang").val(ui.item.idbarang);
                $("#cari_idsatuan").val(ui.item.idsatuan);
                $('#cari_harga').val(ui.item.harga);
                $('#cari_qty').val(ui.item.qty);
                $('#cari_ppnpersen').val(ui.item.ppn);
                $('#cari_ppn').val(ui.item.ppn_nominal);
                $('#cari_milikgudang').val(ui.item.milik_gudang);
                $('#cari_jenis_sumber_barang').val(ui.item.jenis_sumber_barang);
                return false;
            },
        });

        $(document).on("change", ".ubahSatuan", function(e) {
            var kodebarang = $(this).data("kodebarang")
            var gudang = $(this).data("gudang")
            var jenisSatuan = $(this).val()
            var parentnya = $(this).parent().parent()

            $.ajax(`<?= base_url("kasir/qtychange") ?>?kodebarang=${kodebarang}&satuan=${jenisSatuan}`, {
                dataType: "JSON",
                success: function(data, statusText, jqXhr) {
                    parentnya.find(".harga-input-barangnya").val(data.harga)
                    subtotalx()
                    grandTotal()
                    if (idang != "" && idang != "0") {
                        getmaxkredit(idang, $('#totalxxx').val(), function(bolehbelanja) {
                            if (bolehbelanja) {

                            } else {
                                $('#baris' + data.idbarang).remove()
                                grandTotal()
                                $('#caribarang').focus()
                            }
                        })
                    }
                },
                error: function() {
                    alert("Jenis satuan tidak ada untuk barang ini")
                }
            })
        });

        $("#c_umum").on("change", function(e) {
            if (!$(this).prop("checked")) {
                // $("#sisa_limit_anggota_parent").removeClass("d-none")
                $('.xlimit').prop('style', 'display:block;')
                $('.ylimit').prop('style', 'display:block;')
            } else {
                $('.xlimit').prop('style', 'display:none;')
                $('.ylimit').prop('style', 'display:none;')
                // $("#sisa_limit_anggota_parent").addClass("d-none")
                $("#sisa_limit_anggota").val(null)
                $("#id_customer").val(null)
            }
        })
    });

    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    }

    $(document).ready(function() {
        $('#kelas').change(function() {
            var kelas = $('#kelas').val();
            console.log(kelas);
            if (kelas != '') {
                $.ajax({
                    url: "<?php echo site_url('kasir/fetch_jenis'); ?>",
                    method: "POST",
                    data: {
                        kelas: kelas
                    },
                    success: function(data) {
                        $('#jenis').html(data);
                    }
                });
            } else {
                $('#jenis').html('<option value="">Pilih Jenis</option>');
            }
        });
    });
</script>
<?= $this->endSection(); ?>