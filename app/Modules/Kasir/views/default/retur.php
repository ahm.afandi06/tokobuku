<?php echo form_open_multipart('databuku/update', ['autocomplete' => 'off']); ?>
<div class="modal-header">
    <h4 class="modal-title text-lg"><?= strtoupper($title) . ": " . $row['kode'] ?></h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body text-sm">
    <div class="row p-0">
        <input type="hidden" name="id" value="<?= $id ?>">
        <div class="col-md-4">
            <div class="form-group">
                <label>Kode</label>
                <input type="text" name="kode" id="kode" class="form-control form-control-sm text-center" value="<?= $row['kode'] ?>" readonly />
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <label>Judul</label>
                <input type="text" name="judul" id="judul" class="form-control form-control-sm text-left" value="<?= $row['judul'] ?>" readonly />
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Qty Jual</label>
                <input type="number" name="qty" id="qty" class="form-control form-control-sm text-left" value="<?= $detail['qty'] ?>" required />
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Qty Jual</label>
                <input type="number" name="qtyretur" id="qtyretur" class="form-control form-control-sm text-left" value="" required />
            </div>
        </div>
    </div>
</div>
<div class="modal-footer justify-content-between">
    <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
    <button type="submit" class="btn btn-sm btn-outline-primary"><i class="fa fa-save"></i> Simpan</button>
</div>
<?php echo form_close(); ?>