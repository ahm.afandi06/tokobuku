<?= $this->extend('main/layout'); ?>
<?= $this->section('content'); ?>
<!-- konten disini -->
<div class="card">
    <div class="card-header">
        <span class="h5 m-auto align-middle">Data Histori Penjualan</span>
        <div class="card-tools">
            <a href="<?= base_url('kasir/create') ?>" class="btn btn-outline-primary btn-sm">Tambah Penjualan</a>
        </div>
    </div>
    <div class="card-body">
        <?php if (session()->get('berhasil') !== null) { ?>
            <div class="alert alert-hijau text-success" id="alertnya">
                <span><?= session()->get('pesannya'); ?></span>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php } else if (session()->get('gagal') !== null) { ?>
            <div class="alert alert-merah text-danger" id="alertnya">
                <span><?= session()->get('pesannya'); ?></span>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php } ?>
        <table class="table table-bordered table-hovered table-condensed table-sm" id="table">
            <thead class="bg-secondary text-center">
                <tr>
                    <th class="text-center" style="width:5%;">No</th>
                    <th class="text-center">No.Faktur</th>
                    <th class="text-center">Tanggal</th>
                    <th class="text-center">Pelanggan</th>
                    <th class="text-center" width="5%">Total</th>
                    <!-- <th class="text-center"><i class="fas fa-cog"></i></th> -->
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 0;
                foreach ($penjualan as $bk) {
                    $no++;
                    echo '<tr>
                            <td class="text-center">' . $no . '</td>
                            <td class="text-center">' . $bk['no_faktur'] . '</td>
                            <td class="text-center">' . $bk['tanggal'] . '</td>
                            <td>' . $bk['pelanggan'] . '</td>
                            <td class="text-right">' . number_format($bk['total']) . '</td>
                        </tr>';
                }
                ?>
            </tbody>
        </table>
    </div>
</div>


<style media="screen">
    .fieldnya {
        margin-bottom: 15px;
    }
</style>

<!-- modal hapus -->
<div class="modal fade" id="modalhapus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="p-2">
                    <h4>Hapus Data Barang Keluar</h4>
                    <span>Anda yakin akan menghapus data ini?</span>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="idhapus" value="">
                <button type="button" class="btn btn-danger btn-sm" onclick="hapus()">
                    &nbsp;&nbsp;<i class="fas fa-trash"></i>&nbsp;&nbsp;Ya, Hapus Sekarang&nbsp;&nbsp;
                </button>
                <button type="button" class="btn btn-outline-secondary btn-sm" data-dismiss="modal">
                    &nbsp;&nbsp;<i class="fas fa-times"></i>&nbsp;&nbsp;Batal&nbsp;&nbsp;
                </button>
            </div>
        </div>
    </div>
</div>


<?php //echo $this->section('js');
// $tampungkategori = "";
// $tampungkategori = "<option value='' selected>Pilih Kategori</option>";
// foreach ($kategori as $row) {
//     $tampungkategori .= "<option value='" . $row['namakategori'] . "'>" . $row['namakategori'] . "</option>";
// }
// $selectkategorix = "<select name='kategori3' id='kategori3' class='form-control form-control-sm rounded-0 select2bs4'>" . $tampungkategori . "</select>";

// $tampungjenis = "";
// $tampungjenis = "<option value='' selected>Pilih Jenis</option>";
/* foreach ($jenis as $row) {
    $tampungjenis .= "<option value='" . $row['jenis'] . "'>" . $row['jenis'] . "</option>";
} */
// $selectjenisx = "<select name='jenis3' id='jenis3' class='form-control form-control-sm rounded-0 select2bs4'>" . $tampungjenis . "</select>";
?>
<script>
    function modalHapus(id) {
        $('#idhapus').val(id)
    }

    function hapus() {
        // console.log($('#idhapus').val());
        var form = new FormData();
        form.append("validasi", "go")
        form.append("id", $('#idhapus').val())
        $.ajax({
            url: 'barang/hapus',
            type: "POST",
            data: form,
            processData: false,
            contentType: false,
            success: function(hasil) {
                console.log(hasil);
                location.reload()
            },
            error: function(error) {
                alert("error");
                console.error(error);
            }
        })
    }

    var btntambah = '<a href="<?= base_url('inventory/barangkeluar/tambah') ?>" class="btn btn-sm btn-outline-primary">&nbsp;&nbsp;<i class="fas fa-plus-circle"></i>&nbsp;&nbsp;Tambah Penjualan&nbsp;&nbsp;</a>&nbsp;' +
        '<a href="<?= base_url('inventory/barangkeluar/tambahpo') ?>" class="btn btn-sm btn-outline-info">&nbsp;&nbsp;<i class="fas fa-plus-circle"></i>&nbsp;&nbsp;Tambah Penjualan PO&nbsp;&nbsp;</a>';
    var btninfonya = '<span class="btn">baris data</span>';
    const api = $('#table').DataTable({
        "responsive": true,
        "order": [
            [1, 'asc']
        ],
        "processing": true,
        "serverSide": true,
        "pageLength": 25,
        "language": {
            "lengthMenu": "_MENU_" + btninfonya + "&nbsp;&nbsp;" + btntambah,
        },
        "columns": [{
                "data": null,
                "class": "text-center",
                "orderable": false,
                "searchable": false,
                "render": function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                "data": "nofaktur",
                "className": "text-center",
            },
            {
                "data": "nopo",
                "className": "text-center",
            },
            {
                "data": "tanggal",
                "className": "text-center selectkategori",
            },
            {
                "data": "namapelanggan",
                "className": "text-left",
            },
            {
                "data": "subtotalx",
                "className": "text-right selectjenis",
            },
            {
                "data": "diskonx",
                "className": "text-right",
            },
            {
                "data": "totalx",
                "className": "text-right",
            },
            {
                "data": "totalreturx",
                "className": "text-right",
            },
            {
                "data": "totalnettox",
                "className": "text-right",
            },
            {
                "data": "actions",
                "className": "text-center no-search",
            },
        ],
        "ajax": {
            "url": "<?php echo site_url('kasir/index_json'); ?>",
        },
    })

    $('#table tfoot th').not(".no-search").each(function() {
        var title = $(this).text();
        // var selectkategori = $(this).hasClass('selectkategori');
        // var selectjenis = $(this).hasClass('selectjenis');
        // if (selectkategori == true) {
        //     $(this).html("<?php //echo $selectkategorix
                                ?>");
        // } else if (selectjenis == true) {
        //     $(this).html("<?php //echo $selectjenisx
                                ?>");
        // } else {
        $(this).html('<input class="form-control form-control-sm rounded-0" type="text" placeholder="Search ' + title + '" />');
        // }
    });

    api.on('init.dt', function() {
        api.columns()
            .every(function() {
                var that = this;

                $('input, select', this.footer()).on('keyup change clear', function() {
                    if (that.search() !== this.value) {
                        that.search(this.value).draw();
                    }
                });
            });
    });

    $(document).ready(function() {
        $('#kategori').change(function() {
            var kategori = $('#kategori').val();
            console.log(kategori);
            if (kategori != '') {
                $.ajax({
                    url: "<?php echo site_url('inventory/barang/fetch_jenis'); ?>",
                    method: "POST",
                    data: {
                        kategori: kategori
                    },
                    success: function(data) {
                        $('#jenis').html(data);
                    }
                });
            } else {
                $('#jenis').html('<option value="">Pilih jenis</option>');
            }
        });

        $('#kategori2').change(function() {
            var kategori2 = $('#kategori2').val();
            console.log(kategori2);
            if (kategori2 != '') {
                $.ajax({
                    url: "<?php echo site_url('inventory/barang/fetch_jenis'); ?>",
                    method: "POST",
                    data: {
                        kategori: kategori2
                    },
                    success: function(data) {
                        $('#jenis2').html(data);
                    }
                });
            } else {
                $('#jenis2').html('<option value="">Pilih jenis Barang</option>');
            }
        });

        $('#kategori3').change(function() {
            var kategori3 = $('#kategori3').val();
            console.log(kategori3);
            if (kategori3 != '') {
                $.ajax({
                    url: "<?php echo site_url('inventory/barang/fetch_jenisnama'); ?>",
                    method: "POST",
                    data: {
                        kategori: kategori3
                    },
                    success: function(data) {
                        $('#jenis3').html(data);
                    }
                });
            } else {
                $('#jenis3').html('<option value="">Pilih jenis</option>');
            }
        });
    });

    function getfungsi(kategori2, jenis2) {
        if (kategori2 != '') {
            $.ajax({
                url: "<?php echo site_url('inventory/barang/fetch_jenis'); ?>",
                method: "POST",
                data: {
                    kategori: kategori2
                },
                success: function(data) {
                    $('#jenis2').html(data);
                    $('#jenis2').val(jenis2);

                }
            });
        } else {
            $('#jenis2').html('<option value="">Pilih jenis Barang</option>');
        }
    }
</script>
<?= $this->endSection(); ?>