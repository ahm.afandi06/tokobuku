<?= $this->extend('main/layout'); ?>
<?= $this->section('content'); ?>
<!-- konten disini -->
<div class="card">
    <div class="card-header">
        <span class="h5 m-auto align-middle"><i class="fas fa-book"></i> INFO DATA BUKU</span>
        <div class="card-tools">
            <!-- <a href="<?= base_url('kasir/create') ?>" class="btn btn-outline-primary btn-sm">Tambah Penjualan</a> -->
        </div>
    </div>
    <div class="card-body">
        <table class="table table-bordered table-hovered table-condensed table-sm" id="table">
            <thead>
                <tr class="bg-biru text-light text-center">
                    <th class="text-center" style="width:1%;">No</th>
                    <th style="width:15%;" class="text-center text-nowrap">No. Faktur</th>
                    <th style="width:10%;" class="text-center text-nowrap">Tanggal</th>
                    <th class="text-center text-nowrap">Pelanggan</th>
                    <th class="text-center text-nowrap">Alamat</th>
                    <th style="width:10%;" class="text-center text-nowrap">Zona</th>
                    <th style="width:8%;" class="text-center text-nowrap">Total</th>
                    <th style="width:10%;" class="text-center text-nowrap"><i>input</i></th>
                    <th style="width:5%;" class="text-center text-nowrap"></th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr class="text-center filter">
                    <th class="no-search"></th>
                    <th>No. Faktur</th>
                    <th>Tanggal</th>
                    <th>Pelanggan</th>
                    <th>Alamat</th>
                    <th>Zona</th>
                    <th></th>
                    <th><i>input</i></th>
                    <th class="no-search"></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>


<style media="screen">
    .fieldnya {
        margin-bottom: 15px;
    }
</style>

<!-- modal hapus -->
<div class="modal fade" id="modalhapus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="p-2">
                    <h4>Hapus Data Buku</h4>
                    <span>Anda yakin akan menghapus data ini?</span>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="idhapus" value="">
                <button type="button" class="btn btn-danger btn-sm" onclick="hapus()">
                    &nbsp;&nbsp;<i class="fas fa-trash"></i>&nbsp;&nbsp;Ya, Hapus Sekarang&nbsp;&nbsp;
                </button>
                <button type="button" class="btn btn-outline-secondary btn-sm" data-dismiss="modal">
                    &nbsp;&nbsp;<i class="fas fa-times"></i>&nbsp;&nbsp;Batal&nbsp;&nbsp;
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-md" id="ini">
        <div class="modal-content">

        </div>
    </div>
</div>

<?php
$selopt_untuk = "";
$selopt_untuk = "<option value='' selected></option>";
$selopt_untuk .= "<option value='GURU'>GURU</option>";
$selopt_untuk .= "<option value='SISWA'>SISWA</option>";
$selectuntuk = "<select name='jenis' id='jenis' class='form-control form-control-sm rounded-0 mb-0'>" . $selopt_untuk . "</select>";

$selopt_kelas = "";
$selopt_kelas = "<option value='' selected></option>";
for ($i = 1; $i <= 12; $i++) {
    $selopt_kelas .= "<option value='$i'>" . $i . "</option>";
}
$selectkelas = "<select name='kelas' id='kelas' class='form-control form-control-sm rounded-0 mb-0'>" . $selopt_kelas . "</select>";
?>
<script>
    function load_tambah() {
        // $(".preloader").fadeIn();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('databuku/tambah'); ?>",
            success: function(response) {
                // console.log(response);
                document.getElementById("ini").className = "modal-dialog modal-xl";
                $(".modal-content").html(response);
                // $(".preloader").fadeOut();
            }
        });
    }

    function load_edit(id) {
        // $(".preloader").fadeIn();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('databuku/edit'); ?>",
            data: {
                id: id,
            },
            success: function(response) {
                // console.log(response);
                document.getElementById("ini").className = "modal-dialog modal-xl";
                $(".modal-content").html(response);
                // $(".preloader").fadeOut();
            }
        });
    }

    function modalHapus(id) {
        $('#idhapus').val(id)
    }

    function hapus() {
        // console.log($('#idhapus').val());
        var form = new FormData();
        form.append("validasi", "go")
        form.append("id", $('#idhapus').val())
        $.ajax({
            url: 'databuku/hapus',
            type: "POST",
            data: form,
            processData: false,
            contentType: false,
            success: function(hasil) {
                console.log(hasil);
                location.reload()
            },
            error: function(error) {
                alert("error");
                console.error(error);
            }
        })
    }

    var btntambah = '<a href="/kasir/create" class="btn btn-sm btn-primary">&nbsp;&nbsp;<i class="fas fa-store"></i>&nbsp;&nbsp;Tambah Penjualan&nbsp;&nbsp;</a>';
    // var btntambah = '';
    var btninfonya = '<span class="btn">baris data</span>';
    $(document).ready(function() {
        const api = $('#table').DataTable({
            "responsive": true,
            "order": [
                [0, 'asc']
            ],
            "processing": true,
            "serverSide": true,
            "pageLength": 25,
            "language": {
                "lengthMenu": "_MENU_" + btninfonya + "&nbsp;&nbsp;" + btntambah,
            },
            "columns": [{
                    "data": null,
                    "class": "text-center",
                    "orderable": false,
                    "searchable": false,
                    "render": function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "no_faktur",
                    "className": "text-center text-nowrap",
                },
                {
                    "data": "tanggal",
                    "className": "text-center text-nowrap",
                },
                {
                    "data": "pelanggan",
                    "className": "text-left",
                },
                {
                    "data": "alamat",
                    "className": "text-left",
                },
                {
                    "data": "zona",
                    "className": "text-center",
                },
                {
                    "data": "total",
                    "className": "text-right no-search",
                    "render": $.fn.dataTable.render.number('.', ',', 0, '')
                },
                {
                    "data": "waktu_jual",
                    "className": "text-center ",
                },
                {
                    "data": "actions",
                    "className": "text-center text-nowrap no-search",
                },
            ],
            "ajax": {
                "url": "<?php echo site_url('kasir/index_json'); ?>",
            }

        });

        $('#table tfoot th').not(".no-search").each(function() {
            var title = $(this).text();
            var selectuntuk = $(this).hasClass('selectuntuk');
            var selectkelas = $(this).hasClass('selectkelas');
            if (selectuntuk == true) {
                $(this).html("<?php echo $selectuntuk ?>");
            } else if (selectkelas == true) {
                $(this).html("<?php echo $selectkelas ?>");
            } else {
                $(this).html('<input class="form-control form-control-sm rounded-0 mb-0" type="text" placeholder="Search ' + title + '" />');
            }
        });
        api.on('init.dt', function() {
            var r = $('#table tfoot tr.filter');
            r.find('th').each(function() {
                $(this).css('padding', 0);
            });
            $('#table thead').append(r);
            $('#search_0').css('text-align', 'center');

            api.columns()
                .every(function() {
                    var that = this;

                    $('input, select', this.footer()).on('keyup change clear', function() {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });
        });
    });
</script>
<?= $this->endSection(); ?>