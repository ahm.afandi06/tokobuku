<?php

namespace App\Modules\Kasir\Controllers;

use Mpdf\Mpdf;
use Config\Database;
use Ozdemir\Datatables\Datatables;
use Ozdemir\Datatables\DB\Codeigniter4Adapter;

class DefaultController extends \App\Controllers\BaseController
{
    protected $helpers = [];
    private $idgudangtoko = 2;

    public function __construct()
    {
        helper(['form']);
    }

    public function index()
    {
        $model = new \App\Models\Kasir();
        return $this->mks::view('index')->mod(__CLASS__, '!');
    }

    public function index_json()
    {
        $dt = new Datatables(new Codeigniter4Adapter);
        $dt->query("SELECT id, no_faktur, tanggal, pelanggan, SUBSTRING(alamat, 1,50) as alamat, zona, total, waktu_jual FROM `penjualan` a ORDER BY a.id");

        $dt->add("actions", function ($record) {
            $defaultActions = [];
            $defaultActions[] = '<a href="/kasir/edit/' . $record['id'] . '" class="btn btn-xs btn-outline-primary">
                      <i class="fas fa-eye"></i> 
                    </a>';
            $defaultActions[] = '<a href="/kasir/cetakpenjualanpdf?no_faktur=' . str_replace('/', '_', $record['no_faktur']) . '" target="_blank" class="btn btn-xs btn-outline-success">
                      <i class="fas fa-print"></i>
                    </a>';
            /* $defaultActions[] = '<a href="/kasir/edit/' . $record['id'] . '?act=retur" class="btn btn-xs btn-outline-warning">
                      <i class="fas fa-undo"></i> 
                    </a>'; */
            $defaultActions[] = "";

            return "<span class='btn-group'>" . implode("", $defaultActions) . "</span>";
        });

        return $this->response->setJSON($dt->generate()->toJson());
    }

    public function create()
    {
        if (session('mksLogin') && session('level') == "3") {
            $model = new \App\Models\Kasir();
            // $satuan = $model->getSatuanAll()->getResultArray();
            $getnotrans = $this->db
                ->query("SELECT DATE_FORMAT(CURRENT_DATE(), '%y%m%d') as kode_date, LPAD(IF(COUNT(id)=0, 1,MAX(CAST(SUBSTRING_INDEX(no_faktur,'/',-1) AS UNSIGNED))+1),3,'0') as urut FROM `penjualan` WHERE SUBSTRING_INDEX(no_faktur,'/',1)='" . date('ymd') . "'")
                ->getRowArray();

            // $provider = $this->db
            //     ->query("select * from multipayment where gudang_id = '" . session('gudang') . "' and status = 1")
            //     ->getResultObject();

            $data = [
                'notransaksi' => $getnotrans['kode_date'] . "/" . session('kodeuser') . "/TKO" . "/" . $getnotrans['urut'],
                // 'satuan' => $satuan,
                // 'getkasir' => $getkasir,
                // 'countkasir' => count($getkasir),
                // 'provider' => $provider,
            ];

            return $this->mks::view('create', $data)->mod(__CLASS__, '@');
        }
        return redirect()->to('auth/login');
    }

    public function pilihproduk()
    {
        $search = $this->request->getPost('search');
        $zona = $this->request->getPost('zona');
        $kelas = $this->request->getPost('kelas');
        $jenis = $this->request->getPost('jenis');

        $model = new \App\Models\Kasir();
        $data = $model->getProdukAll($search, $zona, $kelas, $jenis)->getResultArray();
        if (count($data) > 0) {
            foreach ($data as $row) {
                $arr_result[] = array("value" => $row['kode'], "label" => $row['judul'], "harga" => $row['harga'], "qty" => 1, "idbarang" => $row['id']);
            }
            echo json_encode($arr_result);
        }
    }

    public function cekbykode()
    {
        $search = $this->request->getPost('search');
        $zona = $this->request->getPost('zona');
        $kelas = $this->request->getPost('kelas');
        $jenis = $this->request->getPost('jenis');

        $model = new \App\Models\Kasir();
        $row = $model->cekbykode($search, $zona, $kelas, $jenis)->getResultArray();
        $jenis_sumber_barang = "";
        $arr_result = [];
        if (count($row) > 0) {
            $r = $row[0];
            $arr_result[] = [
                "kodebarang" => $r['kode'],
                "namabarang" => $r['judul'],
                "harga" => $r['harga'],
                "qty" => 1,
                "idbarang" => $r['id'],
            ];
        }
        return json_encode($arr_result);
    }

    public function getharga()
    {
        $kode = $this->request->getGet('kode');
        $zona = $this->request->getGet('zona');

        $model = new \App\Models\Kasir();
        $data = $model->getHarga($kode, $zona)->getResultArray();
        if (count($data) > 0) {
            foreach ($data as $row) {
                $arr_result[] = array("value" => $row['kode'], "label" => $row['judul'], "harga" => $row['harga'], "qty" => 1, "idbarang" => $row['id']);
            }
            echo json_encode($arr_result[0]);
        }
    }

    public function simpan_create()
    {
        // dd('stop');
        if (session('mksLogin') && $this->request->getPost()) {
            // dd($this->request->getPost());
            // ngoding mulai dari sini
            $model = new \App\Models\Kasir();

            $no_faktur              = $this->request->getPost('no_faktur');
            $nama_customer          = $this->request->getPost('nama_customer');
            $alamat                 = $this->request->getPost('alamat');
            $zonatext                   = $this->request->getPost('zonatext');
            $total                  = $this->request->getPost('total');
            $bayar                  = $this->request->getPost('bayar');
            $sisa                   = $this->request->getPost('kembali');
            $kasir                  = session()->get('id');
            $waktu_jual             = date('Y-m-d H:i:s');

            $data = [
                'no_faktur'                 => $no_faktur,
                'tanggal'                   => date('Y-m-d'),
                'pelanggan'                 => $nama_customer,
                'alamat'                    => $alamat,
                'zona'                      => $zonatext,
                'total'                     => str_replace(".", "", $total),
                'bayar'                     => str_replace(".", "", $bayar),
                'kembali'                   => str_replace(".", "", $sisa),
                'kasir'                     => $kasir,
                'waktu_jual'                => $waktu_jual,
            ];
            // dd($data);
            $model->insertPenjualan($data);

            $IdPenjualan = $model->getIidPenjualan($no_faktur);

            foreach ($this->request->getPost('items') as $item) {

                $datadetail = [
                    'penjualan'                 => $IdPenjualan,
                    'barang'                    => $item['idbarang'],
                    'harga'                     => $item['harga'],
                    'qty'                       => $item['qty'],
                    'disc'                      => $item['diskon'],
                    'hargadisc'                 => $item['hargadisc'],
                    'jumlah'                    => $item['total'],
                    'waktu_jual'                => $waktu_jual,
                    'status'                    => 1,
                ];

                $model->insertPenjualanDetail($datadetail);
            }

            session()->setFlashdata(['berhasil' => 'ok', 'pesannya' => 'Berhasil melakukan transaksi.']);
            return redirect()->to("kasir/create");
        }
        return redirect()->to("auth/login");
    }

    public function fetch_jenis()
    {
        $model = new \App\Models\Kasir();

        if ($this->request->getPost('kelas')) {
            echo $model->fetch_jenis($this->request->getPost('kelas'));
        }
    }

    public function cetakStrukPdf()
    {
        if (session('mksLogin')) {
            $no_faktur = str_replace("_", "/", $this->request->getGet('no_faktur'));
            $data['no_faktur'] = $no_faktur;
            // $penjualan = $this->request->getGet('no_faktur');
            // $sql = "";


            // $penjualan = $mdata['id'];
            $sql = "SELECT
                        a.*, b.kode, b.judul, b.kelas, b.untuk
                    FROM
                        penjualan_detail a 
                        LEFT JOIN mst_buku b ON a.barang=b.id
                        LEFT JOIN penjualan c ON a.penjualan=c.id
                    WHERE
                        c.no_faktur= '" . $no_faktur . "'
                        ";
            // dd($sql);
            $data['data'] = $this->db->query($sql)->getResultArray();

            $htmlStr = view('cetak/print_struk_pdf', $data);

            $mpdf = new Mpdf([
                'orientation' => "landscape",
                'format' => "A5",
                'margin_left' => 5,
                'margin_right' => 5,
                'margin_top' => 5,
                'margin_bottom' => 5,
            ]);
            $mpdf->writeHTML($htmlStr);

            $this->response->setHeader('Content-Type', 'application/pdf');
            $mpdf->Output('cek.pdf', 'I');
        }
        return json_encode(['status' => false]);
    }

    public function laporanpenjualan()
    {
        if (session('mksLogin')) {
            $awal = date('Y-m-d');
            $akhir = date('Y-m-d');
            $kasir = "0";

            if ($this->request->getVar('awal') != "" && $this->request->getVar('akhir') != "") {
                $awal = htmlspecialchars($this->request->getVar('awal'));
                $akhir = htmlspecialchars($this->request->getVar('akhir'));
            }

            if (session('level') != "3" && $this->request->getVar('kasir') != "") {
                $kasir = $this->request->getVar('kasir');
            }

            $sesikasir = session('level') == "3" ? session('id') : $this->request->getVar('kasir');
            $awalx = $awal . " 00:00:00";
            $akhirx = $akhir . " 23:59:59";

            // $sql = "select vrp.*, u.nama as namakasir from v_riwayatpenjualan vrp join user_login u on u.id = vrp.kasir where vrp.waktu_jual >= '" . $awalx . "' and vrp.waktu_jual <= '" . $akhirx . "' and vrp.kasir = '" . $sesikasir . "'";
            // $data = $this->db->query($sql)->getResultArray();

            $sql2 = "select
        u.id as idkasir,
        u.gudang, u.nama as namakasir, u.level, u.status,
        g.namagudang
        from user_login u
        left join m_gudang g on g.idgudang = u.gudang
        where u.level = 3 and u.status = 1
        order by u.gudang asc";
            $gkasir = $this->db->query($sql2)->getResultArray();
            $datakasir = $this->mks::selectOpt($gkasir, 'namagudang');
            // dd($data);

            return $this->mks::view('laporan', [
                // 'data' => $data,
                'awal' => $awal,
                'akhir' => $akhir,
                'kasir' => $kasir,
                'datakasir' => $datakasir,
                'sesikasir' => $sesikasir,
            ])->mod(__CLASS__, '@');
        }
        return redirect()->to('auth/login');
    }

    public function edit($id)
    {
        if (session('mksLogin') && session('level') == "3") {
            $model = new \App\Models\Kasir();
            $penjualan = $model->getPenjualanbyId($id)->getRowArray();
            $penjualandetail = $model->getPenjualanDetailbyId($id)->getResultArray();

            $data = [
                'id' => $id,
                'penjualan' => $penjualan,
                'penjualandetail' => $penjualandetail,
                // 'getkasir' => $getkasir,
                // 'countkasir' => count($getkasir),
                // 'provider' => $provider,
            ];

            return $this->mks::view('edit', $data)->mod(__CLASS__, '@');
        }
        return redirect()->to('auth/login');
    }

    public function simpan_edit()
    {
        // dd('stop');
        if (session('mksLogin') && $this->request->getPost()) {
            // dd($this->request->getPost());
            // ngoding mulai dari sini
            $model = new \App\Models\Kasir();

            $id                     = $this->request->getPost('id');
            $no_faktur              = $this->request->getPost('no_faktur');
            $nama_customer          = $this->request->getPost('nama_customer');
            $alamat                 = $this->request->getPost('alamat');
            $zonatext               = $this->request->getPost('zonatext');
            $total                  = $this->request->getPost('total');
            $bayar                  = $this->request->getPost('bayar');
            $sisa                   = $this->request->getPost('kembali');
            $kasir                  = session()->get('id');
            $last_edit              = date('Y-m-d H:i:s');

            $data = [
                'pelanggan'                 => $nama_customer,
                'alamat'                    => $alamat,
                'total'                     => str_replace(".", "", $total),
                'bayar'                     => str_replace(".", "", $bayar),
                'kembali'                   => str_replace(".", "", $sisa),
                'user_edit'                 => $kasir,
                'last_edit'                 => $last_edit,
            ];
            // dd($data);
            $model->updatePenjualan($data, $id);

            $this->db->query("DELETE FROM penjualan_detail WHERE penjualan='$id'");
            foreach ($this->request->getPost('items') as $item) {

                $datadetail = [
                    'penjualan'                 => $id,
                    'barang'                    => $item['idbarang'],
                    'harga'                     => $item['harga'],
                    'qty'                       => $item['qty'],
                    'disc'                      => $item['diskon'],
                    'hargadisc'                 => $item['hargadisc'],
                    'jumlah'                    => $item['total'],
                    'status'                    => 1,
                ];

                $model->insertPenjualanDetail($datadetail);
            }

            session()->setFlashdata(['berhasil' => 'ok', 'pesannya' => 'Berhasil melakukan transaksi.']);
            return redirect()->to("kasir/edit/$id");
        }
        return redirect()->to("auth/login");
    }

    public function retur()
    {

        $id     = $this->request->getPost('id');

        $model = new \App\Models\Databuku();
        $modelKasir = new \App\Models\Kasir();
        $row = $model->whereBarang($id)->getRowArray();
        $data = [
            'title'             => 'Retur Barang',
            'id'                => $id,
            'row'               => $row,
            'detail'            => $modelKasir->getPenjualandetailbyIddetail($id)->getRowArray(),
        ];

        return $this->mks::view('retur', $data)->mod(__CLASS__, '!');
    }
}
