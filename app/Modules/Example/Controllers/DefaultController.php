<?php
namespace App\Modules\Auth\Controllers;

class DefaultController extends \App\Controllers\BaseController {

  public function index(){
    return $this->mks::view('index')->mod($this::class, '!');
  }
}
