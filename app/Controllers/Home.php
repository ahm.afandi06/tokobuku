<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {

        if (session('mksLogin')) {
            $jenis = "1";
            $nilai = date('m');
            $where1 = "and month(p.waktu_jual) = " . $nilai;
            $where2 = "";
            $where3 = "";
            $where33 = "";
            $where4 = "";
            $bataswaktu = "";
            $bataswaktu2 = "";

            /* if (session('level') == "3") {
                $where2 = "and gudang = '" . session('gudang') . "'";
                $where3 = "and gudang_req = '" . session('gudang') . "'";
                $where33 = "and gudangku = '" . session('gudang') . "'";
                $where4 = "and ke_gudang = '" . session('gudang') . "'";
            } */

            if ($this->request->getVar('jenis') != "") {
                $jenis = $this->request->getVar('jenis');
                if ($jenis == "0") {
                    $nilai = $this->request->getVar('nilai');
                    $where1 = "and year(p.waktu_jual) = " . $nilai;
                    $bataswaktu = "and year(waktu_req) = " . $nilai;
                    $bataswaktu2 = "and year(waktu_terima) = " . $nilai;
                } else if ($jenis == "1") {
                    $nilai = $this->request->getVar('nilai');
                    $plus = ((int)$nilai + 1);
                    $hasil = $plus < 10 ? "0" . $plus : $plus;
                    $where1 = "and month(p.waktu_jual) = " . $hasil;
                    $bataswaktu = "and month(waktu_req) = " . $hasil;
                    $bataswaktu2 = "and month(waktu_terima) = " . $hasil;
                } else if ($jenis == "2") {
                    $kal = explode("|", $this->request->getVar('kal'));
                    $where1 = "and date(p.waktu_jual) >= '" . $kal[0] . "' and date(p.waktu_jual) <= '" . $kal[1] . "'";
                    $bataswaktu = "and date(waktu_req) >= '" . $kal[0] . "' and date(waktu_req) <= '" . $kal[1] . "'";
                    $bataswaktu2 = "and date(waktu_terima) >= '" . $kal[0] . "' and date(waktu_terima) <= '" . $kal[1] . "'";
                }
            } else {
                $nilai = (int)$nilai - 1;
            }
            $sql = "select p.*, month(waktu_jual) as bln from penjualan p where p.status = 1 " . $where1 . " ";
            $query = $this->db->query($sql)->getResultArray();

            $label = [];
            $tunai = [];
            $kredit = [];

            if ($jenis == "0") {
                $labelbln = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];
                for ($i = 0; $i < count($labelbln); $i++) {
                    $tmptunai = 0;
                    $tmpkredit = 0;
                    foreach ($query as $q) {
                        $bln = ((int)$q['bln'] - 1);
                        if ($i === $bln && $q['bayar'] != "0") {
                            $tmptunai += (int)$q['total'];
                        } else if ($i === $bln && $q['bayar'] == "0") {
                            $tmpkredit += (int)$q['total'];
                        }
                    }
                    $label[] = $labelbln[$i];
                    $tunai[] = $tmptunai;
                    $kredit[] = $tmpkredit;
                }
            } else if ($jenis == "1") {
                $thn = date('Y');
                $bul = ((int)$nilai + 1) < 10 ? '0' . ((int)$nilai + 1) : ((int)$nilai + 1);
                $xdate = $thn . '-' . $bul . '-10';
                $tglakhir = (int)date("t", strtotime($xdate));
                for ($i = 1; $i <= $tglakhir; $i++) {
                    $tmptunai = 0;
                    $tmpkredit = 0;
                    foreach ($query as $q) {
                        $waktu_jual = substr($q['waktu_jual'], 0, 10);
                        $tglx = (int)date("d", strtotime($waktu_jual));
                        if ($i === $tglx && $q['bayar'] != "0") {
                            $tmptunai += (int)$q['total'];
                        } else if ($i === $tglx && $q['bayar'] == "0") {
                            $tmpkredit += (int)$q['total'];
                        }
                    }
                    $label[] = $i;
                    $tunai[] = $tmptunai;
                    $kredit[] = $tmpkredit;
                }
            } else if ($jenis == "2") {
                $kal = explode("|", $this->request->getVar('kal'));
                $tglawal_x = (int)date("d", strtotime($kal[0]));
                $tglakhir_x = (int)date("d", strtotime($kal[1]));
                for ($i = $tglawal_x; $i <= $tglakhir_x; $i++) {
                    $tmptunai = 0;
                    $tmpkredit = 0;
                    foreach ($query as $q) {
                        $waktu_jual = substr($q['waktu_jual'], 0, 10);
                        $tglx = (int)date("d", strtotime($waktu_jual));
                        if ($i === $tglx && $q['bayar'] != "0") {
                            $tmptunai += (int)$q['total'];
                        } else if ($i === $tglx && $q['bayar'] == "0") {
                            $tmpkredit += (int)$q['total'];
                        }
                    }
                    $label[] = $i;
                    $tunai[] = $tmptunai;
                    $kredit[] = $tmpkredit;
                }
            }

            $arrkal = [];
            if ($this->request->getVar('kal') != "") {
                $arrkal = explode("|", $this->request->getVar('kal'));
            } else {
                $tgl_x7 = date('Y-m-d', strtotime("-6 day", strtotime(date("Y-m-d"))));
                $arrkal = [$tgl_x7, date('Y-m-d')];
            }
            $kalenderx = $arrkal[0] . " s/d " . $arrkal[1];

            $totaltunai = 0;
            $totalkredit = 0;
            for ($i = 0; $i < count($tunai); $i++) {
                $totaltunai += (int)$tunai[$i];
                $totalkredit += (int)$kredit[$i];
            }
            $totalhasil = $totaltunai + $totalkredit;
            $persentunai = ($totaltunai != 0) ? $totaltunai / $totalhasil * 100 : 0;
            $persenkredit = ($totalkredit != 0) ? $totalkredit / $totalhasil * 100 : 0;
            // $qproses = $this->db->query("select count(id) as totalpoproses from pembelian where status = 1 " . $bataswaktu2 . " " . $where33)->getResultArray()[0];
            // $qkirim = $this->db->query("select count(id) as totalpoterkirim from req_po where status = 4 " . $bataswaktu . " " . $where4)->getResultArray()[0];

            return $this
                ->mks::view('welcome_message', [
                    'label' => json_encode($label),
                    'tunai' => json_encode($tunai),
                    'kredit' => json_encode($kredit),
                    'totaltunai' => json_encode(round($persentunai)),
                    'totalkredit' => json_encode(round($persenkredit)),
                    'totalhasil' => $totalhasil,
                    'jenis' => $jenis,
                    'nilai' => $nilai,
                    'kal' => $this->request->getVar('kal') != "" ? $this->request->getVar('kal') : $arrkal[0] . "|" . $arrkal[1],
                    'kalenderx' => $kalenderx,
                    'jmltrans' => count($query),
                    // 'qproses' => $qproses['totalpoproses'],
                    // 'qkirim' => $qkirim['totalpoterkirim'],
                ])
                ->render("get", '@');
        }
        return redirect()->to('auth/login');
    }

    public function dev()
    {
        if (session('mksLogin') && session('level') == "1") {
            if ($this->request->getPost()) {
                $kode = $this->request->getPost('kode');
                if ($kode == "duktekMKS2023") {
                    $sesi = session();
                    $sesi->set(['developer' => date('YmdHis')]);
                    return redirect()->to('/');
                } else {
                    return redirect()->to('home/dev');
                }
            }
            if (session('developer') != "") {
                return redirect()->to('/');
            }
            return $this->mks::view('tes')->render("get", "@");
        }
        return redirect()->to('auth/login');
    }
}
