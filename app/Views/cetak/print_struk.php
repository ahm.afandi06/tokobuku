<?php
$total = 0;
$totalWithPpn = 0;
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cetak Struk</title>
    <style type="text/css">
        table {
            border-collapse: separate;
            border-spacing: 0 .3em;
        }
    </style>
</head>

<body style="width: 88mm;">
    <div style="text-align: center; ">
        <span><b>KOPERASI KARYAWAN PEMI PASI</b></span><br>
        <span style="padding-top: -30px; ">Jl. Raya Serang Km. 24, Balaraja - Tangerang 15610</span><br>
        <span style="padding-top: -30px; ">Phone: (021) 595 1535, Fax. (021) 595 1539</span>
    </div>
    <div style="padding-top: 30px;">
        <table style="width: 100%; padding: 10px;">
            <?php
                $pajak = 0;
                $subTotal = 0;
                foreach($data['items'] as $_result):
            ?>
                <tr style="text-align: center;">
                    <td colspan="3" style=" text-align: left;"><?= $_result['nama_produk'] ?></td>
                </tr>
                <tr>
                    <td style="text-align: center; "><?= $_result['qty'] ?> <?= strtoupper($_result['satuan_txt']) ?>&nbsp;&nbsp;X</td>
                    <td style="text-align: right; "><?= $this->mks::rupiah($_result['harga']) ?>&nbsp;&nbsp;= </td>
                    <td style="text-align: right; "><?= $this->mks::rupiah($_result['total']) ?></td>
                </tr>
            <?php
                $pajak += $_result['ppnnominalreal'];
                $subTotal += $_result['subtotal'];
                endforeach
            ?>
        </table>
    </div>
    <div style="padding: 10px; margin-top: 25px;">
        <table style="width: 100%;">
            <tr>
                <td>Total</td>
                <td style="text-align: right;"><?= $this->mks::rupiah($subTotal) ?></td>
            </tr>
            <tr>
                <td>Diskon</td>
                <td style="text-align: right;"><?= $this->mks::rupiah(0) ?></td>
            </tr>
            <tr>
                <td><b>Total</b></td>
                <td style="text-align: right;"><?= $this->mks::rupiah($subTotal) ?></td>
            </tr>
            <tr>
                <td><b>Bayar</b></td>
                <td style="text-align: right;"><?= $this->mks::rupiah($data['bayar'] != "" ? $data['bayar'] : 0) ?></td>
            </tr>
            <tr>
                <td><b>Kembali</b></td>
                <td style="text-align: right;"><?= $this->mks::rupiah(preg_replace('/(\.|\")/i', '', $data['kembali']) != "" ? preg_replace('/(\.|\")/i', '', $data['kembali']) : 0) ?></td>
            </tr>
            <tr>
                <td>PPN</td>
                <td style="text-align: right;"><?= $this->mks::rupiah($pajak) ?></td>
            </tr>
        </table>
    </div>
</body>

<script>
    window.print();
    setTimeout(() => {
        window.close();
    }, 125);
</script>

</html>
