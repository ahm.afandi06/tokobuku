<?php setlocale(LC_TIME, "id_ID"); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cetak Penjualan</title>
    <style>
        @page {
            margin: 1cm;
        }

        table, .penjualan{
          font-size: 13px;
        }
        table#penjualan {
            width: 100%;
            border: 1px solid #000;
            border-collapse: collapse;
        }

        table#penjualan td,
        table#penjualan th {
            border: 1px solid #000;
            text-align: left;
            padding: 5px 15px 5px 15px;
        }
    </style>
  </head>
  <body>
    <table>
      <tr>
        <td>
          <table>
            <tr>
              <td style="width: 80px;">
                <img src="<?= $logo ?>" alt="" width="64">
              </td>
              <td style="font-size: 10pt;">
                <span>KOPERASI KARYAWAN PEMI PASI</span><br>
                <span>BADAN HUKUM No. 518 / 154A / PAD / XI.3 / KUMKM / 2015</span><br>
                <span>Jl. Raya Serang Km. 24, Balaraja - Tangerang 15610</span>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <div style="text-align: center;" class="period">
        <span><b>LAPORAN PENJUALAN - <?=strtoupper($gudang['namagudang']);?></b></span>
        <div>Periode</div>
        <div>
          <?=$a;?> s/d <?=$b;?>
        </div>
        <br>
    </div>
    <table class="penjualan" id="penjualan">
      <tr>
        <th style="text-align:center;">No</th>
        <th style="text-align:center;">Waktu Transaksi</th>
        <th style="text-align:center;">Jenis Pembeli</th>
        <th style="text-align:center;">Nama Anggota</th>
        <th style="text-align:center;">Metode Bayar</th>
        <th style="text-align:center;">Total Penjualan</th>
      </tr>
      <?php
        $no = 1;foreach($data as $d):
        $waktu = explode(" ", $d['waktu_jual']);
        $jenispembeli = $d['no_anggota'] != "" ? "Anggota" :"Pembeli Umum";;
        $noanggota = $d['no_anggota'] != "" ? "(".$d['no_anggota'].")" : "";;
        $mbayar = "";
        if($d['kode_edc'] != ""){
          $mbayar = "EDC (Kredit)";
        }else{
          if($d['tunai_bayar'] == "0"){
            $mbayar = "Kredit";
          }else{
            $mbayar = "Tunai";
          }
        }
      ?>
        <tr>
          <td style="text-align:center;"><?=$no;?></td>
          <td style="text-align:center;"><?=$this->mks::tglIndo($waktu[0]).' - ('.substr($waktu[1], 0, 5).')';?></td>
          <td style="text-align:center;"><?=$jenispembeli;?></td>
          <td style="text-align:center;"><?=$d['namaanggota'];?> <?=$noanggota;?></td>
          <td style="text-align:center;"><?=$mbayar;?></td>
          <td style="text-align:right;"><?=$this->mks::rupiah($d['totharga']);?></td>
        </tr>
      <?php $no++;endforeach; ?>
    </table>
  </body>
</html>
