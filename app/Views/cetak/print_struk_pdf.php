<?php

$this->db = \Config\Database::connect();
$mdata = $this->db->query("SELECT * FROM penjualan WHERE no_faktur= '" . $no_faktur . "'")->getRowArray();
$totqty = 0;
$total = 0;
$totalWithPpn = 0;
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Cetak Struk</title>
	<style type="text/css">
		body {
			font-family: Arial, Helvetica, sans-serif;
		}

		table {
			/* border-collapse: separate; */
			/* border-spacing: 0 .3em; */
			font-size: 10px;
		}

		table#penjualan {
			width: 100%;
			border: 1px dotted #000;
			border-collapse: collapse;
			font-size: 9px;
		}
	</style>
</head>

<body style="width: 88mm;">
	<div style="text-align: center; ">
		<table style="width: 100%;">
			<tr>
				<td rowspan="5" style="width: 60%;">
					<h1>FAKTUR PENJUALAN</h1>
				</td>
				<td>No. Transaksi</td>
				<td>: <?= $mdata['no_faktur'] ?></td>
			</tr>
			<tr>
				<td>Tanggal</td>
				<td>: <?= $mdata['tanggal'] ?></td>
			</tr>
			<tr>
				<td>Pelanggan</td>
				<td>: <?= $mdata['pelanggan'] ?></td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td>: <?= $mdata['alamat'] ?></td>
			</tr>
			<tr>
				<td colspan="2" style="font-weight: bold; padding-top: 30px;">*** Komplain Maksimal 7 Hari setelah barang diterima***</td>
			</tr>
		</table>
	</div>
	<div>
		<table style="width: 100%;" id="penjualan">
			<tr>
				<th style="border-top:1px solid #000;border-bottom:1px solid #000;padding:5px;">No</th>
				<th style="border-top:1px solid #000;border-bottom:1px solid #000;padding:5px;">Kelas</th>
				<th style="border-top:1px solid #000;border-bottom:1px solid #000;padding:5px;">Code</th>
				<th style="border-top:1px solid #000;border-bottom:1px solid #000;padding:5px;">Jenis</th>
				<th style="border-top:1px solid #000;border-bottom:1px solid #000;padding:5px;">Judul Buku</th>
				<th style="border-top:1px solid #000;border-bottom:1px solid #000;padding:5px;">HET</th>
				<th style="border-top:1px solid #000;border-bottom:1px solid #000;padding:5px;">Qty</th>
				<th style="border-top:1px solid #000;border-bottom:1px solid #000;padding:5px;">Disc</th>
				<th style="border-top:1px solid #000;border-bottom:1px solid #000;padding:5px;">Harga(Disc)</th>
				<th style="border-top:1px solid #000;border-bottom:1px solid #000;padding:5px;">JUMLAH</th>
				<th style="border-top:1px solid #000;border-bottom:1px solid #000;padding:5px;">#</th>
			</tr>
			<tbody>
				<?php
				$no = 0;
				foreach ($data as $_data) :
					$no++;
					$total += $_data['qty'] * $_data['hargadisc'];
					$totqty += $_data['qty'];
				?>
					<tr style="text-align: center;">
						<td style="padding:3px;text-align: center;"><?= $no ?></td>
						<td style="padding:3px;text-align: center;"><?= $this->mks->numberToRoman($_data['kelas']) ?></td>
						<td style="padding:3px;text-align: center;"><?= $_data['kode'] ?></td>
						<td style="padding:3px;text-align: center;"><?= $_data['untuk'] ?></td>
						<td style="padding:3px;"><?= $_data['judul'] ?></td>
						<td style="padding:3px;text-align: right;"><?= $this->mks::rupiah($_data['harga']) ?></td>
						<td style="padding:3px;text-align: center;"><?= $_data['qty'] ?></td>
						<td style="padding:3px;text-align: right;"><?= ($_data['disc']) ?>%</td>
						<td style="padding:3px;text-align: right;"><?= $this->mks::rupiah($_data['hargadisc']) ?></td>
						<td style="padding:3px;text-align: right;"><strong><?= $this->mks::rupiah($_data['hargadisc'] * $_data['qty']) ?></strong></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
			<tfoot>
				<tr>
					<th colspan="6" style="text-align: right;">Jumlah Item</th>
					<th><?= $totqty ?></th>
				</tr>
			</tfoot>
		</table>
	</div>
	<div style="padding-top: 10px;">
		<table style="width: 100%;">
			<tr>
				<td style="width: 65%;"></td>
				<td style="font-size: 9px; text-align: right;">Total</td>
				<td style="text-align: right; font-size: 9px;"><strong><?= $this->mks::rupiah($total) ?></strong></td>
			</tr>
			<tr>
				<td style="width: 65%;"></td>
				<td style="font-size: 9px; text-align: right;">Potongan</td>
				<td style="text-align: right; font-size: 9px;"><strong><?= $this->mks::rupiah(0) ?></strong></td>
			</tr>
			<tr>
				<td style="width: 65%;"></td>
				<td style="font-size: 9px; text-align: right;">Total Net</td>
				<td style="text-align: right; font-size: 9px;"><strong><?= $this->mks::rupiah($totalWithPpn + $total) ?></strong></td>
			</tr>
		</table>
	</div>
	<div style="padding-top: 10px;">
		<table>
			<tr>
				<td style="width: 200px;text-align:center;">Hormat Kami,</td>
			</tr>
			<tr>
				<td style="width: 200px;text-align:center;"></td>
				<td style="width: 200px;text-align:center;">Penerima</td>
			</tr>
			<tr>
				<td style="width: 200px;text-align:center;"><br><br><br><br><br><br><br></td>
				<td style="width: 200px;text-align:center;"></td>
			</tr>
			<tr>
				<td style="width: 200px;text-align:center;">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
				<td style="width: 200px;text-align:center;">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
			</tr>
			<tr>
				<td colspan="2">*Terbilang: <b><i><?= $this->mks->terbilang($total) ?> rupiah</b></td>
			</tr>
		</table>
	</div>
	<script>
		window.location.reload()
	</script>
</body>

</html>