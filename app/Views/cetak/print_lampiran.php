<?php setlocale(LC_TIME, "id_ID"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cetak Invoice</title>
    <style>
        @page {
            margin: 1cm;
        }


        table#invoice {
            width: 100%;
            border: 1px solid #000;
            border-collapse: collapse;
        }

        table#invoice td,
        table#invoice th {
            border: 1px solid #000;
            text-align: left;
            padding: 5px 15px 5px 15px;
        }
    </style>
</head>

<body>
    <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <td style="width: 80px;">
                            <img src="<?= $logo ?>" alt="" width="64">
                        </td>
                        <td style="font-size: 10pt;">
                            <span>KOPERASI KARYAWAN PEMI PASI</span><br>
                            <span>BADAN HUKUM No. 518 / 154A / PAD / XI.3 / KUMKM / 2015</span><br>
                            <span>Jl. Raya Serang Km. 24, Balaraja - Tangerang 15610</span>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="font-size: 10pt;">
                <span>BUYER: </span><br>
                <div style="margin-top:5px;">
                    <table style="border: 1px solid; border-collapse:collapse;">
                        <tr>
                            <td style="padding: 2px;">
                                <span><b>PT. EDS MANUFAKTURING INDONESIA</b></span><br>
                                <span><b>BALARA - TANGERANG - BANTEN</b></span>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>

    <div style="margin-top: 50px">
        <div style="text-align: center;">
            <span><b>LAMPIRAN</b></span>
        </div>
        <span><b>No. Invoice: <?= $main['nomor_invoice'] ?></b></span>
        <div style="padding-top: 20px;">
            <table style="width: 100%;" id="invoice">
                <thead>
                    <tr>
                        <th style="text-align: center;">NO</th>
                        <th style="text-align: center;">BUKTI KIRIM</th>
                        <th style="text-align: center;">NAMA BARANG</th>
                        <th style="text-align: center;">HARGA</th>
                        <th style="text-align: center;">Quantity</th>
                        <th style="text-align: center;">TOTAL</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($sub as $key => $data) : ?>
                        <tr>
                            <td colspan="6">
                                <span><b>No. PO: <?= $data['no_req_po'] ?></b></span>
                            </td>
                        </tr>
                        <?php $i = 0;
                        foreach ($data['items'] as $key2 => $data2) : ?>
                            <tr>
                                <td><?= $i += 1 ?></td>
                                <td><?= $data2['tgl_do'] ?> <?= $data2['surat_jalan'] ?></td>
                                <td><?= $data2['namabarang'] ?></td>
                                <td><?= $data2['harga'] ?></td>
                                <td><?= $data2['qty'] ?></td>
                                <td><?= $data2['harga_total'] ?></td>
                            </tr>
                        <?php endforeach; ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><?= $data['total_per_sj'] ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="5" style="text-align: right;">Total</th>
                        <th><?= $main['total'] ?></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</body>

</html>