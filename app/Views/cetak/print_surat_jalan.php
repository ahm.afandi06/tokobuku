<?php setlocale(LC_TIME, "id_ID"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cetak Invoice</title>
    <style>
        @page {
            margin: 1cm;
        }


        table#barang {
            width: 100%;
            border: 1px solid #000;
            border-collapse: collapse;
        }

        table#barang td,
        table#barang th {
            border: 1px solid #000;
            text-align: left;
            padding: 5px 15px 5px 15px;
        }
    </style>
</head>

<body>
    <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <td style="width: 170px;">
                            <img src="<?= $logo ?>" alt="" width="92">
                        </td>
                        <td style="font-size: 10pt; text-align: center;">
                            <span>KOPERASI KARYAWAN</span><br>
                            <span>PEMI-PASI</span><br>
                            <span>BADAN HUKUM No. 518 / 154A / PAD / XI.3 / KUMKM / 2015</span><br>
                            <span>Jl. Raya Serang Km. 24, Balaraja - Tangerang 15610, PO. BOX 229</span><br>
                            <span>Tangerang, Indonesia</span><br>
                            <span>Phone: (021) 595 1535, Fax. (021) 595 1539</span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <div style="margin-top: 15px">
        <div style="float: right; width: 28%;">
            <table>
                <tr>
                    <td colspan="2">SURAT JALAN</td>
                </tr>
                <tr>
                    <td>No. </td>
                    <td><?= $sub['surat_jalan'] ?></td>
                </tr>
                <tr>
                    <td colspan="2">Tanggal <?= date("d-m-Y", strtotime($sub['tgl_do'])) ?></td>
                </tr>
            </table>
        </div>
    </div>

    <div style="padding-top: 25px;">
        <table style="width: 100%;">
            <tr>
                <td>
                    <span>Buyer / Pemberi Order</span><br>
                    <span>10102939/PT EDS MANUFAKTURING INDONESIA</span>
                </td>
                <td>
                    <span>No. PO <?= $sub['no_req_po'] ?></span><br>
                    <span>Penerima: </span><br>
                    <span>PRD</span><br>
                    <span>IBU SUSILAH/ IBU KESIH</span>
                </td>
            </tr>
        </table>
    </div>

    <div style="padding-top: 35px;">
        <table id="barang">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Barang</th>
                    <th>Jumlah</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($sub['items'] as $key => $data) : ?>
                    <tr>
                        <td><?= $key + 1 ?></td>
                        <td><?= $data['namabarang'] ?></td>
                        <td><?= $data['qty'] ?> <?= $data['namasatuan'] ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</body>

</html>