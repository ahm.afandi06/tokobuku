<?php setlocale(LC_TIME, "id_ID"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cetak Cash Flow</title>
    <style>
        @page {
            margin: 1cm;
        }

        table, .period{
          font-size: 13px;
        }
        table#cashflow {
            width: 100%;
            border: 1px solid #000;
            border-collapse: collapse;
        }

        table#cashflow td,
        table#cashflow th {
            border: 1px solid #000;
            text-align: left;
            padding: 5px 15px 5px 15px;
        }
    </style>
</head>

<body>
    <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <td style="width: 80px;">
                            <img src="<?= $logo ?>" alt="" width="64">
                        </td>
                        <td style="font-size: 10pt;">
                            <span>KOPERASI KARYAWAN PEMI PASI</span><br>
                            <span>BADAN HUKUM No. 518 / 154A / PAD / XI.3 / KUMKM / 2015</span><br>
                            <span>Jl. Raya Serang Km. 24, Balaraja - Tangerang 15610</span>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="font-size: 10pt;">

            </td>
        </tr>
    </table>

    <?php
      $arropen = explode(" ", $waktu_open);
      $wopen = $this->mks::tglIndo($arropen[0]);
      $arrclose = [];
      $wclose = "Belum diatur";
      if($waktu_close != "Belum diatur"){
        $arrclose = explode(" ", $waktu_close);
        $wclose = $this->mks::tglIndo($arrclose[0]);
      }
    ?>

    <div style="margin-top: 50px">
        <div style="text-align: center;" class="period">
            <span><b>LAPORAN CASH FLOW</b></span>
            <div>
              <?=$wopen;?> s/d <?=$wclose;?>
            </div>
        </div>
        <p></p>
        <table>
          <tr>
            <td>Saldo Akhir (Sistem)</td>
            <td>:</td>
            <td>
              <strong><?=$this->mks::rupiah($saldoakhir);?></strong>
            </td>
          </tr>
          <tr>
            <td>Saldo Akhir (Dilaporkan)</td>
            <td>:</td>
            <td>
              <strong><?=($tunai_close != ". . ." ? $this->mks::rupiah($tunai_close) : $tunai_close);?></strong>
            </td>
          </tr>
          <tr>
            <td>Selisih</td>
            <td>:</td>
            <td>
              <strong><?=$this->mks::rupiah($selisih);?></strong>
            </td>
          </tr>
        </table>

        <div style="padding-top: 5px;">
            <table id="cashflow">
                <thead>
                    <tr>
                        <td colspan="3"></td>
                        <td colspan="4" style="text-align: center;">Cash Flow</td>
                    </tr>
                    <tr>
                        <th>No</th>
                        <th>Tanggal Transaksi</th>
                        <th>Keterangan</th>
                        <th style="text-align: right;">Saldo Awal</th>
                        <th style="text-align: right;">Masuk</th>
                        <th style="text-align: right;">Keluar</th>
                        <th style="text-align: right;">Saldo Akhir</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($main as $key => $data) : ?>
                        <tr>
                            <td><?= $key + 1 ?></td>
                            <td><?= date("d F Y - (H:i)", strtotime($data['tgl_trans'])) ?></td>
                            <td><?= $data['keterangan'] ?></td>
                            <td style="text-align: right;">Rp. <?= number_format($data['sebelumnya'], 0, ',', '.') ?></td>
                            <td style="text-align: right;">Rp. <?= number_format($data['masuk'], 0, ',', '.') ?></td>
                            <td style="text-align: right;">Rp. <?= number_format($data['keluar'], 0, ',', '.') ?></td>
                            <td style="text-align: right;">Rp. <?= number_format($data['saatini'], 0, ',', '.') ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>
