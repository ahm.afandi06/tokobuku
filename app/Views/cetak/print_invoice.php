<?php setlocale(LC_TIME, "id_ID"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cetak Invoice</title>
    <style>
        @page {
            margin: 1cm;
        }


        table#invoice {
            width: 100%;
            border: 1px solid #000;
            border-collapse: collapse;
        }

        table#invoice td,
        table#invoice th {
            border: 1px solid #000;
            text-align: left;
            padding: 5px 15px 5px 15px;
        }
    </style>
</head>

<body>
    <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <td style="width: 80px;">
                            <img src="<?= $logo ?>" alt="" width="64">
                        </td>
                        <td style="font-size: 10pt;">
                            <span>KOPERASI KARYAWAN PEMI PASI</span><br>
                            <span>BADAN HUKUM No. 518 / 154A / PAD / XI.3 / KUMKM / 2015</span><br>
                            <span>Jl. Raya Serang Km. 24, Balaraja - Tangerang 15610</span>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="font-size: 10pt;">
                <span><i><u>SALES INVOICE</u></i></span><br>
                <span>Tangerang, <?= date("d F Y", strtotime($main['tanggal_buat'])) ?></span><br>
                <span>Kepada Yth : </span><br>
                <span>PT. EDS MANUFACTURING INDONESIA</span><br>
                <span>BALARAJA - TANGERANG - BANTEN</span>
            </td>
        </tr>
    </table>

    <div style="margin-top: 50px">
        <div style="text-align: center;">
            <span>NO. INVOICE : <?= $main['nomor_invoice'] ?></span>
        </div>
        <div style="padding-top: 20px;">
            <table style="width: 100%;" id="invoice">
                <thead>
                    <tr>
                        <th style="text-align: center;">No.</th>
                        <th style="text-align: center;">TANGGAL DO</th>
                        <th style="text-align: center;">NO SURAT JALAN</th>
                        <th style="text-align: center;">NO PO</th>
                        <th style="text-align: center;">AMOUNT</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($sub as $key => $data) : ?>
                        <tr class="no-bottom-border">
                            <td><?= $key + 1 ?></td>
                            <td><?= $data['tgl_do'] ?></td>
                            <td><?= $data['nomor_surat_jalan'] ?></td>
                            <td><?= $data['no_req_po'] ?></td>
                            <td><?= $data['amount'] ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="4" style="text-align:right;">Sub Total</th>
                        <td><?= $main['sub_total'] ?></td>
                    </tr>
                    <tr>
                        <th colspan="4" style="text-align:right;">PPN</th>
                        <td><?= $main['ppn'] ?></td>
                    </tr>
                    <tr>
                        <th colspan="4" style="text-align:right;">Grand Total</th>
                        <td><?= $main['total'] ?></td>
                    </tr>
                    <tr>
                        <th colspan="5" style="text-align:right;"><i><?= ucfirst(terbilang($main['total'])) ?></i></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</body>

</html>