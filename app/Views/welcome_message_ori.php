<?= $this->extend('main/layout'); ?>
<?= $this->section('content'); ?>
<style media="screen">
    .h2,
    #labelx,
    #labelnilai {
        color: #666666;
    }
</style>
<div class="card">
    <div class="card-header card-title bg-biru" style="padding:1px;"></div>
    <div class="card-body">
        <form class="form" action="<?= $this->mks::base(''); ?>" method="get">
            <div class="row text-center">
                <div class="col-sm-3 text-left">
                    <strong><span id="labelx">Pilih Jenis Filter</span></strong>
                    <select class="form-control form-control-sm" id="jenis" name="jenis" onchange="tampilnilai(this.value)">
                        <option value="0">Tahun</option>
                        <option value="1" selected>Bulanan</option>
                        <option value="2">Harian</option>
                    </select>
                </div>
                <div class="col-sm-3 text-left" id="lnilai" style="display:none;">
                    <strong><span id="labelnilai">. . .</span></strong>
                    <select class="form-control form-control-sm" id="nilai" name="nilai"></select>
                    <input type="text" id="kalender" name="kalender" style="display:none;" class="form-control form-control-sm" placeholder="Klik untuk memilih tanggal">
                    <input type="hidden" id="kal" name="kal">
                </div>
                <div class="col-sm-4 text-left pl-0" id="lbtn" style="display:none;">
                    <button type="submit" class="btn btn-sm text-light bg-biru" style="margin-top:19.5px;">
                        <i class="fa fa-filter"></i>
                        Filter Data
                    </button>
                </div>
                <div class="col-sm-12 p-1 text-center" style="color:#666666;">
                    <hr style="margin-top:5px;margin-bottom:10px;">
                    <span>Periode :</span>
                    <div id="subjudul" style="font-weight:bold;">. . .</div>
                    <p></p>
                </div>
                <div class="col-sm-3">
                    <a href="<?= $this->mks::base('kasir/laporanpenjualan'); ?>">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-header bg-biru" style="padding:0.5px;"></div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0"><span class="text-biru">Tra</span>nsaksi</h5>
                                        <span class="h2 font-weight-bold mb-0 nominal"><?= $jmltrans; ?></span>
                                        <div id="dtekskecil"><small><span class="text-biru">Tra</span>nsaksi</small></div>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-biru text-white text-center rounded-circle shadow">
                                            <i class="fa fa-shopping-basket"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-muted text-sm">
                                    <span class="text-nowrap text-biru">JUMLAH TRANSAKSI</span>
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="<?= $this->mks::base('kasir/laporanpenjualan'); ?>">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-header bg-danger" style="padding:0.5px;"></div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0"><span class="text-danger">Pen</span>jualan</h5>
                                        <span class="h2 font-weight-bold mb-0 nominal"><?= $this->mks::rupiah($totalhasil, false); ?></span>
                                        <div id="dtekskecil"><small><span class="text-danger">Pen</span>jualan</small></div>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-danger text-white text-center rounded-circle shadow">
                                            <i class="fa fa-calculator"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-muted text-sm">
                                    <span class="text-nowrap text-danger">TOTAL HASIL PENJUALAN</span>
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="<?= $this->mks::base('inventory/pembelian'); ?>">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-header bg-warning" style="padding:0.5px;"></div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0"><span class="text-warning">PEM</span>BELIAN</h5>
                                        <span class="h2 font-weight-bold mb-0 nominal"><?= $qproses; ?></span>
                                        <div id="dtekskecil"><small><span class="text-warning">PEM</span>BELIAN</small></div>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-warning text-white text-center rounded-circle shadow">
                                            <i class="fa fa-truck"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-muted text-sm">
                                    <span class="text-nowrap text-warning">BARANG MASUK</span>
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="<?= $this->mks::base('pogudang'); ?>">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-header bg-success" style="padding:0.5px;"></div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0"><span class="text-success">Kir</span>im PO</h5>
                                        <span class="h2 font-weight-bold mb-0 nominal"><?= $qkirim; ?></span>
                                        <div id="dtekskecil"><small><span class="text-success">Kir</span>im PO</small></div>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-success text-white text-center rounded-circle shadow">
                                            <i class="fa fa-paper-plane-o"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-muted text-sm">
                                    <span class="text-nowrap text-success">TERKIRIM</span>
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-12">
                    <hr>
                </div>
                <div class="col-sm-5 text-center" style="color:#666666;">
                    <div class="bg-warning mb-2" style="padding:1px;"></div>
                    <div class="text-muted" style="font-size:15px;">Prosentase Hasil Penjualan</div>
                    <small id="subjudulpie" style="display:none;">. . .</small>
                    <canvas id="canvaspie"></canvas>
                </div>
                <div class="col-sm-7 text-center" style="color:#666666;">
                    <div class="bg-biru mb-2" style="padding:1px;"></div>
                    <div class="text-muted" style="font-size:15px;">Grafik Hasil Penjualan</div>
                    <small id="subjudul" style="display:none;">. . .</small>
                    <div id="canvasgrafik"></div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    var jenis = "<?= $jenis; ?>";
    var nilai = "<?= $nilai; ?>";

    // var height = document.body.clientHeight;
    // var width = document.body.clientWidth;
    // console.log(width, height);

    $(function() {
        $('input[name="kalender"]').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: '<i class="fa fa-times mr-1"></i>Batal',
                applyLabel: '<i class="fa fa-search mr-1"></i>Terapkan',
            }
        });
        $('input[name="kalender"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD') + ' s/d ' + picker.endDate.format('YYYY-MM-DD'));
            var a = picker.startDate.format('YYYY-MM-DD');
            var b = picker.endDate.format('YYYY-MM-DD');
            $('#kal').val(a + '|' + b)
        });
        $('input[name="kalender"]').on('cancel.daterangepicker', function(ev, picker) {
            // $(this).val('');
        });
    });

    tampilnilai(jenis, 'langsung');

    function tampilnilai(id, dari = null) {
        $('#lbtn').show()
        $('#nilai').empty()
        var tahunsekarang = "<?= date('Y'); ?>";
        var blnsekarang = "<?= date('m'); ?>";
        if (id != "2") {
            $('#kalender').prop('required', false)
            $('#kalender').hide()
            $('#nilai').show()
            $('#jenis').val(id)
            if (id == "0") {
                $('#labelnilai').text('Spesifik Tahun')
                for (var i = Number(tahunsekarang) - 5; i <= Number(tahunsekarang); i++) {
                    var selek = '';
                    if (dari == null) {
                        selek = i == tahunsekarang ? 'selected' : '';
                    } else {
                        selek = i == nilai ? 'selected' : '';
                    }
                    $('#nilai').append('<option value="' + i + '" ' + selek + '>' + i + '</option>')
                }
                $('#subjudul').text('Tahun ' + $('#nilai').val())
                $('#subjudulpie').text('Tahun ' + $('#nilai').val())
            } else if (id == "1") {
                var bln = [
                    'Januari', 'Februari', 'Maret',
                    'April', 'Mei', 'Juni',
                    'Juli', 'Agustus', 'September',
                    'Oktober', 'November', 'Desember'
                ];
                $('#labelnilai').text('Spesifik Bulan')
                for (var i = 0; i < 12; i++) {
                    var selek = '';
                    var selek = '';
                    if (dari == null) {
                        selek = i == (Number(blnsekarang) - 1) ? 'selected' : '';
                    } else {
                        selek = i == nilai ? 'selected' : '';
                    }
                    $('#nilai').append('<option value="' + i + '" ' + selek + '>' + bln[i] + '</option>')
                }
                $('#subjudul').text('Bulan ' + $('#nilai option:selected').text())
                $('#subjudulpie').text('Bulan ' + $('#nilai option:selected').text())
            }
        } else {
            $('#jenis').val(id)
            $('#kalender').prop('required', true)
            $('#kalender').val('<?= $kalenderx; ?>')
            $('#kal').val('<?= $kal; ?>')
            $('#nilai').hide()
            $('#kalender').show()
            $('#labelnilai').text('Pilih Rentang Waktu')
            var tg = $('#kalender').val().split(" s/d ");
            $('#subjudul').text(tglIndo(tg[0]) + ' s/d ' + tglIndo(tg[1]))
            $('#subjudulpie').text(tglIndo(tg[0]) + ' s/d ' + tglIndo(tg[1]))
        }
        $('#lnilai').show()
    }
    var chart;
    var hasiltunai = JSON.parse('<?= $tunai; ?>');
    var hasilkredit = JSON.parse('<?= $kredit; ?>');
    var hasiltotaltunai = JSON.parse('<?= $totaltunai; ?>');
    var hasiltotalkredit = JSON.parse('<?= $totalkredit; ?>');
    var label = JSON.parse('<?= $label; ?>');

    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'canvasgrafik',
                type: 'line',
            },
            title: {
                text: '',
                x: -20
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                categories: label
            },
            yAxis: {
                title: { //label yAxis
                    text: 'Hasil Penjualan'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                    return '<b>' + this.x + '</b><br/>' +
                        this.series.name + ': ' + this.y;
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -10,
                y: 100,
                borderWidth: 0
            },

            series: [{
                name: 'Penjualan Tunai',
                lineWidth: 2,
                color: '#4F94CD',
                data: hasiltunai
            }, {
                name: 'Penjualan Kredit',
                lineWidth: 2,
                dashStyle: 'ShortDash',
                color: '#dc3545',
                data: hasilkredit
            }]
        });

        var canvaspie = document.getElementById("canvaspie");
        Chart.defaults.global.defaultFontFamily = "Lato";

        var datapie = {
            labels: ['Penjualan Tunai', 'Penjualan Kredit'],
            datasets: [{
                data: [Number(hasiltotaltunai), Number(hasiltotalkredit)],
                backgroundColor: [
                    'rgba(79, 148, 205,1)',
                    'rgba(220, 53, 69,1)',
                    'rgba(186,225,255,1)',
                    'rgba(186,255,201,1)',
                    'rgba(204, 153, 201,1)',
                    'rgba(158, 193, 207,1)',
                    'rgba(158, 224, 158,1)',
                    'rgba(253, 253, 151,1)',
                    'rgba(186,225,255,1)',
                    'rgba(158, 193, 168,1)',
                    'rgba(186,186,255,1)',
                    'rgba(158,253,201,1)'
                ]
            }]
        };

        var pieChart = new Chart(canvaspie, {
            type: 'pie',
            data: datapie,
            options: {
                legend: {
                    display: true,
                    position: 'top',
                    labels: {
                        fontColor: '#000'
                    }
                },
                layout: {
                    padding: {
                        left: 20,
                        right: 20,
                        bottom: 0,
                        top: 20
                    }
                },
                tooltips: {
                    enabled: true,
                    callbacks: {
                        label: function(tooltipItem, data) {
                            var dataset = data.datasets[tooltipItem.datasetIndex];
                            var labels = data.labels[tooltipItem.index];
                            var currentValue = dataset.data[tooltipItem.index];
                            return labels + ": " + currentValue + "%";
                        }
                    }
                }
            }
        });
    });
</script>
<?= $this->endSection(); ?>