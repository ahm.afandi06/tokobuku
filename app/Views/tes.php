<?=$this->extend('main/layout');?>
<?=$this->section('content');?>
<div class="modal fade" id="moddev" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Halo Developer!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modalbody">
        <form class="formxx" action="<?=$this->mks::base('home/dev');?>" method="post">
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-4"></div>
              <div class="col-sm-4">
                <strong>Masukkan Kode Developer :</strong>
                <input type="password" name="kode" id="kod" required placeholder="Masukkan Kode" class="form-control form-control-sm">
              </div>
              <div class="col-sm-4"></div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(e){
    $('#moddev').modal('show')
    $('#kod').focus()
  })
</script>
<?=$this->endSection();?>
