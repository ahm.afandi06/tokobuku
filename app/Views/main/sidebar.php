<aside class="main-sidebar elevation-1 bg-biru text-light">
  <!-- Brand Logo -->
  <!-- <a href="index3.html" class="brand-link">
    <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">AdminLTE 3</span>
  </a> -->

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-1 pt-1 d-flex">
      <div class="image" style="padding-top:8px;padding-left:9px;">
        <img src="<?= $this->mks::base('logo.png'); ?>" class="img-circle brand-image-xl elevation-2" style="width:40px;" alt="Logo PemiMart">
      </div>
      <div class="info">
        <div class="badge badge-light" style="font-size:20px;font-weight:bold;margin-top:5px;padding-top:2.5px;">
          <span class="text-biru">TOKO</span><span class="text-warning" style="font-size:14px;" class="text-muted">BUKU</span>
        </div>
        <div style="height: 12px;font-size:13px;padding-top:2px;margin-bottom:2px;color:white;">(SEBAGAI <?= strtoupper(session('namalevel')); ?>)</div>
      </div>
    </div>
    <hr>

    <!-- Sidebar Menu -->
    <?= view('main/menu', ['zmenu' => $zmenu]); ?>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>