<div class="content-header pt-0 m-0">
  <style media="screen">
  .clock {
    display: inline;
    font-weight: bold;
  }
  </style>
  <div class="row pt-1 pb-1" style="background:#e2e3e8;">
    <div class="col-sm-6 text-8">
      <i class="ml-1 fas fa-clock mr-1"></i>
      <?=date('d/m/Y');?> -
      <div id="jamdigital" class="clock" onload="jamDigital()"></div>
    </div>
    <div class="col-sm-6 text-right" style="font-weight:bold;">
      <!-- <a href="#" class="text-8" style="font-weight:normal;" onclick="modcarimenu()">
        <i class="fas fa-search"></i>
        Cari Menu
      </a> -->
      <a class="ml-2 text-8 mr-1" data-widget="fullscreen" href="#" role="button" style="font-weight:normal;">
        <i class="fas fa-expand-arrows-alt"></i>
        Mode Layar
      </a>
    </div>
  </div>
</div>

<!-- cari menu -->
<div class="modal fade" id="modcarimenu" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="overflow-y: initial !important">
    <div class="modal-content">
      <div class="modal-header" style="background:#e2e3e8;padding-bottom:10px !important;">
        <span style="margin-top:7px;" class="text-muted"><strong>CARI</strong></span>
        <input type="text" oninput="carikatamenu(this.value)" id="kata" class="ml-3 form-control form-control-sm" placeholder="Masukkan kata... (minimal 3 huruf)" autocomplete="off">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="overflow-y: auto;">
        <div class="row pl-1 pr-2" id="menupopup"></div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
function modcarimenu(){
  carikatamenu()
  $('#modcarimenu').modal('show')
}
$(document).ready(function(){
  $('#modcarimenu').on('shown.bs.modal', function(){
    $('#kata').val('')
    $('#kata').focus();
  })
})
function carikatamenu(kata = ""){
  var base = "<?=$this->mks::base('');?>";
  $('#menupopup').empty()
  if(kata.length > 2){
    var data = [];
    data['kata'] = kata;
    kirim(data, 'aksesmenu/popup', function(obj){
      if(obj.status){
        if(Object.keys(obj.data).length > 0){
          obj.data.forEach(x => {
            if(x.path != "#"){
              if(x.path == "Beranda"){
                x.path = "";
              }
              var tr = $('<div class="col-sm-12 p-1"></div>');
              tr.append('<a href="'+base+x.path+'" style="text-decoration:none;width:100%;"><span class="w-100 ml-1" style="color:#333333;">'+x.namamenu+'</span><i class="fa fa-long-arrow-right text-muted ml-3 fa-xs"></i><span style="margin-left:10px;"><span class="text-muted">('+ambilParentMenu(obj, x.parent, x.namamenu)+')</span></span></a><p></p>')
              $('#menupopup').append(tr)
            }
          })
        }else{
          var tr = $('<div class="col-sm-12 text-center"></div>');
          tr.append('<div><h4>Data tidak ditemukan!</h4></div>')
          tr.append('<div><img src="'+base+'sedih.gif" width="150"></div>')
          tr.append('<span class="w-100">Tidak ditemukan data menu yang mengandung kata <strong>'+kata+'</strong>.</span>')
          $('#menupopup').append(tr)
        }
      }
    })
  }else{
    var tr = $('<div class="col-sm-12 text-center pt-0 pb-2"></div>');
    tr.append('<div><img src="'+base+'cari.gif" width="200"></div>')
    tr.append('<span class="w-100">Silahkan masukkan kata yang ingin Anda cari.</span>')
    $('#menupopup').append(tr)
  }
}
function ambilParentMenu(obj, parent, namamenu){
  var hasil = namamenu;
  if(parent != null){
    obj.datalengkap.forEach(x => {
      if(x.id == parent){
        hasil = ambilParentMenu(obj, x.parent, x.namamenu)+' -> '+namamenu;
      }
    })
  }
  return hasil;
}
function jamDigital(){
  var date = new Date();
  var h = date.getHours();
  var m = date.getMinutes();
  var s = date.getSeconds();
  var session = "AM";

  if(h == 0){
      h = 12;
  }

  if(h > 12){
      h = h - 12;
      session = "PM";
  }

  h = (h < 10) ? "0" + h : h;
  m = (m < 10) ? "0" + m : m;
  s = (s < 10) ? "0" + s : s;

  var time = h + ":" + m + ":" + s + " " + session;
  document.getElementById("jamdigital").innerText = time;
  document.getElementById("jamdigital").textContent = time;

  setTimeout(jamDigital, 1000);

}

jamDigital();
</script>
