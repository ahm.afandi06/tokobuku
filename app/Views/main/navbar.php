<style>
    .scroll {
        max-height: 250px;
        /* menentukan tinggi maksimal */
        overflow-y: scroll;
        /* menambahkan scrollbar pada sumbu Y */
    }

    .dropdown-menu li {
        position: relative;
    }

    .dropdown-menu .dropdown-submenu {
        display: none;
        position: absolute;
        left: 100%;
        top: -7px;
    }

    .dropdown-menu .dropdown-submenu-left {
        right: 100%;
        left: auto;
    }

    .dropdown-menu>li:hover>.dropdown-submenu {
        display: block;
    }
</style>
<nav class="main-header navbar navbar-sm navbar-expand bg-biru" style="padding:0px;">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item pl-1">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>

        <li class="nav-item d-none d-sm-inline-block" onclick="showtentang()">
            <a href="#" class="nav-link">
                <i class="fa fa-info-circle"></i>
            </a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" onclick="modcarimenu()" href="#" role="button">
                <i class="fa fa-search"></i>
            </a>
        </li>
        <!-- BATAS -->
        <li class="nav-item pr-1">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button">
                <img src="<?= $this->mks::base(session('foto')) ?>" class="rounded-circle elevation-1 mr-1" alt="ftuser" width="24">
                <strong><span><?= session('nama'); ?></span></strong>
            </a>
            <div class="dropdown-menu dropdown-menu-right pt-0 pb-0 mr-2" style="font-size:13px;">
                <a href="<?= $this->mks::base('profil'); ?>" class="dropdown-item text-muted pt-2 pb-2">
                    <i class="nav-icon fas fa-id-card mr-2"></i>
                    Lihat Profil
                </a>
                <div class="dropdown-divider m-0"></div>
                <a href="#" class="dropdown-item text-muted pt-2 pb-2" onclick="modallogout()">
                    <i class="nav-icon fas fa-sign-in-alt mr-2"></i>
                    Keluar
                </a>
            </div>
        </li>
    </ul>
</nav>

<!-- modal tentang -->
<div class="modal fade" id="modtentang" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header bg-biru text-light" style="padding-bottom:10px !important;">
                <span class=""><strong><i class="fa fa-info-circle mr-1"></i>TENTANG APLIKASI</strong></span>
                <button type="button" class="close text-warning" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center bg-biru text-light">
                <br>
                <div class="badge badge-light" style="font-size:23px;font-weight:bold;margin-top:0px;padding-top:2.5px;">
                    <span class="text-biru">TOKO</span><span class="text-warning" style="font-size:14px;" class="text-muted">BUKU</span>
                </div>
                <div class="mt-3 mb-3">
                    <img src="<?= $this->mks::base('logo.png'); ?>" class="rounded-circle" width="100">
                </div>
                <div style="font-weight:bold;">
                    <span>Versi 1.0.0</span>
                </div>
                <div class="">
                    <span>Copyright &copy; 2023</span>
                    <div>Powered By JEMBERDEV </div>
                </div>
                <p></p>
            </div>
        </div>
    </div>
</div>

<!-- Modal peringatan -->
<div class="modal fade" id="modlogout" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Peringatan!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <h3 id="judullogout">Keluar dari Aplikasi</h3>
                <img src="<?= $this->mks::base('logout.gif'); ?>" width="200">
                <p></p>
                <div id="tekslogout">Anda yakin akan keluar dari aplikasi sekarang?</div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <a href="#" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">
                        <i class="fa fa-times"></i>
                        Batal
                    </a>
                    <a href="#" class="btn btn-sm bg-biru text-light" onclick="keluaraplikasi()">
                        <i class="fa fa-check"></i>
                        Ya, Keluar Sekarang!
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function modallogout() {
        $('#modlogout').modal('show')
    }

    function showtentang() {
        $('#modtentang').modal('show')
    }
</script>