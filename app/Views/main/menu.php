<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar text-sm flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li>
            <div class="input-group">
                <input class="form-control form-control-sidebar form-control-sm" type="search" id="carimenukiri" placeholder="cari menu..." aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sm btn-warning" id="carisaja">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
            <div class="p-1"></div>
        </li>
        <li class="nav-header"><strong>Menu</strong></li>
        <?php
        foreach ($zmenu->parent() as $p) :
            $aktif1 = $zmenu->cekRoute($p['route'], uri_string()) ? ' active' : '';
            $li1 = $zmenu->cekRoute($p['route'], uri_string()) ? ' menu-is-opening menu-open' : '';
        ?>
            <li class="nav-item<?= $li1; ?>">
                <a href="<?= base_url($p['path']); ?>" class="nav-link<?= $aktif1; ?>" style="padding-left:5px;">
                    <i class="nav-icon <?= $p['ikon']; ?>"></i>
                    <p>
                        <?= $p['namamenu']; ?>
                        <?php if ($p['asparent'] == "1") : ?>
                            <i class="right fas fa-angle-left"></i>
                        <?php endif; ?>
                    </p>
                </a>
                <?php if ($p['asparent'] == "1") : ?>
                    <ul class="nav nav-treeview">
                        <?php
                        foreach ($zmenu->sub($p['id']) as $s) :
                            $aktif2 = $zmenu->cekRoute($s['route'], uri_string()) ? ' active' : '';
                            $li2 = $zmenu->cekRoute($s['route'], uri_string()) ? ' menu-is-opening menu-open' : '';
                        ?>
                            <li class="nav-item<?= $li2; ?>">
                                <a href="<?= base_url($s['path']); ?>" class="nav-link<?= $aktif2; ?>" style="padding-left:13px;">
                                    <i class="nav-icon <?= $s['ikon']; ?>"></i>
                                    <p>
                                        <?= $s['namamenu']; ?>
                                        <?php if ($s['assub'] == "1") : ?>
                                            <i class="right fas fa-angle-left"></i>
                                        <?php endif; ?>
                                    </p>
                                </a>
                                <?php if ($s['assub'] == "1") : ?>
                                    <ul class="nav nav-treeview">
                                        <?php foreach ($zmenu->child($s['id']) as $c) : $aktif3 = $zmenu->cekRoute($c['route'], uri_string(), "child") ? ' active' : ''; ?>
                                            <li class="nav-item">
                                                <a href="<?= base_url($c['path']); ?>" class="nav-link<?= $aktif3; ?>" style="padding-left:21px;">
                                                    <i class="nav-icon <?= $c['ikon']; ?>"></i>
                                                    <p><?= $c['namamenu']; ?></p>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
        <li class="nav-item">
            <a href="#" class="nav-link" onclick="modallogout()" style="padding-left:5px;">
                <i class="nav-icon fas fa-sign-in-alt"></i>
                <p>
                    Keluar
                    <span class="right badge badge-danger">Logout</span>
                </p>
            </a>
        </li>
        <?php if (session('mksLogin') && session('level') == "1") { ?>
            <li class="nav-header">Administrator</li>
            <li class="nav-item">
                <a href="<?= $this->mks::base('aturkeuangan'); ?>" class="nav-link" style="padding-left:5px;">
                    <i class="nav-icon fas fa-list-alt"></i>
                    <p>
                        Atur Keuangan
                    </p>
                </a>
            </li>
            <?php if (session('developer') != "") : ?>
                <li class="nav-item">
                    <a href="<?= $this->mks::base('submitkeuangan'); ?>" class="nav-link" style="padding-left:5px;">
                        <i class="nav-icon fas fa-list-alt"></i>
                        <p>
                            Submit Keuangan
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <hr class="mb-2 mt-2">
                </li>
                <li class="nav-item">
                    <a href="<?= $this->mks::base('aksesmenu'); ?>" class="nav-link" style="padding-left:5px;">
                        <i class="nav-icon fas fa-list-alt"></i>
                        <p>
                            Akses / Atur Menu
                        </p>
                    </a>
                </li>
            <?php endif; ?>
        <?php } ?>
    </ul>
</nav>
<script type="text/javascript">
    $('#carimenukiri').on('click', function() {
        $('#carimenukiri').blur()
        modcarimenu()
    })

    function keluaraplikasi() {
        var login = "<?= $this->mks::base('auth/logout'); ?>"
        location.href = login
    }

    function formatRupiah(angka, prefix) {
        if (angka === undefined || angka === null)
            return 0;
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    function tglIndo(tanggal) {
        var bln = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni',
            'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'
        ]

        var thn = tanggal.substr(0, 4);
        var b = Number(tanggal.substr(5, 2)) - 1;
        var tgl = tanggal.substr(8, 2);

        return tgl + ' ' + bln[b] + ' ' + thn;
    }
</script>