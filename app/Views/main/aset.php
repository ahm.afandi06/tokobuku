<link rel="stylesheet" href="<?=$this->mks::base('dist/css/bootstrap.css');?>">
<link rel="stylesheet" href="<?=$this->mks::base('dist/css/adminlte.min.css');?>">
<link rel="stylesheet" href="<?=$this->mks::base('font/css.css');?>">
<link rel="stylesheet" href="<?=$this->mks::base('plugins/fontawesome-free/css/all.min.css');?>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
<link rel="stylesheet" href="<?=$this->mks::base('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css');?>">
<link rel="stylesheet" href="<?=$this->mks::base('plugins/icheck-bootstrap/icheck-bootstrap.min.css');?>">
<link rel="stylesheet" href="<?=$this->mks::base('plugins/overlayScrollbars/css/OverlayScrollbars.min.css');?>">
<link rel="stylesheet" href="<?=$this->mks::base('plugins/daterangepicker/daterangepicker.css');?>">
<link rel="stylesheet" href="<?=$this->mks::base('plugins/summernote/summernote-bs4.min.css');?>">
<link rel="stylesheet" href="<?= $this->mks::base('plugins/select2/css/select2.min.css'); ?>">
<link rel="stylesheet" href="<?= $this->mks::base('plugins/select2-bootstrap4-theme/select2-bootstrap4.css'); ?>">
<link rel="stylesheet" href="<?=$this->mks::base('dist/css/pemi.css');?>">
<link rel="stylesheet" href="<?= $this->mks::base('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css'); ?>">
<link rel="stylesheet" href="<?= $this->mks::base('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')?>">
<link rel="stylesheet" href="<?= $this->mks::base('plugins/datatables-buttons/css/buttons.bootstrap4.min.css')?>">
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
<script src="<?=$this->mks::base('dist/js/jquery-1.11.3.min.js');?>"></script>
<script src="<?=$this->mks::base('dist/js/Chart.js');?>"></script>
<script src="<?=$this->mks::base('dist/js/canvasjs.min.js');?>"></script>
<script src="<?=$this->mks::base('dist/js/moment.min.js');?>"></script>
<script src="<?=$this->mks::base('dist/js/highcharts.js');?>"></script>
