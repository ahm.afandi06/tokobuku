<script src="<?=$this->mks::base('plugins/jquery/jquery.min.js');?>"></script>
<script src="<?=$this->mks::base('plugins/jquery-ui/jquery-ui.min.js');?>"></script>
<script src="<?=$this->mks::base('plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>
<script src="<?=$this->mks::base('plugins/moment/moment.min.js')?>"></script>
<script src="<?=$this->mks::base('plugins/daterangepicker/daterangepicker.js');?>"></script>
<script src="<?=$this->mks::base('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js');?>"></script>
<script src="<?=$this->mks::base('plugins/summernote/summernote-bs4.min.js');?>"></script>
<script src="<?=$this->mks::base('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js');?>"></script>
<script src="<?=$this->mks::base('dist/js/adminlte.js');?>"></script>
<script src="<?= $this->mks::base('plugins/select2/js/select2.full.min.js'); ?>"></script>
<script src="<?=$this->mks::base('plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?=$this->mks::base('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');?>"></script>
<script src="<?=$this->mks::base('plugins/datatables-responsive/js/dataTables.responsive.min.js');?>"></script>
<script src="<?=$this->mks::base('plugins/datatables-responsive/js/responsive.bootstrap4.min.js');?>"></script>
<script src="<?=$this->mks::base('plugins/datatables-buttons/js/dataTables.buttons.min.js');?>"></script>
<script src="<?=$this->mks::base('plugins/datatables-buttons/js/buttons.bootstrap4.min.js');?>"></script>
<script src="<?=$this->mks::base('plugins/jszip/jszip.min.js');?>"></script>
<script src="<?=$this->mks::base('plugins/pdfmake/pdfmake.min.js');?>"></script>
<script src="<?=$this->mks::base('plugins/pdfmake/vfs_fonts.js');?>"></script>
<script src="<?=$this->mks::base('plugins/datatables-buttons/js/buttons.html5.min.js');?>"></script>
<script src="<?=$this->mks::base('plugins/datatables-buttons/js/buttons.print.min.js');?>"></script>
<script src="<?=$this->mks::base('plugins/datatables-buttons/js/buttons.colVis.min.js');?>"></script>
<!-- <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script> -->
<script>
  $(function () {
    $("#tabel").DataTable({
      "responsive": true, "lengthChange": true, "autoWidth": false,
      //"buttons": ["copy", "csv", "excel", "pdf", "print"]
    })//.buttons().container().appendTo('#tabel_wrapper .col-md-6:eq(0)');
    $("#tabel2").DataTable({
      "responsive": true, "lengthChange": true, "autoWidth": false,
    })
    $('.select2').select2()

    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
  });
  function eronly(params){
    $(params).blur()
  }
</script>
<script>
  $.widget.bridge('uibutton', $.ui.button)
  $("#alertnya").fadeTo(3000, 500).slideUp(500, function() {
    $("#alertnya").slideUp(500);
  });
</script>
<script>
  function kirim(dataInput, urlnya, callBack) {
    var base = "<?= $this->mks::base('') ?>"
    // var csrfToken = $('meta[name="csrf-token"]').attr("content");
    var csrfToken = "<?= date('YmdHis') ?>.";
    var form = new FormData();
    form.append("validasi", "go")
    form.append("_csrf", csrfToken)
    for (var key in dataInput) {
      form.append(key, dataInput[key])
    }

    $.ajax({
      url: base + urlnya,
      type: "POST",
      data: form,
      processData: false,
      contentType: false,
      success: function(hasil) {
        var obj = JSON.parse(hasil)
        callBack(obj)
      },
      error: function(error) {
        alert("error");
        console.error(error);
      }
    })
  }
</script>
<script>
  function read(path, idnotifikasi){
    let data = []
    data['path'] = path
    data['id_notifikasi'] = idnotifikasi
    // return console.log(''+path+'')
    kirim(data, 'notifikasi/dibaca', function(obj){
      location.href = path
    })
  }
</script>
<script type="text/javascript">
window.addEventListener('keydown', function(e){
  var ruting_x = "<?=uri_string();?>";
  var map = {};
  var levelku = "<?=session('level');?>";
  onkeydown = onkeyup = function(e){
    e = e || event;
    map[e.keyCode] = e.type == 'keydown';
    // console.log(map);
    if(map[16] == false && map[18] == false && map[67] == false){
      modcarimenu()
    }else if(map[16] == false && map[18] == false && map[75] == false && levelku == "3"){
      location.href="<?=$this->mks::base('');?>"+'kasir/create'
    }else if(map[119] == false && ruting_x == "kasir/create"){
      if(!$("#c_umum").is(':checked')){
        $('#c_umum').prop('checked', true);
        $('.xlimit').prop('style', 'display:none;')
        $('.ylimit').prop('style', 'display:none;')
          // $("#sisa_limit_anggota_parent").addClass("d-none")
        $("#sisa_limit_anggota").val(null)
        $("#id_customer").val(null)
      }else{
        $('#c_umum').prop('checked', false);
        $('.xlimit').prop('style', 'display:block;')
        $('.ylimit').prop('style', 'display:block;')
      }
      enable_cb()
    }else if(map[120] == false && ruting_x == "kasir/create"){ // tunai
      $('#jenispembayaran').empty()
      $('#modpilihbayar').modal('hide')
      if(!$('#submitModal').is(":disabled")){
        $('#bayar').prop('readonly', false)
        $('#jenispembayaran').append('<option value="0">Tunai</option>')
        $('#modal-lg').modal('show')
      }else{
        modaltidakboleh("Ooops", "Belum ada data penjualan yang terdaftar!");
      }
    }else if(map[17] == false && map[121] == false && ruting_x == "kasir/create"){ // kredit
      $('#jenispembayaran').empty()
      $('#modpilihbayar').modal('hide')
      if(!$('#submitModal').is(":disabled")){
        if(!$("#c_umum").is(':checked')){
          $('#jenispembayaran').append('<option value="0">Kredit Anggota</option>')
          $('#bayar').prop('readonly', true)
          $('#modal-lg').modal('show')
        }else{
          modaltidakboleh("Metode Tidak Tersedia", "Pembayaran kredit tidak tersedia untuk <strong>pelanggan umum</strong>");
        }
      }else{
        modaltidakboleh("Ooops", "Belum ada data penjualan yang terdaftar!");
      }
    }
  }
}, false);
function modaltidakboleh(judul, teks){
  $('#judulperingatantidak').html(judul)
  $('#teksperingatantidak').html(teks)
  $('.close').show()
  $('#modaltidakboleh').modal('show')
}
</script>
<!-- Modal peringatan -->
<div class="modal fade" id="modaltidakboleh" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Peringatan!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <h3 id="judulperingatantidak">. . .</h3>
        <img src="<?=$this->mks::base('sedih.gif');?>" width="150">
        <p></p>
        <div id="teksperingatantidak">. . .</div>
      </div>
    </div>
  </div>
</div>
