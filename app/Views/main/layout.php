<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TOKO BUKU</title>
    <link rel="shortcut icon" type="image/png" href="<?= $this->mks::base('logo.png'); ?>" />
    <?= view('main/aset'); ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper" id="pemiwrap">
        <?php $zmenu = $this->mks::menu(); ?>
        <?= view('main/preload'); ?>
        <?= view('main/navbar', ['zmenu' => $zmenu]); ?>
        <?= view('main/sidebar', ['zmenu' => $zmenu]); ?>
        <div class="content-wrapper">
            <?= view('main/header'); ?>
            <section class="content">
                <?= $this->renderSection('content'); ?>
            </section>
        </div>
        <?= view('main/footer'); ?>
    </div>
    <?= view('main/javascript'); ?>
</body>

</html>