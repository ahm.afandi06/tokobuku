<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Toko Buku</title>
  <link rel="shortcut icon" type="image/png" href="<?= $this->mks::base('logo.png'); ?>" />
  <?= view('main/aset'); ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
  <section class="content">
    <?= $this->renderSection('content'); ?>
  </section>
  <?= view('main/javascript'); ?>
  <script>
    $(function() {
      $('.select2').select2()
    })

    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
  </script>
</body>

</html>
