<?php

function rupiah1($angka){
  $hasil_rupiah = "Rp " . number_format($angka, 2, ',', '.');
  return substr($hasil_rupiah, 0, -3);
}

function tanggalan($tgl_awal){
  $exp = explode('-',$tgl_awal);
  return $exp[2]."-".$exp[1]."-".$exp[0];
}
