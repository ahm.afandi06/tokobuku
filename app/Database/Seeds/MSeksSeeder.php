<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MSeksSeeder extends Seeder
{
    public function run(){
        $data = [
            [
                'namaseks'  => 'Laki-laki',
            ],
            [
                'namaseks'  => 'Perempuan',
            ]
        ];
        $this->db->table('m_seks')->insertBatch($data);
    }
}
