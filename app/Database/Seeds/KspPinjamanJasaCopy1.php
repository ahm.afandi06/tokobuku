<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class KspPinjamanJasaCopy1 extends Seeder
{
    public function run(){
        $this->db->table('ksp_pinjaman_jasa_copy1')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/ksp_pinjaman_jasa_copy1.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 1000, ',')) !== false){
            if(!$transRow){
                $this->db->table('ksp_pinjaman_jasa_copy1')->insert([
                    'id_pinjaman'    => $data['1'] == '' ? null : $data['1'],
                    'jasa'    => $data['2'] == '' ? null : $data['2'],
                    'lama'    => $data['3'] == '' ? null : $data['3'],
                    'waktu'    => $data['4'] == '' ? null : $data['4'],
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
