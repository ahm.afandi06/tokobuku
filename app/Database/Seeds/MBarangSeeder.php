<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MBarangSeeder extends Seeder
{
    public function run(){
        $this->db->table('m_barang')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/m_barang.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 9999, ',')) !== false){
            if(!$transRow){
                $this->db->table('m_barang')->insert([
                    'idkategori'    => $data['1'],
                    'kodebarang'    => $data['2'],
                    'namabarang'    => $data['3'],
                    'foto'    => $data['4'],
                    'status'    => $data['5'],
                    'gudang'    => $data['6'],
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
