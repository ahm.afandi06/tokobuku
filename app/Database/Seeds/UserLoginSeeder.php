<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class UserLoginSeeder extends Seeder
{
    public function run(){
        $data = [
            [
                'gudang'    => '',
                'nama'  => 'Administrator',
                'username'  => 'admin',
                'password'  => password_hash('123', PASSWORD_DEFAULT),
                'foto'  => 'nouser.jpg',
                'level'  => '1',
                'terdaftar'  => date('Y-m-d H:i:s'),
                'status'    => '1'
            ],
            [
                'gudang'    => '',
                'nama'  => 'Supervisor',
                'username'  => 'supervisor',
                'password'  => password_hash('123', PASSWORD_DEFAULT),
                'foto'  => 'nouser.jpg',
                'level'  => '2',
                'terdaftar'  => date('Y-m-d H:i:s'),
                'status'    => '1'
            ],
            [
                'gudang'    => '2',
                'nama'  => 'Kasir Pemi-2',
                'username'  => 'pemi2',
                'password'  => password_hash('123', PASSWORD_DEFAULT),
                'foto'  => 'nouser.jpg',
                'level'  => '3',
                'terdaftar'  => date('Y-m-d H:i:s'),
                'status'    => '1'
            ],
            [
                'gudang'    => '1',
                'nama'  => 'Kasir Pemi-1',
                'username'  => 'pemi1',
                'password'  => password_hash('123', PASSWORD_DEFAULT),
                'foto'  => 'nouser.jpg',
                'level'  => '3',
                'terdaftar'  => date('Y-m-d H:i:s'),
                'status'    => '1'
            ],
        ];
        $this->db->table('user_login')->insertBatch($data);
    }
}
