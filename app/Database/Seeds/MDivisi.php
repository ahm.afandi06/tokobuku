<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MDivisi extends Seeder
{
    public function run()
    {
      $data = [
          [
              'kddivisi' => '9008006',
              'divisi' => 'Divisi 1',
              'status' => 1,
          ],
          [
              'kddivisi' => '9008007',
              'divisi' => 'Divisi 2',
              'status' => 1,
          ]
      ];
      $this->db->table('m_divisi')->insertBatch($data);
    }
}
