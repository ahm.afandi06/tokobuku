<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class KspSimpananJasaSeeder extends Seeder
{
    public function run(){
        $this->db->table('ksp_simpanan_jasa')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/ksp_simpanan_jasa.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 1000, ',')) !== false){
            if(!$transRow){
                $this->db->table('ksp_simpanan_jasa')->insert([
                    'id_simpanan'    => $data['1'] == '' ? null : $data['1'],
                    'jasa'    => $data['2'] == '' ? null : $data['2'],
                    'lama'    => $data['3'] == '' ? null : $data['3'],
                    'waktu'    => $data['4'] == '' ? null : $data['4'],
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
