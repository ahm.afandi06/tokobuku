<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call('AnggotaSeeder');
        $this->call('UserLoginSeeder');
        $this->call('UserMobileSeeder');
        $this->call('MGudangSeeder');
        $this->call('MRakSeeder');
        $this->call('MKategoriSeeder');
        $this->call('MKeuanganSeeder');
        $this->call('MSatuanSeeder');
        $this->call('MBarangSeeder');
        $this->call('MBarangHargaSeeder');
        $this->call('StokBarangSeeder');
        $this->call('CoaRek1Seeder');
        $this->call('CoaRek2Seeder');
        $this->call('CoaRek3Seeder');
        $this->call('CoaRek4Seeder');
        $this->call('MSeksSeeder');
        $this->call('MSebagaiBarangSeeder');
        $this->call('MPpnSeeder');
        $this->call('MLevelSeeder');
        $this->call('MMaxSeeder');
        $this->call('MJenisSumberBarangSeeder');
        $this->call('MSupplierSeeder');
        $this->call('ModalKasirSeeder');
        $this->call('MultipaymentSeeder');
        $this->call('ZMenuSeeder');
        $this->call('ZMenuDetailSeeder');
        $this->call('MBankSeeder');
        $this->call('KspMutasiSeeder');
        $this->call('KspPinjamanSeeder');
        $this->call('KspPinjamanAdminSeeder');
        $this->call('KspPinjamanAsuransiSeeder');
        $this->call('KspPinjamanJasaSeeder');
        $this->call('KspPinjamanPolaSeeder');
        $this->call('KspRekeningSeeder');
        $this->call('KspSimpananSeeder');
        $this->call('KspSimpananJasaSeeder');
        $this->call('MBagian');
        $this->call('MDivisi');
        $this->call('MGolongan');
        $this->call('SetakuntansiSeeder');
        $this->call('ZSubmitSeeder');
        $this->call('KspPinjamanJasaCopy1');
    }
}
