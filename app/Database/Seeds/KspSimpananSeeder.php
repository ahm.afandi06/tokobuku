<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class KspSimpananSeeder extends Seeder
{
    public function run(){
        $this->db->table('ksp_simpanan')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/ksp_simpanan.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 1000, ',')) !== false){
            if(!$transRow){
                $this->db->table('ksp_simpanan')->insert([
                    'jenis'    => $data['1'] == '' ? null : $data['1'],
                    'awal_buka'    => $data['2'] == '' ? null : $data['2'],
                    'jumlah'    => $data['3'] == '' ? null : $data['3'],
                    'satuan_simpanan'    => $data['4'] == '' ? null : $data['4'],
                    'biaya_admin'    => $data['5'] == '' ? null : $data['5'],
                    'waktu_simpan'    => $data['6'] == '' ? null : $data['6'],
                    'waktu_tarik'    => $data['7'] == '' ? null : $data['7'],
                    'kode'    => $data['8'] == '' ? null : $data['8'],
                    'keterangan'    => $data['9'] == '' ? null : $data['9'],
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
