<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class ModalKasirSeeder extends Seeder
{
    public function run(){
        $data = [
            [
                'kasir' => '4',
                'uniq_periode'  => md5(date('Ymdhis')),
                'open' => 'N'
            ],
            [
                'kasir' => '3',
                'uniq_periode'  => md5(date('Ymdhis')),
                'open' => 'N'
            ],
        ];
        $this->db->table('modal_kasir')->insertBatch($data);
    }
}
