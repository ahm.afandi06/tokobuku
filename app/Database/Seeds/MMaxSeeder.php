<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MMaxSeeder extends Seeder
{
  public function run(){
      $data = [
          [
              'limit' => 50,
              'mlg' => 30,
              'kredit' => 50
          ]
      ];
      $this->db->table('m_max')->insertBatch($data);
  }
}
