<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MSebagaiBarangSeeder extends Seeder
{
    public function run(){
        $data = [
            [
                'keterangan'    => 'Masuk'
            ],
            [
                'keterangan'    => 'Keluar'
            ],
            [
                'keterangan'    => 'Mastering'
            ],
            [
                'keterangan'    => 'Penyesuaian'
            ],
            [
                'keterangan'    => 'Penyesuaian (+)'
            ],
            [
                'keterangan'    => 'Penyesuaian (-)'
            ],
            [
                'keterangan'    => 'Retur'
            ],
        ];
        $this->db->table('m_sebagaibarang')->insertBatch($data);
    }
}
