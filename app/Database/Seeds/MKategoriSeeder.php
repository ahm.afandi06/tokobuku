<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MKategoriSeeder extends Seeder
{
    public function run(){
        $this->db->table('m_kategori')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/m_kategori.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 9999, ',')) !== false){
            if(!$transRow){
                $this->db->table('m_kategori')->insert([
                    'namakategori'    => $data['1'],
                    'status'    => $data['2'],
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
