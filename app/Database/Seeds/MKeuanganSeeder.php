<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MKeuanganSeeder extends Seeder
{
    public function run(){
        $this->db->table('m_keuangan')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/m_keuangan.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 1000, ',')) !== false){
            if(!$transRow){
                $this->db->table('m_keuangan')->insert([
                    'id'    => $data['0'],
                    'nmtrans'    => $data['1'],
                    'submit_id'    => $data['2'],
                    'fungsi'    => $data['3'],
                    'status'    => $data['4'],
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
