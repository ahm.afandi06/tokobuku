<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class CoaRek1Seeder extends Seeder
{
    public function run(){
        $this->db->table('coa_rek1')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/coa_rek1.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 1000, ',')) !== false){
            if(!$transRow){
                $this->db->table('coa_rek1')->insert([
                    'idrek1'    => $data['0'],
                    'koderek1'    => $data['1'],
                    'namarek1'    => $data['2'],
                    'na'    => $data['3'],
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
