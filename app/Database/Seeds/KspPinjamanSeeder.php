<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class KspPinjamanSeeder extends Seeder
{
    public function run(){
        $this->db->table('ksp_pinjaman')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/ksp_pinjaman.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 1000, ',')) !== false){
            if(!$transRow){
                $this->db->table('ksp_pinjaman')->insert([
                    'jenis'    => $data['1'] == '' ? null : $data['1'],
                    'kode'    => $data['2'] == '' ? null : $data['2'],
                    'pola'    => $data['3'] == '' ? null : $data['3'],
                    'max_pinjaman'    => $data['4'] == '' ? null : $data['4'],
                    'max_angsuran'    => $data['5'] == '' ? null : $data['5'],
                    'keterangan_angsuran'    => $data['6'] == '' ? null : $data['6'],
                    'provisi'    => $data['7'] == '' ? null : $data['7'],
                    'bayar_tagihan1'    => $data['8'] == '' ? null : $data['8'], // 0=tagihan pertama dibulan depannya; 1=tagihan pertama langsung potong saat pencairan
                    'bayar_admin'    => $data['9'] == '' ? null : $data['9'], // 0=admin tidak langsung dipotongkan, bulan depannya; 1=admin langsung potong saat pencairan
                    'bayar_asuransi'    => $data['10'] == '' ? null : $data['10'], // 0=asurannsi tidak langsung dipotongkan, bulan depannya; 1=asuransi langsung potong saat pencairan
                    'bayar_provisi'    => $data['11'] == '' ? null : $data['11'], // 0=provisi tidak langsung dipotongkan, bulan depannya; 1=provisi langsung potong saat pencairan
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
