<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MPpnSeeder extends Seeder
{
    public function run(){
        $data = [
            [
                'persen'    => 11,
                'status_aktif' => 'Y'
            ]
        ];
        $this->db->table('m_ppn')->insertBatch($data);
    }
}
