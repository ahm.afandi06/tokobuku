<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class CoaRek3Seeder extends Seeder
{
    public function run(){
        $this->db->table('coa_rek3')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/coa_rek3.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 1000, ',')) !== false){
            if(!$transRow){
                $this->db->table('coa_rek3')->insert([
                    'idrek3'    => $data['0'],
                    'koderek3'    => $data['1'],
                    'namarek3'    => $data['2'],
                    'na'    => $data['3'],
                    'idrek2'    => $data['4'],
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
