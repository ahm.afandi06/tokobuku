<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class KspRekeningSeeder extends Seeder
{
    public function run(){
        $this->db->table('ksp_rekening')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/ksp_rekening.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 1000, ',')) !== false){
            if(!$transRow){
                $this->db->table('ksp_rekening')->insert([
                    'norekening'    => $data['1'] == '' ? null : $data['1'],
                    'jenisproduk'    => $data['2'] == '' ? null : $data['2'], // S=Simpanan; P=Pinjaman
                    'idproduk'    => $data['3'] == '' ? null : $data['3'],
                    'idanggota'    => $data['4'] == '' ? null : $data['4'],
                    'tanggaldaftar'    => $data['5'] == '' ? null : $data['5'],
                    'jumlahpengajuan'    => $data['6'] == '' ? null : $data['6'],
                    'jumlahsetujui'    => $data['7'] == '' ? null : $data['7'],
                    'lamaangsuran'    => $data['8'] == '' ? null : $data['8'],
                    'lamaangsuranbulan'    => $data['9'] == '' ? null : $data['9'],
                    'angsuran'    => $data['10'] == '' ? null : $data['10'],
                    'jasa'    => $data['11'] == '' ? null : $data['11'],
                    'jasarp'    => $data['12'] == '' ? null : $data['12'],
                    'asuransi'    => $data['13'] == '' ? null : $data['13'],
                    'asuransirp'    => $data['14'] == '' ? null : $data['14'],
                    'admin'    => $data['15'] == '' ? null : $data['15'],
                    'provisi'    => $data['16'] == '' ? null : $data['16'],
                    'provisirp'    => $data['17'] == '' ? null : $data['17'],
                    'saldo'    => $data['18'] == '' ? null : $data['18'],
                    'status'    => $data['19'] == '' ? null : $data['19'],
                    'status_tagihan1'    => $data['20'] == '' ? null : $data['20'],
                    'status_admin'    => $data['21'] == '' ? null : $data['21'],
                    'status_asuransi'    => $data['22'] == '' ? null : $data['22'],
                    'status_provisi'    => $data['23'] == '' ? null : $data['23'],
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
