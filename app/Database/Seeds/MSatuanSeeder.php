<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MSatuanSeeder extends Seeder
{
    public function run(){
        $this->db->table('m_satuan')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/m_satuan.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 9999, ',')) !== false){
            if(!$transRow){
                $this->db->table('m_satuan')->insert([
                    'namasatuan'    => $data['1'],
                    'status'    => $data['2'],
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
