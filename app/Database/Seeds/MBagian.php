<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MBagian extends Seeder
{
    public function run()
    {
      $data = [
          [
              'kdbagian' => '1006007',
              'keterangan' => 'Bagian 1',
              'status' => 1,
          ],
          [
              'kdbagian' => '5008009',
              'keterangan' => 'Bagian 2',
              'status' => 1,
          ]
      ];
      $this->db->table('m_bagian')->insertBatch($data);
    }
}
