<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MBarangHargaSeeder extends Seeder
{
    public function run(){
        $this->db->table('m_barang_harga')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/m_barang_harga.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 9999, ',')) !== false){
            if(!$transRow){
                $this->db->table('m_barang_harga')->insert([
                    'idkategori'    => $data['1'],
                    'idbarang'    => $data['2'],
                    'idsatuan'    => $data['3'],
                    'harga'    => $data['4'],
                    'status'    => $data['5'],
                    'milik_gudang'    => $data['6'],
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
