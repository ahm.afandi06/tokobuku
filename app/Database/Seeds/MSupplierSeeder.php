<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MSupplierSeeder extends Seeder
{
    public function run(){
        $data = [
            [
                'namasupplier'  => 'PT Supplier 1',
                'kontak'  => 'Jakarta',
                'nohp'  => '081234567891',
                'npwp'  => '654322112',
                'pkp'  => 'Y',
                'telp'  => '021765432',
                'fax'  => '021765432',
                'email' => 'supplier1@mail.com',
                'idbank'  => '1',
                'norek'  => '99887654331',
                'status'  => '1',
            ],
            [
                'namasupplier'  => 'PT Supplier 2',
                'kontak'  => 'Jakarta',
                'nohp'  => '0811234565431',
                'npwp'  => '7896543211',
                'pkp'  => 'Y',
                'telp'  => '021654123',
                'fax'  => '021654123',
                'email' => 'supplier2@mail.com',
                'idbank'  => '1',
                'norek'  => '7865432111',
                'status'  => '1',
            ],
        ];
        $this->db->table('m_supplier')->insertBatch($data);
    }
}
