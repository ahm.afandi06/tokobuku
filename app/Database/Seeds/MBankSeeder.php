<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MBankSeeder extends Seeder
{
    public function run(){
        $this->db->table('m_bank')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/m_bank.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 1000, ',')) !== false){
            if(!$transRow){
                $this->db->table('m_bank')->insert([
                    'idbank'    => $data['0'],
                    'kdbank'    => $data['1'],
                    'namabank'    => $data['2'],
                    'status'    => $data['3']
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
