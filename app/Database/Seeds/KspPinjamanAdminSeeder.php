<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class KspPinjamanAdminSeeder extends Seeder
{
    public function run(){
        $this->db->table('ksp_pinjaman_admin')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/ksp_pinjaman_admin.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 1000, ',')) !== false){
            if(!$transRow){
                $this->db->table('ksp_pinjaman_admin')->insert([
                    'id_pinjaman'    => $data['1'] == '' ? null : $data['1'],
                    'biaya'    => $data['2'] == '' ? null : $data['2'],
                    'jumlah'    => $data['3'] == '' ? null : $data['3'],
                    'sampai'    => $data['4'] == '' ? null : $data['4'],
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
