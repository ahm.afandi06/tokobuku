<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class UserMobileSeeder extends Seeder
{
    public function run(){
        $data = [
            [
                'anggota_id'    => '1',
                'nokartu'    => '1363243735',
                'nama'    => 'Budi Sudarsono',
                'password'  => password_hash('123', PASSWORD_DEFAULT),
                'nohp'  => '08123456789',
                'status'    => 1
            ]
        ];
        $this->db->table('user_mobile')->insertBatch($data);
    }
}
