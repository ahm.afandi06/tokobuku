<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MLevelSeeder extends Seeder
{
    public function run(){
        $data = [
            [
                'namalevel' => 'Administrator',
                'status' => '1'
            ],
            [
                'namalevel' => 'Supervisor',
                'status' => '1'
            ],
            [
                'namalevel' => 'Kasir',
                'status' => '1'
            ],
        ];
        $this->db->table('m_level')->insertBatch($data);
    }
}
