<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MGolongan extends Seeder
{
    public function run()
    {
      $data = [
          [
              'kdgolongan' => '1667668',
              'namagolongan' => 'Golongan 1',
              'status' => 1,
          ],
          [
              'kdgolongan' => '5667668',
              'namagolongan' => 'Golongan 2',
              'status' => 1,
          ]
      ];
      $this->db->table('m_golongan')->insertBatch($data);
    }
}
