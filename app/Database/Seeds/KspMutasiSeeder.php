<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class KspMutasiSeeder extends Seeder
{
    public function run(){
        $this->db->table('ksp_mutasi')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/ksp_mutasi.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 1000, ',')) !== false){
            if(!$transRow){
                $this->db->table('ksp_mutasi')->insert([
                    // 'idmutasi'    => $data['0'],
                    'nomutasi'    => $data['1'] == '' ? null : $data['1'],
                    'idrekening'    => $data['2'] == '' ? null : $data['2'],
                    'tanggal'    => $data['3'] == '' ? null : $data['3'],
                    'keterangan'    => $data['4'] == '' ? null : $data['4'],
                    'debet'    => $data['5'] == '' ? null : $data['5'],
                    'kredit'    => $data['6'] == '' ? null : $data['6'],
                    'saldo'    => $data['7'] == '' ? null : $data['7'],
                    'tanggalupdate'    => $data['8'] == '' ? null : $data['8'],
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
