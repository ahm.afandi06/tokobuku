<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MultipaymentSeeder extends Seeder
{
    public function run(){
        $data = [
            [
                'gudang_id' => '2',
                'namaprov' => 'OVO',
                'harga_jual' => '0',
                'nominal' => '0',
                'status' => '1',
            ]
        ];
        $this->db->table('multipayment')->insertBatch($data);
    }
}
