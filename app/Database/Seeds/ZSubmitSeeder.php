<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class ZSubmitSeeder extends Seeder
{
    public function run(){
        $this->db->table('z_submit')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/z_submit.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 1000, ',')) !== false){
            if(!$transRow){
                $this->db->table('z_submit')->insert([
                    'id'    => $data['0'],
                    'controller'    => $data['1'],
                    'status'    => $data['2']
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
