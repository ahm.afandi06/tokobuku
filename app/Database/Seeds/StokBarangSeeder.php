<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class StokBarangSeeder extends Seeder
{
    public function run(){
        $this->db->table('stok_barang')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/stok_barang.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 9999, ',')) !== false){
            if(!$transRow){
                $this->db->table('stok_barang')->insert([
                    'rak_id'    => $data['1'],
                    'milik_gudang'    => $data['2'],
                    'barang'    => $data['3'],
                    'kd_barang'    => $data['4'],
                    'satuan'    => $data['5'],
                    'harga_pokok'    => $data['6'],
                    'jmlstok'    => $data['7'],
                    'minimal_stok'    => $data['8'],
                    'status_ppn'    => $data['9'],
                    'status'    => $data['10'],
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
