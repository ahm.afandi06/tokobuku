<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MGudangSeeder extends Seeder
{
    public function run(){
        $data = [
            [
                'idgudang'  => '1',
                'kdgudang'  => 'G-00001',
                'namagudang'  => 'Pemi Mart 1',
                'alamat'  => 'Jakarta Selatan',
                'inisial'  => 'PM1',
                'bukantoko'  => 'Y',
                'status'  => '1'
            ],
            [
                'idgudang'  => '2',
                'kdgudang'  => 'G-00002',
                'namagudang'  => 'Pemi Mart 2',
                'alamat'  => 'Jakarta Timur',
                'inisial'  => 'PM2',
                'bukantoko'  => 'N',
                'status'  => '1'
            ],
            [
                'idgudang'  => '3',
                'kdgudang'  => 'G-00003',
                'namagudang'  => 'Pemi Mart 3',
                'alamat'  => 'Jakarta Barat',
                'inisial'  => 'PM3',
                'bukantoko'  => 'N',
                'status'  => '1'
            ],
        ];
        $this->db->table('m_gudang')->insertBatch($data);
    }
}
