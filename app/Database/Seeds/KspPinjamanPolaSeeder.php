<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class KspPinjamanPolaSeeder extends Seeder
{
    public function run(){
        $this->db->table('ksp_pinjaman_pola')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/ksp_pinjaman_pola.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 1000, ',')) !== false){
            if(!$transRow){
                $this->db->table('ksp_pinjaman_pola')->insert([
                    'pola'    => $data['1'] == '' ? null : $data['1'],
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
