<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class SetakuntansiSeeder extends Seeder
{
    public function run(){
        $this->db->table('setakuntansi')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/setakuntansi.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 1000, ',')) !== false){
            if(!$transRow){
                $this->db->table('setakuntansi')->insert([
                    'id'    => $data['0'],
                    'keuangan_id'    => $data['1'],
                    'coa_id'    => $data['2'],
                    'posisi'    => $data['3'],
                    'relasi'    => $data['4'],
                    'status'    => $data['5']
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
