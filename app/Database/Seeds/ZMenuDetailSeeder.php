<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class ZMenuDetailSeeder extends Seeder
{
    public function run(){
        $this->db->table('z_menudetail')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/z_menudetail.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 1000, ",")) !== false){
            if(!$transRow){
                $this->db->table('z_menudetail')->insert([
                    'id'    => $data['0'],
                    'menu_id'    => $data['1'],
                    'level'    => $data['2'],
                    'status'    => $data['3']
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
