<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MJenisSumberBarangSeeder extends Seeder
{
    public function run(){
        $data = [
            [
                'keterangan'    => 'Bukan Titipan',
                'status'    => '1',
            ],
            [
                'keterangan'    => 'Titipan',
                'status'    => '1',
            ]
        ];
        $this->db->table('m_jenis_sumber_barang')->insertBatch($data);
    }
}
