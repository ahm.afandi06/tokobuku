<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MRakSeeder extends Seeder
{
    public function run(){
        $this->db->table('m_rak')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/m_rak.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 9999, ',')) !== false){
            if(!$transRow){
                $this->db->table('m_rak')->insert([
                    'milik_gudang'    => $data['1'],
                    'nama_rak'    => $data['2'],
                    'harga_sewa_anggota'    => $data['3'],
                    'harga_sewa_umum'    => $data['4'],
                    'status'    => $data['5'],
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
