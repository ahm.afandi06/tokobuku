<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class CoaRek2Seeder extends Seeder
{
    public function run(){
        $this->db->table('coa_rek2')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/coa_rek2.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 1000, ',')) !== false){
            if(!$transRow){
                $this->db->table('coa_rek2')->insert([
                    'idrek2'    => $data['0'],
                    'koderek2'    => $data['1'],
                    'namarek2'    => $data['2'],
                    'na'    => $data['3'],
                    'idrek1'    => $data['4'],
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
