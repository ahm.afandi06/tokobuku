<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class ZMenuSeeder extends Seeder
{
    public function run(){
        $this->db->table('z_menu')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/z_menu.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 1000, ",")) !== false){
            if(!$transRow){
                $this->db->table('z_menu')->insert([
                    'id'    => $data['0'],
                    'ikon'    => $data['1'],
                    'namamenu'    => $data['2'],
                    'path'    => $data['3'],
                    'asparent'    => $data['4'] == '' ? null : $data['4'],
                    'assub'    => $data['5'] == '' ? null : $data['5'],
                    'aschild'    => $data['6'] == '' ? null : $data['6'],
                    'parent'    => $data['7'] == '' ? null : $data['7'],
                    'urutan'    => $data['8'],
                    'route'    => $data['9'] == '' ? null : $data['9'],
                    'status'    => $data['10'],
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
