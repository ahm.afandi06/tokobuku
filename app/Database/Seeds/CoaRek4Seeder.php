<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class CoaRek4Seeder extends Seeder
{
    public function run(){
        $this->db->table('coa_rek4')->truncate();
        $csvData = fopen(APPPATH .'Database/csv/coa_rek4.csv', 'r');
        $transRow = true;

        while(($data = fgetcsv($csvData, 1000, ',')) !== false){
            if(!$transRow){
                $this->db->table('coa_rek4')->insert([
                    'idrek4'    => $data['0'],
                    'koderek4'    => $data['1'],
                    'namarek4'    => $data['2'],
                    'na'    => $data['3'],
                    'idrek3'    => $data['4'],
                ]);
            }
            $transRow = false;
        }
        fclose($csvData);
    }
}
