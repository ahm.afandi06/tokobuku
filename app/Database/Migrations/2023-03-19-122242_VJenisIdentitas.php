<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class VJenisIdentitas extends Migration
{
    private $nmview = 'v_jenis_identitas';

    public function up()
    {
        $headnya = "create ALGORITHM = UNDEFINED DEFINER =
    " . $this->db->username . "@" . $this->db->hostname . "
    SQL SECURITY DEFINER VIEW `" . $this->nmview . "` AS ";

        $this->db->query($headnya . "select
        'ANGGOTA' AS `jenis_identitas`
        FROM
            `anggota`
        WHERE
            (`anggota`.`kdstatus` = '1')
        GROUP BY `jenis_identitas`
        UNION ALL SELECT 'UMUM' AS `jenis_identitas`
        UNION ALL SELECT
            'SUPPLIER' AS `jenis_identitas`
        FROM
            `m_supplier`
        WHERE
            (`m_supplier`.`status` = '1')
        GROUP BY `jenis_identitas`
        UNION ALL SELECT
            'GUDANG' AS `jenis_identitas`
        FROM
            `m_gudang`
        WHERE
            (`m_gudang`.`status` = '1')
        GROUP BY `jenis_identitas`");
    }

    public function down()
    {
        $this->db->query('drop view if exists ' . $this->nmview);
    }
}
