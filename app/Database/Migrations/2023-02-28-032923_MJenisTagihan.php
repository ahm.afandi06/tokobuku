<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MJenisTagihan extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "idjenistagihan" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "kdjenistagihan" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "jenistagihan" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "status" => [
        "type" => "INT",
      ],
    ]);

    $this->forge->addPrimaryKey("idjenistagihan");
    $this->forge->createTable("m_jenis_tagihan");
  }

  public function down()
  {
    $this->forge->dropTable('m_jenis_tagihan');
  }
}
