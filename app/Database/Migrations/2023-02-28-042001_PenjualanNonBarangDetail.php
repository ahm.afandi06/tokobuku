<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class PenjualanNonBarangDetail extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'nonbarang' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'jumlah' => [
                'type' => 'INT',
                'null' => true
            ],
            'harga' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('penjualan_non_barang_detail');
    }

    public function down()
    {
        $this->forge->dropTable('penjualan_non_barang_detail');
    }
}
