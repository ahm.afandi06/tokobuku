<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MKategori extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "idkategori" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "namakategori" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "status" => [
        "type" => "INT",
      ],
    ]);

    $this->forge->addPrimaryKey("idkategori");
    $this->forge->createTable("m_kategori");
  }

  public function down()
  {
    $this->forge->dropTable('m_kategori');
  }
}
