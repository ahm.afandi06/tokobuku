<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MStatKdAnggota extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "idstatkdanggota" => [
        "type" => "INT",
        "auto_increment" => true
      ],
      "kdstatanggota" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "keterangan" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "status" => [
        "type" => "INT"
      ],
    ]);

    $this->forge->addPrimaryKey("idstatkdanggota");
    $this->forge->createTable("m_stat_kd_anggota");
  }

  public function down()
  {
    $this->forge->dropTable('m_stat_kd_anggota');
  }
}
