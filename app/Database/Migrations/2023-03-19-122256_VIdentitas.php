<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class VIdentitas extends Migration
{
    private $nmview = 'v_identitas';

    public function up()
    {
        $headnya = "create ALGORITHM = UNDEFINED DEFINER =
      " . $this->db->username . "@" . $this->db->hostname . "
      SQL SECURITY DEFINER VIEW `" . $this->nmview . "` AS ";

        $this->db->query($headnya . "select `anggota`.`idanggota` AS `id`,`anggota`.`nama` AS `nama`,'ANGGOTA' AS `jenis_identitas` from `anggota` where (`anggota`.`kdstatus` = '1') union all select `m_supplier`.`idsupplier` AS `id`,`m_supplier`.`namasupplier` AS `namasupplier`,'SUPPLIER' AS `jenis_identitas` from `m_supplier` where (`m_supplier`.`status` = '1') union all select `m_gudang`.`idgudang` AS `id`,`m_gudang`.`namagudang` AS `namagudang`,'GUDANG' AS `jenis_identitas` from `m_gudang` where (`m_gudang`.`status` = '1')");
    }

    public function down()
    {
        $this->db->query('drop view if exists ' . $this->nmview);
    }
}
