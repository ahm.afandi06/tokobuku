<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MStatAnggota extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "idstatanggota" => [
        "type" => "INT",
        "auto_increment" => true
      ],
      "kdstatus" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "keterangan" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "status" => [
        "type" => "INT"
      ],
    ]);

    $this->forge->addPrimaryKey("idstatanggota");
    $this->forge->createTable("m_stat_anggota");
  }

  public function down()
  {
    $this->forge->dropTable('m_stat_anggota');
  }
}
