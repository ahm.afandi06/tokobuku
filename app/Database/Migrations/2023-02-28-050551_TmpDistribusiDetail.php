<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class TmpDistribusiDetail extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "id" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "tmp_distribusi_id" => [
        "type" => "BIGINT",
      ],
      "barang" => [
        "type" => "BIGINT",
      ],
      "qty" => [
        "type" => "INT",
      ],
      "qty_terima" => [
        "type" => "INT",
      ],
      "satuan" => [
        "type" => "BIGINT",
      ],
      "harga" => [
        "type" => "INT",
      ],
      "harga_total" => [
        "type" => "BIGINT",
      ],
      "harga_total_datang" => [
        "type" => "BIGINT",
      ],
      "invoice_detail_id" => [
        "type" => "BIGINT",
      ],
    ]);

    $this->forge->addPrimaryKey("id");
    $this->forge->createTable("tmp_distribusi_detail");
  }

  public function down()
  {
    $this->forge->dropTable('tmp_distribusi_detail');
  }
}
