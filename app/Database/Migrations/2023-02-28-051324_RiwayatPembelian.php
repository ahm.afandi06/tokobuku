<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class RiwayatPembelian extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'pembelian' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'kredit_terbayar' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'kredit_sebelumnya' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'kredit_saat_ini' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'keterangan' => [
                'type' => 'TEXT',
                'null' => true
            ],
            'waktu_bayar' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'waktu_jatuh_tempo' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);

        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('riwayat_pembelian');
    }

    public function down()
    {
        $this->forge->dropTable('riwayat_pembelian');
    }
}
