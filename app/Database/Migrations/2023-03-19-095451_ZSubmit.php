<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ZSubmit extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'controller' => [
                'type' => 'TEXT',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('z_submit');
    }

    public function down()
    {
        $this->forge->dropTable('z_submit');
    }
}
