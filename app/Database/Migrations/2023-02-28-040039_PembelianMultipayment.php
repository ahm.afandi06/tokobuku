<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class PembelianMultipayment extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'provider' => [
                'type' => 'INT',
                'null' => true
            ],
            'jml_deposit' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'jml_saldo_sebelumnya' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'jml_saldo_saat_ini' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'harga' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'dicatat_oleh' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'waktu_beli' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);

        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('pembelian_multipayment');
    }

    public function down()
    {
        $this->forge->dropTable('pembelian_multipayment');
    }
}
