<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MLevel extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "id" => [
        "type" => "INT",
        "auto_increment" => true
      ],
      "namalevel" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "status" => [
        "type" => "INT",
      ],
    ]);

    $this->forge->addPrimaryKey("id");
    $this->forge->createTable("m_level");
  }

  public function down()
  {
    $this->forge->dropTable('m_level');
  }
}
