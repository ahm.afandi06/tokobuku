<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Multipayment extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'gudang_id' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'namaprov' => [
                'type' => 'VARCHAR',
                'null' => true,
                'constraint' => 255
            ],
            'harga_jual' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'nominal' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);

        $this->forge->addPrimaryKey("id");
        $this->forge->createTable("multipayment");
    }

    public function down()
    {
        $this->forge->dropTable('multipayment');
    }
}
