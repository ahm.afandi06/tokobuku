<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class KspSimpanan extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "id" => [
        "type" => "INT",
        "auto_increment" => true
      ],
      "jenis" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "awal_buka" => [
        "type" => "INT",
        "null" => true,
      ],
      "jumlah" => [
        "type" => "INT",
        "null" => true,
      ],
      "satuan_simpanan" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "biaya_admin" => [
        "type" => "INT",
        "null" => true,
      ],
      "waktu_simpan" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "waktu_tarik" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "kode" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "keterangan" => [
        "type" => "TEXT",
        "null" => true,
      ],
    ]);

    $this->forge->addPrimaryKey("id");
    $this->forge->createTable("ksp_simpanan");
  }

  public function down()
  {
    $this->forge->dropTable('ksp_simpanan');
  }
}
