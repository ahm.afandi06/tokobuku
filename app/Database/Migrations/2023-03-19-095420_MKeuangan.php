<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MKeuangan extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'nmtrans' => [
                'type' => 'TEXT',
                'null' => true
            ],
            'submit_id' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'fungsi' => [
                'type' => 'TEXT',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('m_keuangan');
    }

    public function down()
    {
        $this->forge->dropTable('m_keuangan');
    }
}
