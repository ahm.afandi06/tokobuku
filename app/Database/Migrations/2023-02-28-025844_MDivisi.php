<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MDivisi extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "iddivisi" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "kddivisi" => [
        "type" => "VARCHAR",
        "constraint" => 10,
        "null" => true
      ],
      "divisi" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "status" => [
        "type" => "INT",
      ],
    ]);

    $this->forge->addPrimaryKey("iddivisi");
    $this->forge->createTable("m_divisi");
  }

  public function down()
  {
    $this->forge->dropTable('m_divisi');
  }
}
