<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class VTitipanKomisi extends Migration
{
  private $nmview = 'v_titipan_komisi';

  public function up()
  {
    $headnya = "create ALGORITHM = UNDEFINED DEFINER =
    ".$this->db->username."@".$this->db->hostname."
    SQL SECURITY DEFINER VIEW `".$this->nmview."` AS ";

    $this->db->query($headnya."SELECT
        `a`.`barang` AS `barang`,
        SUM((`b`.`komisi` * `a`.`qty`)) AS `total`,
        `a`.`sumber_id_pemilik` AS `sumber_id_pemilik`,
        `a`.`waktu_jual` AS `waktu`
    FROM
        (`penjualan_detail` `a`
        LEFT JOIN (SELECT
            `riwayat_barang_titipan_detail`.`barang_titipan_id` AS `barang_titipan_id`,
                `riwayat_barang_titipan_detail`.`komisi` AS `komisi`
        FROM
            `riwayat_barang_titipan_detail`
        GROUP BY `riwayat_barang_titipan_detail`.`barang_titipan_id`) `b` ON ((`b`.`barang_titipan_id` = `a`.`barang`)))
    WHERE
        (`a`.`jenis_sumber_barang` = 2)");
  }

  public function down()
  {
    $this->db->query('drop view if exists '.$this->nmview);
  }
}
