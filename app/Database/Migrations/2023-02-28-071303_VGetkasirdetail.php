<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class VGetkasirdetail extends Migration
{
  private $nmview = 'v_getkasirdetail';

  public function up()
  {
    $headnya = "create ALGORITHM = UNDEFINED DEFINER =
    ".$this->db->username."@".$this->db->hostname."
    SQL SECURITY DEFINER VIEW `".$this->nmview."` AS ";

    $this->db->query($headnya."SELECT
        `mkd`.`id` AS `iddetail`,
        `mkd`.`kasir` AS `idkasir`,
        `mkd`.`waktu_open` AS `waktu_open`,
        `mkd`.`uang_tunai_open` AS `tunai_open`,
        `mkd`.`waktu_close` AS `waktu_close`,
        `mkd`.`uang_tunai_close` AS `tunai_close`,
        `mkd`.`uniq_periode` AS `uniq_periode`,
        IF(((`mkd`.`waktu_open` IS NOT NULL)
                AND (`mkd`.`waktu_close` IS NULL)),
            'Y',
            'N') AS `status_open`
    FROM
        `modal_kasir_detail` `mkd`
    ORDER BY `mkd`.`id`");
  }

  public function down()
  {
    $this->db->query('drop view if exists '.$this->nmview);
  }
}
