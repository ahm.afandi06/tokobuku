<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MBarangHarga extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "idbarangharga" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "idkategori" => [
        "type" => "BIGINT",
        'null' => true
      ],
      "idbarang" => [
        "type" => "BIGINT",
        'null' => true
      ],
      "idsatuan" => [
        "type" => "BIGINT",
        'null' => true
      ],
      "harga" => [
        "type" => "BIGINT",
        'null' => true
      ],
      "status" => [
        "type" => "INT",
      ],
      "milik_gudang" => [
        "type" => "BIGINT",
        'null' => true
      ],
    ]);

    $this->forge->addPrimaryKey("idbarangharga");
    $this->forge->createTable("m_barang_harga");
  }

  public function down()
  {
    $this->forge->dropTable('m_barang_harga');
  }
}
