<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MGudang extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "idgudang" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "kdgudang" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "namagudang" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "alamat" => [
        "type" => "TEXT",
      ],
      "inisial" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "bukantoko" => [
        "type" => "ENUM",
        "constraint" => ['Y','N'],
        "null" => true
      ],
      "status" => [
        "type" => "INT",
      ],
    ]);

    $this->forge->addPrimaryKey("idgudang");
    $this->forge->createTable("m_gudang");
  }

  public function down()
  {
    $this->forge->dropTable('m_gudang');
  }
}
