<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class VTitipanTerjual extends Migration
{
  private $nmview = 'v_titipan_terjual';

  public function up()
  {
    $headnya = "create ALGORITHM = UNDEFINED DEFINER =
    ".$this->db->username."@".$this->db->hostname."
    SQL SECURITY DEFINER VIEW `".$this->nmview."` AS ";

    $this->db->query($headnya."SELECT
        `rd`.`barang_titipan_id` AS `barang_titipan_id`,
        `b`.`nama_barang` AS `nama_barang`,
        `pd`.`sumber_id_pemilik` AS `sumber_id_pemilik`,
        `pd`.`waktu_jual` AS `waktu`,
        IF(((SELECT
                    SUM(`penjualan_detail`.`harga_promo`)
                FROM
                    `penjualan_detail`
                WHERE
                    (`penjualan_detail`.`barang` = `rd`.`barang_titipan_id`)) <> ''),
            (SELECT
                    SUM(`penjualan_detail`.`harga_promo`)
                FROM
                    `penjualan_detail`
                WHERE
                    (`penjualan_detail`.`barang` = `rd`.`barang_titipan_id`)),
            0) AS `total`
    FROM
        ((`riwayat_barang_titipan_detail` `rd`
        LEFT JOIN `penjualan_detail` `pd` ON (((`pd`.`barang` = `rd`.`barang_titipan_id`)
            AND (`pd`.`jenis_sumber_barang` = 2))))
        JOIN `m_barang_titipan` `b` ON ((`b`.`id` = `rd`.`barang_titipan_id`)))
    GROUP BY `rd`.`barang_titipan_id`");
  }

  public function down()
  {
    $this->db->query('drop view if exists '.$this->nmview);
  }
}
