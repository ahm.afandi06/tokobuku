<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class RiwayatBarangTitipan extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'no_faktur' => [
                'type' => 'VARCHAR',
                'null' => true,
                'constraint' => 255
            ],
            'member_titipan_id' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'kasir_penerima' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'waktu_titip' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'ket_status' => [
                'type' => 'TEXT',
                'null' => true
            ],
            'gudang' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('riwayat_barang_titipan');
    }

    public function down()
    {
        $this->forge->dropTable('riwayat_barang_titipan');
    }
}
