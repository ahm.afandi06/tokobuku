<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class KspPinjamanPola extends Migration
{
    public function up()
    {
      $this->forge->addField([
        "id" => [
          "type" => "INT",
          "auto_increment" => true
        ],
        "pola" => [
          "type" => "VARCHAR",
          "constraint" => 255,
          "null" => true
        ],
      ]);

      $this->forge->addPrimaryKey("id");
      $this->forge->createTable("ksp_pinjaman_pola");
    }

    public function down()
    {
      $this->forge->dropTable('ksp_pinjaman_pola');
    }
}
