<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class VMenusub extends Migration
{
  private $nmview = 'v_menusub';

  public function up()
  {
    $headnya = "create ALGORITHM = UNDEFINED DEFINER =
    ".$this->db->username."@".$this->db->hostname."
    SQL SECURITY DEFINER VIEW `".$this->nmview."` AS ";

    $this->db->query($headnya."SELECT
        `m`.`id` AS `id`,
        `m`.`ikon` AS `ikon`,
        `m`.`namamenu` AS `namamenu`,
        `m`.`path` AS `path`,
        `m`.`asparent` AS `asparent`,
        `m`.`assub` AS `assub`,
        `m`.`aschild` AS `aschild`,
        `m`.`parent` AS `parent`,
        `m`.`urutan` AS `urutan`,
        `m`.`route` AS `route`,
        `m`.`status` AS `status`,
        `d`.`id` AS `iddetail`,
        `d`.`level` AS `level`
    FROM
        (`z_menudetail` `d`
        JOIN `z_menu` `m` ON ((`m`.`id` = `d`.`menu_id`)))
    WHERE
        ((`m`.`assub` IS NOT NULL)
            AND (`d`.`status` = 1)
            AND (`m`.`status` = 1))
    ORDER BY `m`.`urutan`");
  }

  public function down()
  {
    $this->db->query('drop view if exists '.$this->nmview);
  }
}
