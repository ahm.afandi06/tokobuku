<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MJenisbarangmasuk extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "idjenisbarangmasuk" => [
        "type" => "INT",
        "auto_increment" => true
      ],
      "namajenis" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "status" => [
        "type" => "INT",
      ],
    ]);

    $this->forge->addPrimaryKey("idjenisbarangmasuk");
    $this->forge->createTable("m_jenisbarangmasuk");
  }

  public function down()
  {
    $this->forge->dropTable('m_jenisbarangmasuk');
  }
}
