<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ReqPoPenjualan extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'no_req_po' => [
                'type' => 'VARCHAR',
                'null' => true,
                'constraint' => 45
            ],
            'penjualan_id' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'no_anggota' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'waktu_req' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'ket_status' => [
                'type' => 'TEXT',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('req_po_penjualan');
    }

    public function down()
    {
        $this->forge->dropTable('req_po_penjualan');
    }
}
