<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class InvoiceDetail extends Migration
{
    public function up()
    {
      $this->forge->addField([
        "id" => [
          "type" => "BIGINT",
          "auto_increment" => true
        ],
        "invoice_id" => [
          "type" => "BIGINT",
        ],
        "tmp_distribusi_id" => [
          "type" => "BIGINT",
        ],
        "ke_gudang" => [
          "type" => "BIGINT",
        ],
        "ppn" => [
          "type" => "INT",
        ],
        "total_per_sj" => [
          "type" => "BIGINT",
        ],
        "nomor_surat_jalan" => [
          "type" => "VARCHAR",
          "constraint" => 255,
          "null" => true
        ],
      ]);

      $this->forge->addPrimaryKey("id");
      $this->forge->createTable("invoice_detail");
    }

    public function down()
    {
      $this->forge->dropTable('invoice_detail');
    }
}
