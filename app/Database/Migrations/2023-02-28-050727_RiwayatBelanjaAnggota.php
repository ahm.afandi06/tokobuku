<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class RiwayatBelanjaAnggota extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'penjualan' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'no_anggota' => [
                'type' => 'VARCHAR',
                'null' => true,
                'constraint' => 255
            ],
            'total_belanja' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'waktu_belanja' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('riwayat_belanja_anggota');
    }

    public function down()
    {
        $this->forge->dropTable('riwayat_belanja_anggota');
    }
}
