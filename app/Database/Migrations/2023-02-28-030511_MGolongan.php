<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MGolongan extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "idgolongan" => [
        "type" => "INT",
        "auto_increment" => true
      ],
      "kdgolongan" => [
        "type" => "VARCHAR",
        "constraint" => 10,
        "null" => true
      ],
      "namagolongan" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "status" => [
        "type" => "INT",
      ],
    ]);

    $this->forge->addPrimaryKey("idgolongan");
    $this->forge->createTable("m_golongan");
  }

  public function down()
  {
    $this->forge->dropTable('m_golongan');
  }
}
