<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MStatKerja extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "idstatkerja" => [
        "type" => "INT",
        "auto_increment" => true
      ],
      "kdstatkerja" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "keterangan" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "status" => [
        "type" => "INT"
      ],
    ]);

    $this->forge->addPrimaryKey("idstatkerja");
    $this->forge->createTable("m_stat_kerja");
  }

  public function down()
  {
    $this->forge->dropTable('m_stat_kerja');
  }
}
