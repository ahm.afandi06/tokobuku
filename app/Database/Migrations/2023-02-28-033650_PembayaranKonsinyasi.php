<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class PembayaranKonsinyasi extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'idriwayat' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'idbarang' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'namabarang' => [
                'type' => 'TEXT',
                'null' => true
            ],
            'stokterjual' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'stoksisa' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'nominal' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'komisi' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'gudang' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'akun' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'tgl' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('pembayaran_konsinyasi');
    }

    public function down()
    {
        $this->forge->dropTable('pembayaran_konsinyasi');
    }
}
