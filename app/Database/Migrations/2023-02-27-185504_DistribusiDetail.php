<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class DistribusiDetail extends Migration
{
    public function up()
    {
      $this->forge->addField([
        "id" => [
          "type" => "BIGINT",
          "auto_increment" => true
        ],
        "req_po" => [
          "type" => "BIGINT",
          "null" => true
        ],
        "distribusi" => [
          "type" => "BIGINT",
          "null" => true
        ],
        "kd_barang" => [
          "type" => "VARCHAR",
          "constraint" => 45,
          "null" => true
        ],
        "qty_req" => [
          "type" => "INT",
        ],
        "qty_terima" => [
          "type" => "INT",
        ],
        "satuan" => [
          "type" => "INT",
        ],
        "harga_beli" => [
          "type" => "BIGINT",
        ],
        "harga_datang" => [
          "type" => "BIGINT",
        ],
        "ppn_persen" => [
          "type" => "INT",
        ],
        "ppn_nominal" => [
          "type" => "BIGINT",
        ],
        "status" => [
          "type" => "INT",
        ],
      ]);

      $this->forge->addPrimaryKey("id");
      $this->forge->createTable("distribusi_detail");
    }

    public function down()
    {
      $this->forge->dropTable('distribusi_detail');
    }
}
