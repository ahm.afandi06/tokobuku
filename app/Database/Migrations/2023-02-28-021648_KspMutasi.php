<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class KspMutasi extends Migration
{
    public function up()
    {
      $this->forge->addField([
        "idmutasi" => [
          "type" => "INT",
          "auto_increment" => true
        ],
        "nomutasi" => [
          "type" => "VARCHAR",
          "constraint" => 255,
          "null" => true,
        ],
        "idrekening" => [
          "type" => "INT",
          "null" => true,
        ],
        "tanggal" => [
          "type" => "DATE",
          "null" => true,
        ],
        "keterangan" => [
          "type" => "VARCHAR",
          "constraint" => 255,
          "null" => true,
        ],
        "debet" => [
          "type" => "BIGINT",
          "null" => true,
        ],
        "kredit" => [
          "type" => "BIGINT",
          "null" => true,
        ],
        "saldo" => [
          "type" => "BIGINT",
          "null" => true,
        ],
        "tanggalupdate" => [
          "type" => "DATETIME",
          "null" => true,
        ],
      ]);

      $this->forge->addPrimaryKey("idmutasi");
      $this->forge->createTable("ksp_mutasi");
    }

    public function down()
    {
      $this->forge->dropTable('ksp_mutasi');
    }
}
