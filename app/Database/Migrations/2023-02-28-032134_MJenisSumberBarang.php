<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MJenisSumberBarang extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "id" => [
        "type" => "INT",
        "auto_increment" => true
      ],
      "keterangan" => [
        "type" => "TEXT",
      ],
      "status" => [
        "type" => "INT",
      ],
    ]);

    $this->forge->addPrimaryKey("id");
    $this->forge->createTable("m_jenis_sumber_barang");
  }

  public function down()
  {
    $this->forge->dropTable('m_jenis_sumber_barang');
  }
}
