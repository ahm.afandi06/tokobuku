<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Anggota extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "idanggota" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "nokartu" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "nokta" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "norfid" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "reganggota" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "kdkartu" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "kdtagih" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "nik" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "nama" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "seks" => [
        "type" => "INT",
        "null" => true
      ],
      "tmplahir" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "tgllahir" => [
        "type" => "DATE",
        "null" => true
      ],
      "alamat" => [
        "type" => "TEXT",
        "null" => true
      ],
      "kota" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "divisi" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "golongan" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "bagian" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "tglmasuk" => [
        "type" => "DATE",
        "null" => true
      ],
      "tglkeluar" => [
        "type" => "DATE",
        "null" => true
      ],
      "tglubah" => [
        "type" => "DATETIME",
        "null" => true
      ],
      "ubahke" => [
        "type" => "INT",
        "null" => true
      ],
      "maxlimit" => [
        "type" => "BIGINT",
        "null" => true
      ],
      "maxmlg" => [
        "type" => "BIGINT",
        "null" => true
      ],
      "maxkredit" => [
        "type" => "BIGINT",
        "null" => true
      ],
      "kdstatus" => [
        "type" => "INT",
        "null" => true
      ],
      "kdkerja" => [
        "type" => "INT",
        "null" => true
      ],
      "kdanggota" => [
        "type" => "INT",
        "null" => true
      ],
      "blokir" => [
        "type" => "ENUM",
        "constraint" => ['Y', 'N'],
      ],
      "handphone" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "ktp" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "idbank" => [
        "type" => "BIGINT",
        "null" => true
      ],
      "rekening" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "nmrekening" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "pathphoto" => [
        "type" => "TEXT",
        "null" => true
      ],
      "email" => [
        "type" => "TEXT",
        "constraint" => 255,
        "null" => true
      ],
      "gapok" => [
        "type" => "BIGINT",
        "null" => true
      ],
      "paguksp" => [
        "type" => "DECIMAL",
        "constraint" => [10, 0],
        "null" => true
      ],
      "pagutoko" => [
        "type" => "DECIMAL",
        "constraint" => [10, 0],
        "null" => true
      ],
      "pagumlg" => [
        "type" => "DECIMAL",
        "constraint" => [10, 0],
        "null" => true
      ],
      "jemputan" => [
        "type" => "INT",
        "null" => true
      ],
      "opid" => [
        "type" => "BIGINT",
        "null" => true
      ],
    ]);

    $this->forge->addPrimaryKey("idanggota");
    $this->forge->createTable("anggota");
  }

  public function down()
  {
    $this->forge->dropTable('anggota');
  }
}
