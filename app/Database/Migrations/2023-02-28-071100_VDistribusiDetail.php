<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class VDistribusiDetail extends Migration
{
  private $nmview = 'v_distribusi_detail';

  public function up()
  {
    $headnya = "create ALGORITHM = UNDEFINED DEFINER =
    ".$this->db->username."@".$this->db->hostname."
    SQL SECURITY DEFINER VIEW `".$this->nmview."` AS ";

    $this->db->query($headnya."SELECT
        `dd`.`distribusi` AS `distribusi`,
        `mb`.`idbarang` AS `idbarang`,
        `dd`.`kd_barang` AS `kodebarang`,
        `mb`.`namabarang` AS `namabarang`,
        `dd`.`qty_req` AS `qty_req`,
        `sb`.`harga_pokok` AS `harga_pokok`,
        `dd`.`qty_terima` AS `qty_terima`,
        `dd`.`harga_datang` AS `totalbeli`,
        `dd`.`ppn_persen` AS `ppn_persen`,
        `dd`.`ppn_nominal` AS `ppn_nominal`,
        `sb`.`jmlstok` AS `jmlstok`,
        `dd`.`status` AS `status`
    FROM
        (((`distribusi_detail` `dd`
        JOIN `distribusi` `d` ON ((`d`.`id` = `dd`.`distribusi`)))
        JOIN `stok_barang` `sb` ON ((`sb`.`milik_gudang` = `d`.`gudang_pengirim`)))
        JOIN `m_barang` `mb` ON ((`mb`.`kodebarang` = `dd`.`kd_barang`)))
    WHERE
        (`dd`.`status` = 1)");
  }

  public function down()
  {
    $this->db->query('drop view if exists '.$this->nmview);
  }
}
