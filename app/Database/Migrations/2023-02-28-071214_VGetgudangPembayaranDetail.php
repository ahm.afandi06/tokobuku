<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class VGetgudangPembayaranDetail extends Migration
{
  private $nmview = 'v_getgudang_pemabyaran_detail';

  public function up()
  {
    $headnya = "create ALGORITHM = UNDEFINED DEFINER =
    ".$this->db->username."@".$this->db->hostname."
    SQL SECURITY DEFINER VIEW `".$this->nmview."` AS ";

    $this->db->query($headnya."SELECT
        `a`.`id` AS `id`,
        `a`.`tgl_do` AS `tgl_do`,
        `a`.`surat_jalan` AS `surat_jalan`,
        `a`.`req_po_id` AS `req_po_id`,
        `a`.`amount` AS `amount`,
        `a`.`status_terima` AS `status_terima`,
        `a`.`qty_datang` AS `qty_datang`,
        `a`.`harga_datang` AS `harga_datang`,
        `a`.`ke_gudang` AS `ke_gudang`,
        `a`.`gudang_pengirim` AS `gudang_pengirim`,
        `b`.`no_req_po` AS `no_req_po`
    FROM
        (`tmp_distribusi` `a`
        JOIN `req_po` `b` ON ((`b`.`id` = `a`.`req_po_id`)))
    WHERE
        (`a`.`status_terima` = 1)");
  }

  public function down()
  {
    $this->db->query('drop view if exists '.$this->nmview);
  }
}
