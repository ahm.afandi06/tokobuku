<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class VGetkasir extends Migration
{
  private $nmview = 'v_getkasir';

  public function up()
  {
    $headnya = "create ALGORITHM = UNDEFINED DEFINER =
    ".$this->db->username."@".$this->db->hostname."
    SQL SECURITY DEFINER VIEW `".$this->nmview."` AS ";

    $this->db->query($headnya."SELECT
        `mk`.`id` AS `idmodal`,
        `mkd`.`id` AS `iddetail`,
        `u`.`id` AS `idkasir`,
        `u`.`nama` AS `namakasir`,
        `u`.`gudang` AS `idgudang`,
        `g`.`namagudang` AS `namagudang`,
        `mk`.`uniq_periode` AS `uniq_periode`,
        IF((`mk`.`open` = 'Y'), 'Y', 'N') AS `status_open`,
        `mkd`.`uang_tunai_open` AS `tunai_open`,
        `mkd`.`uang_tunai_close` AS `tunai_close`,
        `mkd`.`waktu_open` AS `waktu_open`,
        `mkd`.`waktu_close` AS `waktu_close`,
        `u2`.`nama` AS `namasupervisor`
    FROM
        ((((`user_login` `u`
        JOIN `m_gudang` `g` ON ((`g`.`idgudang` = `u`.`gudang`)))
        LEFT JOIN `modal_kasir` `mk` ON ((`mk`.`kasir` = `u`.`id`)))
        LEFT JOIN `modal_kasir_detail` `mkd` ON (((`mkd`.`uniq_periode` = `mk`.`uniq_periode`)
            AND (`mkd`.`uang_tunai_open` IS NOT NULL))))
        LEFT JOIN `user_login` `u2` ON ((`u2`.`id` = `mkd`.`supervisor`)))
    WHERE
        (`u`.`level` = 3)");
  }

  public function down()
  {
    $this->db->query('drop view if exists '.$this->nmview);
  }
}
