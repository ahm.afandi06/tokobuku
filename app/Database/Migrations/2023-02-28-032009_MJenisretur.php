<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MJenisretur extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "idjenisretur" => [
        "type" => "INT",
        "auto_increment" => true
      ],
      "keterangan" => [
        "type" => "TEXT",
      ],
      "status" => [
        "type" => "INT",
      ],
    ]);

    $this->forge->addPrimaryKey("idjenisretur");
    $this->forge->createTable("m_jenisretur");
  }

  public function down()
  {
    $this->forge->dropTable('m_jenisretur');
  }
}
