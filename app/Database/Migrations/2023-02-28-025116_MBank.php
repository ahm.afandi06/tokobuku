<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MBank extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "idbank" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "kdbank" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "namabank" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "status" => [
        "type" => "INT",
      ],
    ]);

    $this->forge->addPrimaryKey("idbank");
    $this->forge->createTable("m_bank");
  }

  public function down()
  {
    $this->forge->dropTable('m_bank');
  }
}
