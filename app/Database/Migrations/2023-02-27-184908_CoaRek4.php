<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CoaRek4 extends Migration
{
    public function up()
    {
      $this->forge->addField([
        "idrek4" => [
          "type" => "INT",
          "auto_increment" => true
        ],
        "koderek4" => [
          "type" => "VARCHAR",
          "constraint" => 10,
          "null" => true
        ],
        "namarek4" => [
          "type" => "VARCHAR",
          "constraint" => 255,
          "null" => true
        ],
        "na" => [
          "type" => "ENUM",
          "constraint" => ['N','Y'],
          "null" => true
        ],
        "idrek3" => [
          "type" => "INT",
        ],
        "jenis" => [
          "type" => "INT",
          "constraint" => 255,
          "null" => true
        ],
      ]);

      $this->forge->addPrimaryKey("idrek4");
      $this->forge->createTable("coa_rek4");
    }

    public function down()
    {
      $this->forge->dropTable('coa_rek4');
    }
}
