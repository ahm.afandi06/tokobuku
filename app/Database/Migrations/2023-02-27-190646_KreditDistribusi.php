<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class KreditDistribusi extends Migration
{
    public function up()
    {
      $this->forge->addField([
        "id" => [
          "type" => "BIGINT",
          "auto_increment" => true
        ],
        "tmp_distribusi_id" => [
          "type" => "BIGINT",
        ],
        "ke_gudang" => [
          "type" => "BIGINT",
        ],
        "distribusi" => [
          "type" => "BIGINT",
        ],
        "kredit_terbayar" => [
          "type" => "BIGINT",
        ],
        "kredit_sebelumnya" => [
          "type" => "BIGINT",
        ],
        "kredit_saat_ini" => [
          "type" => "BIGINT",
        ],
        "keterangan" => [
          "type" => "TEXT",
        ],
        "waktu_bayar" => [
          "type" => "DATETIME",
        ],
        "status" => [
          "type" => "INT",
        ],
        "invoice_detail_id" => [
          "type" => "BIGINT",
        ],
      ]);

      $this->forge->addPrimaryKey("id");
      $this->forge->createTable("kredit_distribusi");
    }

    public function down()
    {
      $this->forge->dropTable('kredit_distribusi');
    }
}
