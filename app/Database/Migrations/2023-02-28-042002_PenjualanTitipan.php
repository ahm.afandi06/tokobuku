<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class PenjualanTitipan extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'idriwayat' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'idriwayatdetail' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'idpenjualan' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'idgudang' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'idbarang' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'qtyterjual' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'nominal' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'komisi' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'waktu' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('penjualan_titipan');
    }

    public function down()
    {
        $this->forge->dropTable('penjualan_titipan');
    }
}
