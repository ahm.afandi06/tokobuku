<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ModalKasirDetail extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "id" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "kasir" => [
        "type" => "BIGINT",
      ],
      "uniq_periode" => [
        "type" => "TEXT",
      ],
      "uang_tunai_open" => [
        "type" => "BIGINT",
      ],
      "uang_tunai_close" => [
        "type" => "BIGINT",
        "null" => true
      ],
      "waktu_open" => [
        "type" => "DATETIME",
      ],
      "waktu_close" => [
        "type" => "DATETIME",
        "null" => true
      ],
      "supervisor" => [
        "type" => "BIGINT",
      ]
    ]);

    $this->forge->addPrimaryKey("id");
    $this->forge->createTable("modal_kasir_detail");
  }

  public function down()
  {
    $this->forge->dropTable('modal_kasir_detail');
  }
}
