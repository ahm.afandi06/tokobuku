<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Invoice extends Migration
{
    public function up()
    {
      $this->forge->addField([
        "id" => [
          "type" => "BIGINT",
          "auto_increment" => true
        ],
        "nomor_invoice" => [
          "type" => "VARCHAR",
          "constraint" => 255,
          "null" => true
        ],
        "tanggal_buat" => [
          "type" => "DATETIME",
        ],
        "status" => [
          "type" => "INT",
        ],
        "sub_total" => [
          "type" => "INT",
        ],
        "ppn" => [
          "type" => "INT",
        ],
        "total" => [
          "type" => "INT",
        ],
        "gudang_id" => [
          "type" => "BIGINT",
        ],
      ]);

      $this->forge->addPrimaryKey("id");
      $this->forge->createTable("invoice");
    }

    public function down()
    {
      $this->forge->dropTable('invoice');
    }
}
