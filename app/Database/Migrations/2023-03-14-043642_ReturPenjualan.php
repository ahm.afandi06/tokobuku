<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ReturPenjualan extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'id_penjualan' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'id_penjualan_detail' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'no_faktur' => [
                'type' => 'VARCHAR',
                'null' => true,
                'constraint' => 45
            ],
            'gudang' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'id_barang' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'kasir' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'jenis_sumber_barang' => [
                'type' => 'INT',
                'null' => true
            ],
            'sumber_id_pemilik' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'qty' => [
                'type' => 'INT',
                'null' => true
            ],
            'waktu_jual' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'waktu_retur' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('retur_penjualan');
    }

    public function down()
    {
        $this->forge->dropTable('retur_penjualan');
    }
}
