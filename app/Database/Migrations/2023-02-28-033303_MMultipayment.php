<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MMultipayment extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "id" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "namaprovider" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "saldo" => [
        "type" => "BIGINT",
      ],
      "harga_jual" => [
        "type" => "BIGINT",
      ],
      "status" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
    ]);

    $this->forge->addPrimaryKey("id");
    $this->forge->createTable("m_multipayment");
  }

  public function down()
  {
    $this->forge->dropTable('m_multipayment');
  }
}
