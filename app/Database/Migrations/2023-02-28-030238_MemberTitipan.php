<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MemberTitipan extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "id" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "namamember" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "jenismember" => [
        "type" => "INT",
      ],
      "waktu_registrasi" => [
        "type" => "DATETIME",
      ],
      "alamat" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "telp" => [
        "type" => "TEXT",
      ],
      "status" => [
        "type" => "INT",
      ],
    ]);

    $this->forge->addPrimaryKey("id");
    $this->forge->createTable("member_titipan");
  }

  public function down()
  {
    $this->forge->dropTable('member_titipan');
  }
}
