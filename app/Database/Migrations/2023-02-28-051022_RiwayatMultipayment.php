<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class RiwayatMultipayment extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'multipayment_id' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'tgl_tercatat' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'nominal_trans' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'nominal_sebelum' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'nominal_saat_ini' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'sebagai' => [
                'type' => 'INT',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ],
            'gudang' => [
                'type' => 'BIGINT',
                'null' => true
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('riwayat_multipayment');
    }

    public function down()
    {
        $this->forge->dropTable('riwayat_multipayment');
    }
}
