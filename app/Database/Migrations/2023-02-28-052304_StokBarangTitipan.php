<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class StokBarangTitipan extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'rak_id' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'tgl_sewa_rak' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'tgl_habis_rak' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'harga_sewa_rak' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'member_titipan' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'barang_titipan' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'jmlstok' => [
                'type' => 'INT',
                'null' => true
            ],
            'harga_jual' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'status_ppn' => [
                'type' => 'ENUM',
                'null' => true,
                'constraint' => ['Y', 'N']
            ],
            'kd_barang_titipan' => [
                'type' => 'VARCHAR',
                'null' => true,
                'constraint' => 45
            ],
            'komisi_persen' => [
                'type' => 'INT',
                'null' => true
            ],
            'komisi_nominal' => [
                'type' => 'INT',
                'null' => true
            ],
            'gudang' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('stok_barang_titipan');
    }

    public function down()
    {
        $this->forge->dropTable('stok_barang_titipan');
    }
}
