<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ZMenu extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "id" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "ikon" => [
        "type" => "TEXT",
      ],
      "namamenu" => [
        "type" => "TEXT",
      ],
      "path" => [
        "type" => "TEXT",
      ],
      "asparent" => [
        "type" => "INT",
        "null" => true,
      ],
      "assub" => [
        "type" => "INT",
        "null" => true,
      ],
      "aschild" => [
        "type" => "INT",
        "null" => true,
      ],
      "parent" => [
        "type" => "BIGINT",
        "null" => true,
      ],
      "urutan" => [
        "type" => "INT",
      ],
      "route" => [
        "type" => "TEXT",
        "null" => true,
      ],
      "status" => [
        "type" => "INT",
      ],
    ]);

    $this->forge->addPrimaryKey("id");
    $this->forge->createTable("z_menu");
  }

  public function down()
  {
    $this->forge->dropTable('z_menu');
  }
}
