<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UserLogin extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "id" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "gudang" => [
        "type" => "INT",
      ],
      "nama" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "username" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "password" => [
        "type" => "VARCHAR",
        "constraint" => 64,
        "null" => true
      ],
      "foto" => [
        "type" => "TEXT",
      ],
      "level" => [
        "type" => "INT",
      ],
      "terdaftar" => [
        "type" => "DATETIME",
      ],
      "status" => [
        "type" => "INT",
      ],
    ]);

    $this->forge->addPrimaryKey("id");
    $this->forge->createTable("user_login");
  }

  public function down()
  {
    $this->forge->dropTable('user_login');
  }
}
