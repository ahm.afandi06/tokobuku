<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CoaRek3 extends Migration
{
    public function up()
    {
      $this->forge->addField([
        "idrek3" => [
          "type" => "INT",
          "auto_increment" => true
        ],
        "koderek3" => [
          "type" => "VARCHAR",
          "constraint" => 10,
          "null" => true
        ],
        "namarek3" => [
          "type" => "VARCHAR",
          "constraint" => 255,
          "null" => true
        ],
        "na" => [
          "type" => "ENUM",
          "constraint" => ['N','Y'],
          "null" => true
        ],
        "idrek2" => [
          "type" => "INT",
        ],
      ]);

      $this->forge->addPrimaryKey("idrek3");
      $this->forge->createTable("coa_rek3");
    }

    public function down()
    {
      $this->forge->dropTable('coa_rek3');
    }
}
