<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class TmpDistribusi extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "id" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "tgl_do" => [
        "type" => "DATE",
      ],
      "surat_jalan" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "req_po_id" => [
        "type" => "BIGINT",
      ],
      "amount" => [
        "type" => "INT",
      ],
      "status_terima" => [
        "type" => "INT",
      ],
      "qty_datang" => [
        "type" => "INT",
      ],
      "harga_datang" => [
        "type" => "INT",
      ],
      "harga_datang" => [
        "type" => "BIGINT",
      ],
      "ke_gudang" => [
        "type" => "BIGINT",
      ],
      "gudang_pengirim" => [
        "type" => "BIGINT",
      ],
      "status_lunas" => [
        "type" => "ENUM",
        "constraint" => ['Y', 'N'],
        "null" => true
      ],
      "status_invoice" => [
        "type" => "INT",
      ],
      "no_faktur_terima" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "penjualan_id" => [
        "type" => "BIGINT",
        "null" => true
      ],
    ]);

    $this->forge->addPrimaryKey("id");
    $this->forge->createTable("tmp_distribusi");
  }

  public function down()
  {
    $this->forge->dropTable('tmp_distribusi');
  }
}
