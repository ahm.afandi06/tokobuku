<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class TmpBarangOrder extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "id" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "reqpo" => [
        "type" => "BIGINT",
      ],
      "tanggal_request" => [
        "type" => "DATE",
      ],
      "user" => [
        "type" => "INT",
      ],
    ]);

    $this->forge->addPrimaryKey("id");
    $this->forge->createTable("tmp_barang_order");
  }

  public function down()
  {
    $this->forge->dropTable('tmp_barang_order');
  }
}
