<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MJenisKartu extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "idjeniskartu" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "kdjeniskartu" => [
        "type" => "VARCHAR",
        "constraint" => 3,
        "null" => true
      ],
      "jeniskartu" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "status" => [
        "type" => "INT",
      ],
    ]);

    $this->forge->addPrimaryKey("idjeniskartu");
    $this->forge->createTable("m_jenis_kartu");
  }

  public function down()
  {
    $this->forge->dropTable('m_jenis_kartu');
  }
}
