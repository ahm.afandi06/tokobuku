<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class StokOpname extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'milik_gudang' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'barang' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'satuan' => [
                'type' => 'INT',
                'null' => true
            ],
            'jml_transaksi' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'jml_sebelumnya' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'jml_saat_ini' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'waktu_transaksi' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'sebagai_barang' => [
                'type' => 'INT',
                'null' => true
            ],
            'keterangan' => [
                'type' => 'TEXT',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('stokopname');
    }

    public function down()
    {
        $this->forge->dropTable('stokopname');
    }
}
