<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Pembelian extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'no_faktur' => [
                'type' => 'VARCHAR',
                'null' => true,
                'constraint' => 45
            ],
            'suplier' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'gudangku' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'ppn_kirim_persen' => [
                'type' => 'INT',
                'null' => true
            ],
            'ppn_kirim_nominal' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'total_harga' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'waktu_terima' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'waktu_lunas' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'status_lunas' => [
                'type' => 'ENUM',
                'null' => true,
                'constraint' => ['Y', 'N']
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);

        $this->forge->addPrimaryKey("id");
        $this->forge->createTable("pembelian");
    }

    public function down()
    {
        $this->forge->dropTable("pembelian");
    }
}
