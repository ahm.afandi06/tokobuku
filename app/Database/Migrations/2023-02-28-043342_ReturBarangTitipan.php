<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ReturBarangTitipan extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'barang_titipan' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'qty' => [
                'type' => 'INT',
                'null' => true
            ],
            'milik_member' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'waktu_retur' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('retur_barang_titipan');
    }

    public function down()
    {
        $this->forge->dropTable('retur_barang_titipan');
    }
}
