<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class VCoa extends Migration
{
    private $nmview = 'v_coa';

    public function up()
    {
      $headnya = "create ALGORITHM = UNDEFINED DEFINER =
      ".$this->db->username."@".$this->db->hostname."
      SQL SECURITY DEFINER VIEW `".$this->nmview."` AS ";

      $this->db->query($headnya."select `a`.`idrek1` AS `idrek1`,`a`.`koderek1` AS `kdrek1`,`a`.`namarek1` AS `namarek1`,`b`.`idrek2` AS `idrek2`,`b`.`koderek2` AS `kdrek2`,`b`.`namarek2` AS `namarek2`,`c`.`idrek3` AS `idrek3`,`c`.`koderek3` AS `kdrek3`,`c`.`namarek3` AS `namarek3`,`d`.`idrek4` AS `idrek4`,`d`.`koderek4` AS `kdrek4`,concat(`a`.`koderek1`,' ',`b`.`koderek2`,' ',`c`.`koderek3`,' ',if((length(`d`.`koderek4`) = 1),concat('0',`d`.`koderek4`),`d`.`koderek4`)) AS `kd_rek`,`d`.`namarek4` AS `namarek4`,`d`.`jenis` AS `jenis` from (((`coa_rek1` `a` join `coa_rek2` `b`) join `coa_rek3` `c`) join `coa_rek4` `d`) where ((`a`.`idrek1` = `b`.`idrek1`) and (`b`.`idrek2` = `c`.`idrek2`) and (`c`.`idrek3` = `d`.`idrek3`) and (`a`.`na` = 'N') and (`b`.`na` = 'N') and (`c`.`na` = 'N') and (`d`.`na` = 'N')) order by concat(`a`.`koderek1`,`b`.`koderek2`,`c`.`koderek3`,`d`.`koderek4`)");
    }

    public function down()
    {
      $this->db->query('drop view if exists '.$this->nmview);
    }
}
