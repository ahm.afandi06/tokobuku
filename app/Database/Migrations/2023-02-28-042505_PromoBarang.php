<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class PromoBarang extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'barang' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'satuan' => [
                'type' => 'INT',
                'null' => true
            ],
            'jenis_sumber_barang' => [
                'type' => 'INT',
                'null' => true
            ],
            'sumber_id_pemilik' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'harga_jual' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'harga_promo' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('promo_barang');
    }

    public function down()
    {
        $this->forge->dropTable('promo_barang');
    }
}
