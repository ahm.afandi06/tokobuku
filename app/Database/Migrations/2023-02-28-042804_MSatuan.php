<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MSatuan extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "idsatuan" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "namasatuan" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "status" => [
        "type" => "INT",
      ],
    ]);

    $this->forge->addPrimaryKey("idsatuan");
    $this->forge->createTable("m_satuan");
  }

  public function down()
  {
    $this->forge->dropTable('m_satuan');
  }
}
