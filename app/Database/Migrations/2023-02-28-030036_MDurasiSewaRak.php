<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MDurasiSewaRak extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "id" => [
        "type" => "INT",
        "auto_increment" => true
      ],
      "rak_id" => [
        "type" => "BIGINT",
      ],
      "riwayat_barang_titipan_id" => [
        "type" => "BIGINT",
      ],
      "riwayat_barang_titipan_detail_id" => [
        "type" => "BIGINT",
      ],
      "keterangan" => [
        "type" => "TEXT",
      ],
      "tgl_sewa" => [
        "type" => "DATETIME",
      ],
      "tgl_berakhir" => [
        "type" => "DATETIME",
      ],
      "durasihari" => [
        "type" => "INT",
      ],
      "status" => [
        "type" => "INT",
      ],
    ]);

    $this->forge->addPrimaryKey("id");
    $this->forge->createTable("m_durasi_sewa_rak");
  }

  public function down()
  {
    $this->forge->dropTable('m_durasi_sewa_rak');
  }
}
