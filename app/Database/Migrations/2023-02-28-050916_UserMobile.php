<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UserMobile extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "id" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "anggota_id" => [
        "type" => "BIGINT",
      ],
      "nokartu" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "nama" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "password" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "nohp" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "status" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
    ]);

    $this->forge->addPrimaryKey("id");
    $this->forge->createTable("user_mobile");
  }

  public function down()
  {
    $this->forge->dropTable('user_mobile');
  }
}
