<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class PenjualanDetail extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'auto_increment' => true
            ],
            'penjualan' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'barang' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'harga' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'harga_promo' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'qty' => [
                'type' => 'INT',
                'null' => true
            ],
            'satuan' => [
                'type' => 'INT',
                'null' => true
            ],
            'ppn_persen' => [
                'type' => 'INT',
                'null' => true
            ],
            'ppn_nominal' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'jenis_sumber_barang' => [
                'type' => 'INT',
                'null' => true
            ],
            'sumber_id_pemilik' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'waktu_jual' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ],
            'waktu_kirim' => [
                'type' => 'DATETIME',
                'null' => true
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('penjualan_detail');
    }

    public function down()
    {
        $this->forge->dropTable('penjualan_detail');
    }
}
