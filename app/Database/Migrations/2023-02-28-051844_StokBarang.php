<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class StokBarang extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'rak_id' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'milik_gudang' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'barang' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'kd_barang' => [
                'type' => 'VARCHAR',
                'null' => true,
                'constraint' => 45
            ],
            'satuan' => [
                'type' => 'INT',
                'null' => true
            ],
            'harga_pokok' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'jmlstok' => [
                'type' => 'INT',
                'null' => true
            ],
            'minimal_stok' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'status_ppn' => [
                'type' => 'ENUM',
                'null' => true,
                'constraint' => ['Y', 'N']
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);

        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('stok_barang');
    }

    public function down()
    {
        $this->forge->dropTable('stok_barang');
    }
}
