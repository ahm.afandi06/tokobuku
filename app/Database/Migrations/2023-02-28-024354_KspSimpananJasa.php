<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class KspSimpananJasa extends Migration
{
    public function up()
    {
      $this->forge->addField([
        "id" => [
          "type" => "INT",
          "auto_increment" => true
        ],
        "id_simpanan" => [
          "type" => "INT",
          "null" => true,
        ],
        "jasa" => [
          "type" => "INT",
          "null" => true,
        ],
        "lama" => [
          "type" => "INT",
          "null" => true,
        ],
        "waktu" => [
          "type" => "VARCHAR",
          "constraint" => 255,
          "null" => true
        ],
      ]);

      $this->forge->addPrimaryKey("id");
      $this->forge->createTable("ksp_simpanan_jasa");
    }

    public function down()
    {
      $this->forge->dropTable('ksp_simpanan_jasa');
    }
}
