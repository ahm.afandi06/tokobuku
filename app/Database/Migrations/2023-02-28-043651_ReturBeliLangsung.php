<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ReturBeliLangsung extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'id_pembelian_detail' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'no_faktur_retur' => [
                'type' => 'VARCHAR',
                'null' => true,
                'constraint' => 255
            ],
            'total_pengembalian' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'gudangku' => [
                'type' => 'INT',
                'null' => true
            ],
            'qty' => [
                'type' => 'INT',
                'null' => true
            ],
            'keterangan' => [
                'type' => 'VARCHAR',
                'null' => true,
                'constraint' => 255
            ],
            'tanggal' => [
                'type' => 'DATE',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);

        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('retur_beli_langsung');
    }

    public function down()
    {
        $this->forge->dropTable('retur_beli_langsung');
    }
}
