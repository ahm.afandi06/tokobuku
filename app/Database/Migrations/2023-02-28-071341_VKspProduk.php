<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class VKspProduk extends Migration
{
  private $nmview = 'v_ksp_produk';

  public function up()
  {
    $headnya = "create ALGORITHM = UNDEFINED DEFINER =
    ".$this->db->username."@".$this->db->hostname."
    SQL SECURITY DEFINER VIEW `".$this->nmview."` AS ";

    $this->db->query($headnya."SELECT
        `ksp_simpanan`.`id` AS `id`,
        `ksp_simpanan`.`jenis` AS `jenis`,
        'S' AS `produk`,
        `ksp_simpanan`.`kode` AS `kode`,
        `ksp_simpanan`.`jumlah` AS `jumlah`,
        '' AS `pola`,
        '' AS `max_pinjaman`,
        '' AS `provisi`,
        '' AS `bayar_tagihan1`,
        '' AS `bayar_admin`,
        '' AS `bayar_asuransi`,
        '' AS `bayar_provisi`
    FROM
        `ksp_simpanan`
    UNION ALL SELECT
        `ksp_pinjaman`.`id` AS `id`,
        `ksp_pinjaman`.`jenis` AS `jenis`,
        'P' AS `produk`,
        `ksp_pinjaman`.`kode` AS `kode`,
        0 AS `jumlah`,
        `ksp_pinjaman`.`pola` AS `pola`,
        `ksp_pinjaman`.`max_pinjaman` AS `max_pinjaman`,
        `ksp_pinjaman`.`provisi` AS `provisi`,
        `ksp_pinjaman`.`bayar_tagihan1` AS `bayar_tagihan1`,
        `ksp_pinjaman`.`bayar_admin` AS `bayar_admin`,
        `ksp_pinjaman`.`bayar_asuransi` AS `bayar_asuransi`,
        `ksp_pinjaman`.`bayar_provisi` AS `bayar_provisi`
    FROM
        `ksp_pinjaman`");
  }

  public function down()
  {
    $this->db->query('drop view if exists '.$this->nmview);
  }
}
