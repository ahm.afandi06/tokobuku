<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class VGetbarangTit extends Migration
{
  private $nmview = 'v_getbarang_tit';

  public function up()
  {
    $headnya = "create ALGORITHM = UNDEFINED DEFINER =
    ".$this->db->username."@".$this->db->hostname."
    SQL SECURITY DEFINER VIEW `".$this->nmview."` AS ";

    $this->db->query($headnya."select `a`.`id` AS `id`,`a`.`barang` AS `idbarang`,`a`.`kd_barang` AS `kodebarang`,`a`.`rak_id` AS `rak_id`,`b`.`nama_rak` AS `nama_rak`,`c`.`namabarang` AS `namabarang`,`a`.`jmlstok` AS `jmlstok`,`e`.`idsatuan` AS `idsatuan`,`e`.`namasatuan` AS `namasatuan`,(select `m_ppn`.`persen` from `m_ppn` where (`m_ppn`.`status_aktif` = 'Y')) AS `ppn_persen`,if((`a`.`status_ppn` = 'Y'),round((((select `m_ppn`.`persen` from `m_ppn` where (`m_ppn`.`status_aktif` = 'Y')) / 100) * `d`.`harga`),0),0) AS `ppn_nominal`,`d`.`harga` AS `harga`,'N' AS `tipe_titipan`,`a`.`milik_gudang` AS `milik_gudang`,0 AS `idpemilikbarang` from ((((`stok_barang` `a` left join `m_rak` `b` on((`b`.`id` = `a`.`rak_id`))) join `m_barang` `c` on((`c`.`idbarang` = `a`.`barang`))) join `m_barang_harga` `d` on(((`d`.`idbarang` = `a`.`barang`) and (`d`.`milik_gudang` = `a`.`milik_gudang`) and (`d`.`idsatuan` = `a`.`satuan`)))) join `m_satuan` `e` on((`e`.`idsatuan` = `a`.`satuan`))) where ((`a`.`status` = 1) and (`a`.`jmlstok` <> 0)) union select `a`.`id` AS `id`,`a`.`barang_titipan` AS `idbarang`,`c`.`kd_barang_titipan` AS `kodebarang`,(select `a`.`rak_id` from (`riwayat_barang_titipan_detail` `a` join `m_rak` `b` on((`b`.`id` = `a`.`rak_id`))) where (`a`.`barang_titipan_id` = `a`.`barang_titipan`) order by `a`.`id` desc limit 1) AS `rak_id`,(select `b`.`nama_rak` from (`riwayat_barang_titipan_detail` `a` join `m_rak` `b` on((`b`.`id` = `a`.`rak_id`))) where (`a`.`barang_titipan_id` = `a`.`barang_titipan`) order by `a`.`id` desc limit 1) AS `nama_rak`,`c`.`nama_barang` AS `nama_barang`,`a`.`jmlstok` AS `jmlstok`,`d`.`idsatuan` AS `idsatuan`,`d`.`namasatuan` AS `namasatuan`,(select `m_ppn`.`persen` from `m_ppn` where (`m_ppn`.`status_aktif` = 'Y')) AS `ppn_persen`,if((`a`.`status_ppn` = 'Y'),round((((select `m_ppn`.`persen` from `m_ppn` where (`m_ppn`.`status_aktif` = 'Y')) / 100) * `a`.`harga_jual`),0),0) AS `ppn_nominal`,`a`.`harga_jual` AS `harga`,'Y' AS `tipe_titipan`,`c`.`titip_digudang` AS `milik_gudang`,`a`.`member_titipan` AS `member_titipan` from (((`stok_barang_titipan` `a` left join `m_rak` `b` on((`b`.`id` = `a`.`rak_id`))) join `m_barang_titipan` `c` on((`c`.`id` = `a`.`barang_titipan`))) join `m_satuan` `d` on((`d`.`idsatuan` = `c`.`satuan`))) where ((`a`.`status` = 1) and (`a`.`jmlstok` <> 0))");
  }

  public function down()
  {
    $this->db->query('drop view if exists '.$this->nmview);
  }
}
