<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MSupplier extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "idsupplier" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "namasupplier" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "kontak" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "nohp" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "npwp" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "pkp" => [
        "type" => "ENUM",
        "constraint" => ['Y','N'],
        "null" => true
      ],
      "telp" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "fax" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "email" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "idbank" => [
        "type" => "BIGINT",
      ],
      "norek" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "status" => [
        "type" => "INT"
      ],
    ]);

    $this->forge->addPrimaryKey("idsupplier");
    $this->forge->createTable("m_supplier");
  }

  public function down()
  {
    $this->forge->dropTable('m_supplier');
  }
}
