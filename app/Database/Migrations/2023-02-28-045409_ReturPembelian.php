<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ReturPembelian extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'tmp_distribusi_id' => [
                'type' => 'BIGINT',
                'unsigned' => true,
                'null' => true
            ],
            'tmp_distribusi_detail_id' => [
                'type' => 'BIGINT',
                'unsigned' => true,
                'null' => true
            ],
            'no_faktur_retur' => [
                'type' => 'VARCHAR',
                'null' => true,
                'constraint' => 255
            ],
            'total_pengembalian' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'ke_gudang' => [
                'type' => 'INT',
                'null' => true
            ],
            'gudang_pengirim' => [
                'type' => 'INT',
                'null' => true
            ],
            'qty' => [
                'type' => 'INT',
                'null' => true
            ],
            'keterangan' => [
                'type' => 'TEXT',
                'null' => true
            ],
            'tanggal_retur' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);

        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('retur_pembelian');
    }

    public function down()
    {
        $this->forge->dropTable('retur_pembelian');
    }
}
