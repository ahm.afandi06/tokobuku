<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CoaRek1 extends Migration
{
    public function up()
    {
      $this->forge->addField([
        "idrek1" => [
          "type" => "INT",
          "auto_increment" => true
        ],
        "koderek1" => [
          "type" => "VARCHAR",
          "constraint" => 10,
          "null" => true
        ],
        "namarek1" => [
          "type" => "VARCHAR",
          "constraint" => 255,
          "null" => true
        ],
        "na" => [
          "type" => "ENUM",
          "constraint" => ['N','Y'],
          "null" => true
        ],
      ]);

      $this->forge->addPrimaryKey("idrek1");
      $this->forge->createTable("coa_rek1");
    }

    public function down()
    {
      $this->forge->dropTable('coa_rek1');
    }
}
