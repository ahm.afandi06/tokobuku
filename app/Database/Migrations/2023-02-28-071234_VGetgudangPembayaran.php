<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class VGetgudangPembayaran extends Migration
{
  private $nmview = 'v_getgudang_pembayaran';

  public function up()
  {
    $headnya = "create ALGORITHM = UNDEFINED DEFINER =
    ".$this->db->username."@".$this->db->hostname."
    SQL SECURITY DEFINER VIEW `".$this->nmview."` AS ";

    $this->db->query($headnya."SELECT DISTINCT
        `a`.`idgudang` AS `idgudang`,
        `a`.`inisial` AS `inisial`,
        `a`.`namagudang` AS `namagudang`,
        (SELECT
                COUNT(`tmp_distribusi`.`id`)
            FROM
                `tmp_distribusi`
            WHERE
                (`tmp_distribusi`.`status_terima` = `c`.`status_terima`)) AS `orderan`,
        (SELECT
                COUNT(`kredit_distribusi`.`id`)
            FROM
                `kredit_distribusi`
            WHERE
                ((`kredit_distribusi`.`ke_gudang` = `a`.`idgudang`)
                    AND (`kredit_distribusi`.`status` = 1))) AS `perlu_validasi`
    FROM
        ((`m_gudang` `a`
        LEFT JOIN `req_po` `b` ON ((`b`.`gudang_req` = `a`.`idgudang`)))
        LEFT JOIN `tmp_distribusi` `c` ON (((`c`.`ke_gudang` = `a`.`idgudang`)
            AND (`c`.`status_terima` = 1))))");
  }

  public function down()
  {
    $this->db->query('drop view if exists '.$this->nmview);
  }
}
