<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MSeks extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "idseks" => [
        "type" => "INT",
        "auto_increment" => true
      ],
      "namaseks" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
    ]);

    $this->forge->addPrimaryKey("idseks");
    $this->forge->createTable("m_seks");
  }

  public function down()
  {
    $this->forge->dropTable('m_seks');
  }
}
