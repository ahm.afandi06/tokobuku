<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MSebagaibarang extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "idsebagaibarang" => [
        "type" => "INT",
        "auto_increment" => true
      ],
      "keterangan" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
    ]);

    $this->forge->addPrimaryKey("idsebagaibarang");
    $this->forge->createTable("m_sebagaibarang");
  }

  public function down()
  {
    $this->forge->dropTable('m_sebagaibarang');
  }
}
