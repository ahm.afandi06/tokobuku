<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class VGetpembayaranDistribusi extends Migration
{
  private $nmview = 'v_getpembayaran_distribusi';

  public function up()
  {
    $headnya = "create ALGORITHM = UNDEFINED DEFINER =
    ".$this->db->username."@".$this->db->hostname."
    SQL SECURITY DEFINER VIEW `".$this->nmview."` AS ";

    $this->db->query($headnya."SELECT
        `td`.`id` AS `tmp_dis_id`,
        `td`.`surat_jalan` AS `surat_jalan`,
        `kd`.`id` AS `id`,
        `kd`.`kredit_sebelumnya` AS `kredit_sebelumnya`,
        `kd`.`kredit_terbayar` AS `kredit_terbayar`,
        `kd`.`kredit_saat_ini` AS `kredit_saat_ini`,
        `kd`.`waktu_bayar` AS `waktu_bayar`,
        `kd`.`keterangan` AS `keterangan`,
        `kd`.`status` AS `status`
    FROM
        (`tmp_distribusi` `td`
        JOIN `kredit_distribusi` `kd` ON ((`kd`.`tmp_distribusi_id` = `td`.`id`)))
    WHERE
        ((`td`.`status_terima` = 1)
            AND (`kd`.`status` >= 1))");
  }

  public function down()
  {
    $this->db->query('drop view if exists '.$this->nmview);
  }
}
