<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ModalKasir extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "id" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "kasir" => [
        "type" => "BIGINT",
      ],
      "uniq_periode" => [
        "type" => "TEXT",
      ],
      "open" => [
        "type" => "ENUM",
        "constraint" => ['Y', 'N'],
        "null" => true
      ],
    ]);

    $this->forge->addPrimaryKey("id");
    $this->forge->createTable("modal_kasir");
  }

  public function down()
  {
    $this->forge->dropTable('modal_kasir');
  }
}
