<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class RiwayatUangTunai extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'kode_uniq' => [
                'type' => 'TEXT',
                'null' => true
            ],
            'sebelumnya' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'masuk' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'keluar' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'saatini' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'keterangan' => [
                'type' => 'TEXT',
                'null' => true
            ],
            'tgl_trans' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);

        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('riwayat_uang_tunai');
    }

    public function down()
    {
        $this->forge->dropTable('riwayat_uang_tunai');
    }
}
