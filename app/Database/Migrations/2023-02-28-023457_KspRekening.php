<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class KspRekening extends Migration
{
    public function up()
    {
      $this->forge->addField([
        "idrekening" => [
          "type" => "INT",
          "auto_increment" => true
        ],
        "norekening" => [
          "type" => "VARCHAR",
          "constraint" => 255,
          "null" => true
        ],
        "jenisproduk" => [
          "type" => "ENUM",
          "constraint" => ['P','S'],
        ],
        "idproduk" => [
          "type" => "INT",
          "null" => true,
        ],
        "idanggota" => [
          "type" => "INT",
          "null" => true,
        ],
        "tanggaldaftar" => [
          "type" => "DATETIME",
          "null" => true,
        ],
        "jumlahpengajuan" => [
          "type" => "BIGINT",
          "null" => true,
        ],
        "jumlahsetujui" => [
          "type" => "BIGINT",
          "null" => true,
        ],
        "lamaangsuran" => [
          "type" => "INT",
          "null" => true,
        ],
        "lamaangsuranbulan" => [
          "type" => "INT",
          "null" => true,
        ],
        "angsuran" => [
          "type" => "BIGINT",
          "null" => true,
        ],
        "jasa" => [
          "type" => "FLOAT",
          "null" => true,
        ],
        "jasarp" => [
          "type" => "BIGINT",
          "null" => true,
        ],
        "asuransi" => [
          "type" => "FLOAT",
          "null" => true,
        ],
        "asuransirp" => [
          "type" => "BIGINT",
          "null" => true,
        ],
        "admin" => [
          "type" => "BIGINT",
          "null" => true,
        ],
        "provisi" => [
          "type" => "INT",
          "null" => true,
        ],
        "provisirp" => [
          "type" => "BIGINT",
          "null" => true,
        ],
        "saldo" => [
          "type" => "BIGINT",
          "null" => true,
        ],
        "status" => [
          "type" => "INT",
          "null" => true,
        ],
        "status_tagihan1" => [
          "type" => "INT",
          "null" => true,
        ],
        "status_admin" => [
          "type" => "INT",
          "null" => true,
        ],
        "status_asuransi" => [
          "type" => "INT",
          "null" => true,
        ],
        "status_provisi" => [
          "type" => "INT",
          "null" => true,
        ],
      ]);

      $this->forge->addPrimaryKey("idrekening");
      $this->forge->createTable("ksp_rekening");
    }

    public function down()
    {
      $this->forge->dropTable('ksp_rekening');
    }
}
