<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class PenjualanMultipayment extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'penjualan' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'provider' => [
                'type' => 'INT',
                'null' => true
            ],
            'jml_saldo_jual' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'jml_saldo_sebelumnya' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'jml_saldo_saat_ini' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'harga_jual' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'potongan_persen' => [
                'type' => 'INT',
                'null' => true
            ],
            'potongan_nominal' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'waktu_jual' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ],
            'gudang' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'ket' => [
                'type' => 'TEXT',
                'null' => true
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('penjualan_multipayment');
    }

    public function down()
    {
        $this->forge->dropTable('penjualan_multipayment');
    }
}
