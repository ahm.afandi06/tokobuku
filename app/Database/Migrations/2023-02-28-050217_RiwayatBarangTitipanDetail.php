<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class RiwayatBarangTitipanDetail extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'riwayat_barang_titipan_id' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'barang_titipan_id' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'qty' => [
                'type' => 'INT',
                'null' => true
            ],
            'harga_jual' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'rak_id' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'tgl_sewa_rak' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'tgl_habis_rak' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'harga_sewa_rak' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'durasi_sewa' => [
                'type' => 'INT',
                'null' => true
            ],
            'komisi' => [
                'type' => 'BIGINT',
                'null' => true
            ], 
            'status_ppn' => [
                'type' => 'ENUM',
                'null' => true,
                'constraint' => ['Y', 'N']
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('riwayat_barang_titipan_detail');
    }

    public function down()
    {
        $this->forge->dropTable('riwayat_barang_titipan_detail');
    }
}
