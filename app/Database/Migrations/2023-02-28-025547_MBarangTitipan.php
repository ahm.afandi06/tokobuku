<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MBarangTitipan extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "id" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "member_titipan" => [
        "type" => "BIGINT",
      ],
      "kd_barang_titipan" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "titip_digudang" => [
        "type" => "BIGINT",
      ],
      "harga_jual" => [
        "type" => "BIGINT",
      ],
      "nama_barang" => [
        "type" => "TEXT",
      ],
      "satuan" => [
        "type" => "INT",
      ],
      "ppn" => [
        "type" => "ENUM",
        "constraint" => ['Y', 'N'],
        "null" => true
      ],
      "status" => [
        "type" => "INT",
      ],
    ]);

    $this->forge->addPrimaryKey("id");
    $this->forge->createTable("m_barang_titipan");
  }

  public function down()
  {
    $this->forge->dropTable('m_barang_titipan');
  }
}
