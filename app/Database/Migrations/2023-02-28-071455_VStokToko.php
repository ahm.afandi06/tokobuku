<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class VStokToko extends Migration
{
  private $nmview = 'v_stok_toko';

  public function up()
  {
    $headnya = "create ALGORITHM = UNDEFINED DEFINER =
    ".$this->db->username."@".$this->db->hostname."
    SQL SECURITY DEFINER VIEW `".$this->nmview."` AS ";

  //   $this->db->query($headnya."SELECT
  //       `a`.`id` AS `id`,
  //       `a`.`barang` AS `idbarang`,
  //       `a`.`kd_barang` AS `kd_barang`,
  //       `b`.`nama_rak` AS `nama_rak`,
  //       `c`.`namabarang` AS `namabarang`,
  //       `a`.`jmlstok` AS `jmlstok`,
  //       `e`.`idsatuan` AS `idsatuan`,
  //       `e`.`namasatuan` AS `namasatuan`,
  //       IF((`a`.`status_ppn` = 'Y'),
  //           ROUND((((SELECT
  //                           `m_ppn`.`persen`
  //                       FROM
  //                           `m_ppn`
  //                       WHERE
  //                           (`m_ppn`.`status_aktif` = 'Y')) / 100) * `d`.`harga`),
  //                   0),
  //           0) AS `ppn_nominal`,
  //       `d`.`harga` AS `harga`,
  //       'N' AS `tipe_titipan`,
  //       `a`.`milik_gudang` AS `milik_gudang`,
  //       0 AS `idpemilikbarang`
  //   FROM
  //       ((((`stok_barang` `a`
  //       LEFT JOIN `m_rak` `b` ON ((`b`.`id` = `a`.`rak_id`)))
  //       JOIN `m_barang` `c` ON ((`c`.`idbarang` = `a`.`barang`)))
  //       JOIN `m_barang_harga` `d` ON (((`d`.`idbarang` = `a`.`barang`)
  //           AND (`d`.`milik_gudang` = `a`.`milik_gudang`))))
  //       JOIN `m_satuan` `e` ON ((`e`.`idsatuan` = `a`.`satuan`)))
  //   WHERE
  //       ((`a`.`status` = 1)
  //           AND (`a`.`jmlstok` <> 0))
  //   UNION SELECT
  //       `a`.`id` AS `id`,
  //       `a`.`barang_titipan` AS `idbarang`,
  //       `c`.`kd_barang_titipan` AS `kd_barang`,
  //       `b`.`nama_rak` AS `nama_rak`,
  //       `c`.`nama_barang` AS `nama_barang`,
  //       `a`.`jmlstok` AS `jmlstok`,
  //       `d`.`idsatuan` AS `idsatuan`,
  //       `d`.`namasatuan` AS `namasatuan`,
  //       IF((`a`.`status_ppn` = 'Y'),
  //           ROUND((((SELECT
  //                           `m_ppn`.`persen`
  //                       FROM
  //                           `m_ppn`
  //                       WHERE
  //                           (`m_ppn`.`status_aktif` = 'Y')) / 100) * `a`.`harga_jual`),
  //                   0),
  //           0) AS `ppn_nominal`,
  //       `a`.`harga_jual` AS `harga`,
  //       'Y' AS `tipe_titipan`,
  //       `c`.`titip_digudang` AS `milik_gudang`,
  //       `a`.`member_titipan` AS `member_titipan`
  //   FROM
  //       (((`stok_barang_titipan` `a`
  //       LEFT JOIN `m_rak` `b` ON ((`b`.`id` = `a`.`rak_id`)))
  //       JOIN `m_barang_titipan` `c` ON ((`c`.`id` = `a`.`barang_titipan`)))
  //       JOIN `m_satuan` `d` ON ((`d`.`idsatuan` = `c`.`satuan`)))
  //   WHERE
  //       ((`a`.`status` = 1)
  //           AND (`a`.`jmlstok` <> 0))");
  // }
    $this->db->query($headnya."SELECT
	`a`.`id` AS `id`,
	`a`.`barang` AS `idbarang`,
	`a`.`kd_barang` AS `kd_barang`,
	`b`.`nama_rak` AS `nama_rak`,
	`c`.`namabarang` AS `namabarang`,
	`c`.`foto` AS `foto`,
	`a`.`jmlstok` AS `jmlstok`,
	`e`.`idsatuan` AS `idsatuan`,
	`e`.`namasatuan` AS `namasatuan`,
IF
	((
			`a`.`status_ppn` = 'Y'
			),
		round(((( SELECT `m_ppn`.`persen` FROM `m_ppn` WHERE ( `m_ppn`.`status_aktif` = 'Y' )) / 100 ) * `d`.`harga` ), 0 ),
		0
	) AS `ppn_nominal`,
	`d`.`harga` AS `harga`,
	'N' AS `tipe_titipan`,
	`a`.`milik_gudang` AS `milik_gudang`,
	0 AS `idpemilikbarang`
FROM
	((((
					`stok_barang` `a`
					LEFT JOIN `m_rak` `b` ON ((
							`b`.`id` = `a`.`rak_id`
						)))
				JOIN `m_barang` `c` ON ((
						`c`.`idbarang` = `a`.`barang`
					)))
			JOIN `m_barang_harga` `d` ON (((
						`d`.`idbarang` = `a`.`barang`
						)
				AND ( `d`.`milik_gudang` = `a`.`milik_gudang` ))))
		JOIN `m_satuan` `e` ON ((
				`e`.`idsatuan` = `a`.`satuan`
			)))
WHERE
	((
			`a`.`status` = 1
			)
	AND ( `a`.`jmlstok` <> 0 )) UNION
SELECT
	`a`.`id` AS `id`,
	`a`.`barang_titipan` AS `idbarang`,
	`c`.`kd_barang_titipan` AS `kd_barang`,
	`b`.`nama_rak` AS `nama_rak`,
	`c`.`nama_barang` AS `nama_barang`,
	'foto/noimage.jpg' AS `foto`,
	`a`.`jmlstok` AS `jmlstok`,
	`d`.`idsatuan` AS `idsatuan`,
	`d`.`namasatuan` AS `namasatuan`,
IF
	((
			`a`.`status_ppn` = 'Y'
			),
		round(((( SELECT `m_ppn`.`persen` FROM `m_ppn` WHERE ( `m_ppn`.`status_aktif` = 'Y' )) / 100 ) * `a`.`harga_jual` ), 0 ),
		0
	) AS `ppn_nominal`,
	`a`.`harga_jual` AS `harga`,
	'Y' AS `tipe_titipan`,
	`c`.`titip_digudang` AS `milik_gudang`,
	`a`.`member_titipan` AS `member_titipan`
FROM
	(((
				`stok_barang_titipan` `a`
				LEFT JOIN `m_rak` `b` ON ((
						`b`.`id` = `a`.`rak_id`
					)))
			JOIN `m_barang_titipan` `c` ON ((
					`c`.`id` = `a`.`barang_titipan`
				)))
		JOIN `m_satuan` `d` ON ((
				`d`.`idsatuan` = `c`.`satuan`
			)))
WHERE
	((
			`a`.`status` = 1
		)
	AND ( `a`.`jmlstok` <> 0 ))");
  }

  public function down()
  {
    $this->db->query('drop view if exists '.$this->nmview);
  }
}
