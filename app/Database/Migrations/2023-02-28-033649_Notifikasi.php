<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Notifikasi extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'id_user_gudang' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'ke_user_gudang' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'baca' => [
                'type' => 'INT',
                'null' => true
            ],
            'path' => [
                'type' => 'VARCHAR',
                'null' => true,
                'constraint' => 255
            ],
            'id_reqpo' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'id_retur' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'keterangan' => [
                'type' => 'VARCHAR',
                'null' => true,
                'constraint' => 255
            ],
            'waktu' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);

        $this->forge->addPrimaryKey("id");
        $this->forge->createTable("notifikasi");
    }

    public function down()
    {
        $this->forge->dropTable('notifikasi');
    }
}
