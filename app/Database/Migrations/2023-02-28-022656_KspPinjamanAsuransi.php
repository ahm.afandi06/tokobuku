<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class KspPinjamanAsuransi extends Migration
{
    public function up()
    {
      $this->forge->addField([
        "id" => [
          "type" => "INT",
          "auto_increment" => true
        ],
        "id_pinjaman" => [
          "type" => "INT",
          "null" => true,
        ],
        "asuransi" => [
          "type" => "FLOAT",
          "null" => true,
        ],
        "lama" => [
          "type" => "BIGINT",
          "null" => true,
        ],
        "waktu" => [
          "type" => "VARCHAR",
          "constraint" => 255,
          "null" => true
        ],
      ]);

      $this->forge->addPrimaryKey("id");
      $this->forge->createTable("ksp_pinjaman_asuransi");
    }

    public function down()
    {
      $this->forge->dropTable('ksp_pinjaman_asuransi');
    }
}
