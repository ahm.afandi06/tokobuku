<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class PembelianDetail extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'pembelian' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'barang' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'qty_req' => [
                'type' => 'INT',
                'null' => true
            ],
            'qty_terima' => [
                'type' => 'INT',
                'null' => true
            ],
            'satuan' => [
                'type' => 'INT',
                'null' => true
            ],
            'harga_satuan' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'harga_beli' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'ppn_persen' => [
                'type' => 'INT',
                'null' => true
            ],
            'ppn_nominal' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);

        $this->forge->addPrimaryKey('id');
        $this->forge->createTable("pembelian_detail");
    }

    public function down()
    {
        $this->forge->dropTable("pembelian_detail");
    }
}
