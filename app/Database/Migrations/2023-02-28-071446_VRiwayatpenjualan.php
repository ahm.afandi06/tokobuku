<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class VRiwayatpenjualan extends Migration
{
  private $nmview = 'v_riwayatpenjualan';

  public function up()
  {
    $headnya = "create ALGORITHM = UNDEFINED DEFINER =
    ".$this->db->username."@".$this->db->hostname."
    SQL SECURITY DEFINER VIEW `".$this->nmview."` AS ";

    $this->db->query($headnya."SELECT
        `p`.`id` AS `id`,
        `p`.`no_faktur` AS `no_faktur`,
        `p`.`gudang` AS `gudang`,
        `p`.`no_anggota` AS `no_anggota`,
        `p`.`diskon_persen` AS `diskon_persen`,
        `p`.`diskon_nominal` AS `diskon_nominal`,
        `p`.`total` AS `total`,
        `p`.`tunai_bayar` AS `tunai_bayar`,
        `p`.`saldo_bayar` AS `saldo_bayar`,
        `p`.`sisa_bayar` AS `sisa_bayar`,
        `p`.`waktu_jual` AS `waktu_jual`,
        `p`.`kasir` AS `kasir`,
        `p`.`status_lunas` AS `status_lunas`,
        `p`.`status` AS `status`,
        `p`.`kode_edc` AS `kode_edc`,
        IF((`p`.`no_anggota` <> ''),
            `a`.`nama`,
            '-') AS `namaanggota`,
        IF((`p`.`no_faktur` = '-'),
            `p`.`total`,
            (SELECT
                    (SUM((`d`.`harga_promo` * `d`.`qty`)) + SUM((`d`.`ppn_nominal` * `d`.`qty`)))
                FROM
                    `penjualan_detail` `d`
                WHERE
                    (`d`.`penjualan` = `p`.`id`))) AS `totharga`
    FROM
        (`penjualan` `p`
        LEFT JOIN `anggota` `a` ON ((`a`.`nokartu` = `p`.`no_anggota`)))
    WHERE
        (`p`.`status` = 1)
    ORDER BY `p`.`waktu_jual` DESC");
  }

  public function down()
  {
    $this->db->query('drop view if exists '.$this->nmview);
  }
}
