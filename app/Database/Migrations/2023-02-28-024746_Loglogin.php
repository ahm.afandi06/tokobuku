<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Loglogin extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "id" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "ip" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "waktu" => [
        "type" => "TEXT",
      ],
      "perangkat" => [
        "type" => "TEXT",
      ],
      "user_id" => [
        "type" => "BIGINT",
      ],
    ]);

    $this->forge->addPrimaryKey("id");
    $this->forge->createTable("loglogin");
  }

  public function down()
  {
    $this->forge->dropTable('loglogin');
  }
}
