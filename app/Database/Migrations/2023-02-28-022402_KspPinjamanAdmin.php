<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class KspPinjamanAdmin extends Migration
{
    public function up()
    {
      $this->forge->addField([
        "id" => [
          "type" => "INT",
          "auto_increment" => true
        ],
        "id_pinjaman" => [
          "type" => "INT",
          "null" => true,
        ],
        "biaya" => [
          "type" => "BIGINT",
          "null" => true,
        ],
        "jumlah" => [
          "type" => "BIGINT",
          "null" => true,
        ],
        "sampai" => [
          "type" => "BIGINT",
          "null" => true,
        ],
      ]);

      $this->forge->addPrimaryKey("id");
      $this->forge->createTable("ksp_pinjaman_admin");
    }

    public function down()
    {
      $this->forge->dropTable('ksp_pinjaman_admin');
    }
}
