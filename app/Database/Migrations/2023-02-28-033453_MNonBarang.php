<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MNonBarang extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "id" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "milik_gudang" => [
        "type" => "BIGINT",
      ],
      "keterangan" => [
        "type" => "TEXT",
      ],
      "status" => [
        "type" => "INT",
      ],
    ]);

    $this->forge->addPrimaryKey("id");
    $this->forge->createTable("m_non_barang");
  }

  public function down()
  {
    $this->forge->dropTable('m_non_barang');
  }
}
