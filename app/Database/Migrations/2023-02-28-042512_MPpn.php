<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MPpn extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "id" => [
        "type" => "INT",
        "auto_increment" => true
      ],
      "persen" => [
        "type" => "INT",
      ],
      "status_aktif" => [
        "type" => "ENUM",
        "constraint" => ['Y', 'N'],
        "null" => true
      ],
    ]);

    $this->forge->addPrimaryKey("id");
    $this->forge->createTable("m_ppn");
  }

  public function down()
  {
    $this->forge->dropTable('m_ppn');
  }
}
