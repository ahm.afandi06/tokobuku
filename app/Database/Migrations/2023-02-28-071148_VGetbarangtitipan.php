<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class VGetbarangtitipan extends Migration
{
  private $nmview = 'v_getbarangtitipan';

  public function up()
  {
    $headnya = "create ALGORITHM = UNDEFINED DEFINER =
    ".$this->db->username."@".$this->db->hostname."
    SQL SECURITY DEFINER VIEW `".$this->nmview."` AS ";

    $this->db->query($headnya."SELECT
        `a`.`id` AS `value`,
        `a`.`member_titipan` AS `member_titipan`,
        `a`.`kd_barang_titipan` AS `kd_barang_titipan`,
        `a`.`nama_barang` AS `label`,
        IF((`a`.`ppn` = 'N'),
            0,
            (SELECT
                    `m_ppn`.`persen`
                FROM
                    `m_ppn`
                WHERE
                    (`m_ppn`.`status_aktif` = `a`.`ppn`))) AS `ppn`,
        `a`.`satuan` AS `satuan`,
        `s`.`namasatuan` AS `namasatuan`,
        `a`.`harga_jual` AS `harga_jual`,
        `b`.`jmlstok` AS `sisastok`
    FROM
        ((`m_barang_titipan` `a`
        JOIN `m_satuan` `s` ON ((`s`.`idsatuan` = `a`.`satuan`)))
        JOIN `stok_barang_titipan` `b` ON ((`b`.`barang_titipan` = `a`.`id`)))
    WHERE
        (`a`.`status` = 1)");
  }

  public function down()
  {
    $this->db->query('drop view if exists '.$this->nmview);
  }
}
