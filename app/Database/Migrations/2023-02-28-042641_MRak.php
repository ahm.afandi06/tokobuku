<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MRak extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "id" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "milik_gudang" => [
        "type" => "BIGINT",
        'null' => true
      ],
      "nama_rak" => [
        "type" => "TEXT",
        'null' => true
      ],
      "harga_sewa_anggota" => [
        "type" => "BIGINT",
        'null' => true
      ],
      "harga_sewa_umum" => [
        "type" => "BIGINT",
        'null' => true
      ],
      "status" => [
        "type" => "INT",
      ],
    ]);

    $this->forge->addPrimaryKey("id");
    $this->forge->createTable("m_rak");
  }

  public function down()
  {
    $this->forge->dropTable('m_rak');
  }
}
