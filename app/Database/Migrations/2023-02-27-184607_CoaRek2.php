<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CoaRek2 extends Migration
{
    public function up()
    {
      $this->forge->addField([
        "idrek2" => [
          "type" => "INT",
          "auto_increment" => true
        ],
        "koderek2" => [
          "type" => "VARCHAR",
          "constraint" => 10,
          "null" => true
        ],
        "namarek2" => [
          "type" => "VARCHAR",
          "constraint" => 255,
          "null" => true
        ],
        "na" => [
          "type" => "ENUM",
          "constraint" => ['N','Y'],
          "null" => true
        ],
        "idrek1" => [
          "type" => "INT",
        ],
      ]);

      $this->forge->addPrimaryKey("idrek2");
      $this->forge->createTable("coa_rek2");
    }

    public function down()
    {
      $this->forge->dropTable('coa_rek2');
    }
}
