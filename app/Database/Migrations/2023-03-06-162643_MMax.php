<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MMax extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "id" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "limit" => [
        "type" => "INT",
        "null" => true
      ],
      "mlg" => [
        "type" => "INT",
        "null" => true
      ],
      "kredit" => [
        "type" => "INT",
        "null" => true
      ]
    ]);

    $this->forge->addPrimaryKey("id");
    $this->forge->createTable("m_max");
  }

  public function down()
  {
    $this->forge->dropTable('m_max');
  }
}
