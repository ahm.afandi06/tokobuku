<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ZMenudetail extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "id" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "menu_id" => [
        "type" => "BIGINT",
      ],
      "level" => [
        "type" => "INT",
      ],
      "status" => [
        "type" => "INT",
      ],
    ]);

    $this->forge->addPrimaryKey("id");
    $this->forge->createTable("z_menudetail");
  }

  public function down()
  {
    $this->forge->dropTable('z_menudetail');
  }
}
