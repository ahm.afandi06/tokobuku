<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class TbJurnal extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "id_jurnal" => [
        "type" => "INT",
        "auto_increment" => true
      ],
      "no_bukti" => [
        "type" => "VARCHAR",
        "constraint" => 30,
        "null" => true
      ],
      "tanggal" => [
        "type" => "DATE",
        "null" => true
      ],
      "debet" => [
        "type" => "VARCHAR",
        "constraint" => 6,
        "null" => true
      ],
      "kredit" => [
        "type" => "VARCHAR",
        "constraint" => 6,
        "null" => true
      ],
      "uraian" => [
        "type" => "TEXT",
        "null" => true
      ],
      "keterangan" => [
        "type" => "TEXT",
        "null" => true
      ],
      "jumlah" => [
        "type" => "BIGINT",
        "null" => true
      ],
      "jenis_identitas" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "id_identitas" => [
        "type" => "VARCHAR",
        "constraint" => 50,
        "null" => true
      ],
      "tgl_input" => [
        "type" => "DATETIME",
        "null" => true
      ],
      "user_input" => [
        "type" => "VARCHAR",
        "constraint" => 100,
        "null" => true
      ],
      "idrek1" => [
        "type" => "INT",
        "null" => true
      ],
      "kdrek1" => [
        "type" => "INT",
        "null" => true
      ],
      "idrek2" => [
        "type" => "INT",
        "null" => true
      ],
      "kdrek2" => [
        "type" => "INT",
        "null" => true
      ],
      "idrek3" => [
        "type" => "INT",
        "null" => true
      ],
      "kdrek3" => [
        "type" => "INT",
        "null" => true
      ],
      "idrek4" => [
        "type" => "INT",
        "null" => true
      ],
      "kdrek4" => [
        "type" => "INT",
        "null" => true
      ],
      "namarek1" => [
        "type" => "VARCHAR",
        "constraint" => 200,
        "null" => true
      ],
      "namarek2" => [
        "type" => "VARCHAR",
        "constraint" => 200,
        "null" => true
      ],
      "namarek3" => [
        "type" => "VARCHAR",
        "constraint" => 200,
        "null" => true
      ],
      "namarek4" => [
        "type" => "VARCHAR",
        "constraint" => 200,
        "null" => true
      ],
      "id_jurnalumum" => [
        "type" => "VARCHAR",
        "constraint" => 200,
        "null" => true
      ],
      "kode_transaksi" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
    ]);

    $this->forge->addPrimaryKey("id_jurnal");
    $this->forge->createTable("tb_jurnal");
  }

  public function down()
  {
    $this->forge->dropTable('tb_jurnal');
  }
}
