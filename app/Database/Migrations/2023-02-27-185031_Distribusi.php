<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Distribusi extends Migration
{
    public function up()
    {
      $this->forge->addField([
        "id" => [
          "type" => "BIGINT",
          "auto_increment" => true
        ],
        "no_req_po" => [
          "type" => "VARCHAR",
          "constraint" => 45,
          "null" => true
        ],
        "no_faktur_terima" => [
          "type" => "VARCHAR",
          "constraint" => 45,
          "null" => true
        ],
        "gudang_pengirim" => [
          "type" => "BIGINT",
        ],
        "ke_gudang" => [
          "type" => "BIGINT",
        ],
        "ppn_kirim_persen" => [
          "type" => "INT",
        ],
        "ppn_kirim_nominal" => [
          "type" => "BIGINT",
        ],
        "total_harga" => [
          "type" => "BIGINT",
        ],
        "total_kredit" => [
          "type" => "BIGINT",
        ],
        "waktu_distribusi" => [
          "type" => "DATETIME",
        ],
        "waktu_terima" => [
          "type" => "DATETIME",
        ],
        "waktu_jatuh_tempo" => [
          "type" => "DATETIME",
        ],
        "waktu_lunas" => [
          "type" => "DATETIME",
        ],
        "status_lunas" => [
          "type" => "ENUM",
          "constraint" => ['Y', 'N'],
          "null" => true
        ],
        "status" => [
          "type" => "INT",
        ],
      ]);

      $this->forge->addPrimaryKey("id");
      $this->forge->createTable("distribusi");
    }

    public function down()
    {
      $this->forge->dropTable('distribusi');
    }
}
