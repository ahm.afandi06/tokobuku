<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MBarang extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "idbarang" => [
        "type" => "BIGINT",
        "auto_increment" => true
      ],
      "idkategori" => [
        "type" => "BIGINT",
        'null' => true
      ],
      "kodebarang" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "namabarang" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "foto" => [
        "type" => "TEXT",
        "null" => true
      ],
      "status" => [
        "type" => "INT",
      ],
      "gudang" => [
        "type" => "BIGINT",
        'null' => true
      ],
    ]);

    $this->forge->addPrimaryKey("idbarang");
    $this->forge->createTable("m_barang");
  }

  public function down()
  {
    $this->forge->dropTable('m_barang');
  }
}
