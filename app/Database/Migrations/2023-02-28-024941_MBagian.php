<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MBagian extends Migration
{
  public function up()
  {
    $this->forge->addField([
      "idbagian" => [
        "type" => "INT",
        "auto_increment" => true
      ],
      "kdbagian" => [
        "type" => "VARCHAR",
        "constraint" => 45,
        "null" => true
      ],
      "keterangan" => [
        "type" => "VARCHAR",
        "constraint" => 255,
        "null" => true
      ],
      "status" => [
        "type" => "INT",
      ],
    ]);

    $this->forge->addPrimaryKey("idbagian");
    $this->forge->createTable("m_bagian");
  }

  public function down()
  {
    $this->forge->dropTable('m_bagian');
  }
}
