<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class DistribusiPenjualaan extends Migration
{
    public function up()
    {
      $this->forge->addField([
        "id" => [
          "type" => "BIGINT",
          "auto_increment" => true
        ],
        "no_po" => [
          "type" => "BIGINT",
        ],
        "no_faktur" => [
          "type" => "VARCHAR",
          "constraint" => 255,
          "null" => true
        ],
        "penjualan" => [
          "type" => "BIGINT",
        ],
        "waktu_distribusi" => [
          "type" => "DATE",
        ],
        "waktu_penerimaan" => [
          "type" => "DATE",
        ],
        "status" => [
          "type" => "INT",
        ],
      ]);

      $this->forge->addPrimaryKey("id");
      $this->forge->createTable("distribusi_penjualaan");
    }

    public function down()
    {
      $this->forge->dropTable('distribusi_penjualaan');
    }
}
