<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Setakuntansi extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'keuangan_id' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'coa_id' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'posisi' => [
                'type' => 'ENUM',
                'null' => true,
                'constraint' => ['D', 'K']
            ],
            'relasi' => [
                'type' => 'VARCHAR',
                'null' => true,
                'constraint' => 45
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('setakuntansi');
    }

    public function down()
    {
        $this->forge->dropTable('setakuntansi');
    }
}
