<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Penjualan extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'auto_increment' => true
            ],
            'no_faktur' => [
                'type' => 'VARCHAR',
                'null' => true,
                'constraint' => 45
            ],
            'gudang' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'no_anggota' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true
            ],
            'diskon_persen' => [
                'type' => 'INT',
                'null' => true
            ],
            'diskon_nominal' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'total' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'tunai_bayar' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'saldo_bayar' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'sisa_bayar' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'waktu_jual' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'kasir' => [
                'type' => 'BIGINT',
                'null' => true
            ],
            'status_lunas' => [
                'type' => 'ENUM',
                'null' => true,
                'constraint' => ['Y', 'N']
            ],
            'status' => [
                'type' => 'INT',
                'null' => true
            ],
            'kode_edc' => [
                'type' => 'VARCHAR',
                'null' => true,
                'constraint' => 255
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('penjualan');
    }

    public function down()
    {
        $this->forge->dropTable('penjualan');
    }
}
