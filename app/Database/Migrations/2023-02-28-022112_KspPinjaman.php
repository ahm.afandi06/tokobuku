<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class KspPinjaman extends Migration
{
    public function up()
    {
      $this->forge->addField([
        "id" => [
          "type" => "INT",
          "auto_increment" => true
        ],
        "jenis" => [
          "type" => "VARCHAR",
          "constraint" => 255,
          "null" => true,
        ],
        "kode" => [
          "type" => "VARCHAR",
          "constraint" => 255,
          "null" => true,
        ],
        "pola" => [
          "type" => "INT",
          "null" => true,
        ],
        "max_pinjaman" => [
          "type" => "INT",
          "null" => true,
        ],
        "max_angsuran" => [
          "type" => "INT",
          "null" => true,
        ],
        "keterangan_angsuran" => [
          "type" => "VARCHAR",
          "constraint" => 255,
          "null" => true,
        ],
        "provisi" => [
          "type" => "INT",
          "null" => true,
        ],
        "bayar_tagihan1" => [
          "type" => "INT",
          "null" => true,
        ],
        "bayar_admin" => [
          "type" => "INT",
          "null" => true,
        ],
        "bayar_asuransi" => [
          "type" => "INT",
          "null" => true,
        ],
        "bayar_provisi" => [
          "type" => "INT",
          "null" => true,
        ],
      ]);

      $this->forge->addPrimaryKey("id");
      $this->forge->createTable("ksp_pinjaman");
    }

    public function down()
    {
      $this->forge->dropTable('ksp_pinjaman');
    }
}
