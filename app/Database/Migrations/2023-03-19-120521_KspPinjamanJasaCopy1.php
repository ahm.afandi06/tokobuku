<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class KspPinjamanJasaCopy1 extends Migration
{
    public function up()
    {
        $this->forge->addField([
            "id" => [
                "type" => "INT",
                "auto_increment" => true
            ],
            "id_pinjaman" => [
                "type" => "INT",
                "null" => true,
            ],
            "jasa" => [
                "type" => "FLOAT",
                "null" => true,
            ],
            "lama" => [
                "type" => "INT",
                "null" => true,
            ],
            "waktu" => [
                "type" => "VARCHAR",
                "constraint" => 255,
                "null" => true
            ],
        ]);

        $this->forge->addPrimaryKey("id");
        $this->forge->createTable("ksp_pinjaman_jasa_copy1");
    }

    public function down()
    {
        $this->forge->dropTable('ksp_pinjaman_jasa_copy1');
    }
}
