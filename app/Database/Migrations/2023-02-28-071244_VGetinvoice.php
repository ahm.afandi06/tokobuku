<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class VGetinvoice extends Migration
{
  private $nmview = 'v_getinvoice';

  public function up()
  {
    $headnya = "create ALGORITHM = UNDEFINED DEFINER =
    ".$this->db->username."@".$this->db->hostname."
    SQL SECURITY DEFINER VIEW `".$this->nmview."` AS ";

    $this->db->query($headnya."SELECT
        `i`.`id` AS `id`,
        `i`.`nomor_invoice` AS `nomor_invoice`,
        `i`.`tanggal_buat` AS `tanggal_buat`,
        `i`.`status` AS `status`,
        `i`.`sub_total` AS `sub_total`,
        `i`.`ppn` AS `ppn`,
        `i`.`total` AS `total`,
        `i`.`gudang_id` AS `gudang_id`,
        `g1`.`namagudang` AS `gudang_pengirim`,
        `g1`.`inisial` AS `inisial_a`,
        `d`.`ke_gudang` AS `ke_gudang`,
        `g2`.`namagudang` AS `gudang_penerima`,
        `g2`.`inisial` AS `inisial_b`,
        `td`.`id` AS `id_tmp_dis`,
        `td`.`surat_jalan` AS `surat_jalan`,
        `d`.`total_per_sj` AS `total_kredit`,
        (SELECT
                SUM(`kredit_distribusi`.`kredit_terbayar`)
            FROM
                `kredit_distribusi`
            WHERE
                ((`kredit_distribusi`.`tmp_distribusi_id` = `td`.`id`)
                    AND (`kredit_distribusi`.`status` = 2))) AS `terbayar`,
        `td`.`status_lunas` AS `status_lunas`
    FROM
        ((((`invoice` `i`
        JOIN `invoice_detail` `d` ON ((`d`.`invoice_id` = `i`.`id`)))
        JOIN `tmp_distribusi` `td` ON ((`td`.`id` = `d`.`tmp_distribusi_id`)))
        JOIN `m_gudang` `g1` ON ((`g1`.`idgudang` = `i`.`gudang_id`)))
        JOIN `m_gudang` `g2` ON ((`g2`.`idgudang` = `d`.`ke_gudang`)))
    WHERE
        (`td`.`status_invoice` = 1)
    ORDER BY `i`.`nomor_invoice`");
  }

  public function down()
  {
    $this->db->query('drop view if exists '.$this->nmview);
  }
}
