<?php namespace App\Models;

use CodeIgniter\Model;

class Loglogin extends Model
{
  protected $table = "loglogin";
  protected $DBGroup = 'default';
  protected $primaryKey = 'id';
  protected $useAutoIncrement = true;
  protected $insertID = 0;
  protected $returnType = 'array';
  protected $useSoftDeletes = false;
  protected $protectFields = false;
}
