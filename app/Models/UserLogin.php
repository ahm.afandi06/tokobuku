<?php

namespace App\Models;

use CodeIgniter\Model;

class UserLogin extends Model
{
  protected $DBGroup = 'default';
  protected $table = "user_login";
  protected $primaryKey = 'id';
  protected $useAutoIncrement = true;
  protected $insertID = 0;
  protected $returnType = 'array';
  protected $useSoftDeletes = false;
  protected $protectFields = false;

  public function getDataTambahan($id)
  {
    $q = "select u.foto, u.terdaftar, l.namalevel, l.kode from user_login u join m_level l on l.id = u.level where u.id = '" . $id . "'";
    return $this->db->query($q)
      ->getResultArray()[0];
  }

  public function getUser($id = null)
  {
    if (empty($id)) {
      return $this->table('user_login')
        ->select('user_login.*, mg.kdgudang, mg.namagudang, ml.namalevel, ml.kode')
        ->join('m_gudang mg', 'mg.idgudang=user_login.gudang')
        ->join('m_level ml', 'ml.id=user_login.level')
        ->where('user_login.status = 1')
        ->get()
        ->getResultArray();
    } else {
      return $this->table('user_login')
        ->where('id', $id)
        ->get()
        ->getRowArray();
    }
  }

  public function getLogin($username, $password)
  {
    return $this->table('user_login')
      ->where(['username' => $username, 'password' => $password, 'status_user' => 1])
      ->get()
      ->getRowArray();
  }

  public function simpan($data)
  {
    return $this->db->table($this->table)->insert($data);
  }

  public function ubahdata($data, $id)
  {
    return $this->db->table($this->table)->update($data, ['id' => $id]);
  }

  public function ubah($data, $id)
  {
    return $this->db->table($this->table)->update($data, ['user_id' => $id]);
  }
}
