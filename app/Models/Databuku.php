<?php

namespace App\Models;

use CodeIgniter\Model;

class Databuku extends Model
{
    protected $table = "mst_buku";
    protected $primaryKey = 'id';
    protected $DBGroup = 'default';
    protected $useAutoIncrement = true;
    protected $insertID = 0;
    protected $returnType = 'array';
    protected $useSoftDeletes = false;
    protected $protectFields = false;

    public function getBuku($id = null)
    {
        if (empty($id)) {
            return $this->table('mst_buku')
                ->select('m_barang.*, mk.idkategori, mk.namakategori')
                ->join('m_kategori mk', 'mk.idkategori=m_barang.idkategori')
                ->orderBy('m_barang.idbarang', 'DESC')
                ->get()
                ->getResultArray();
        }
    }

    public function whereBarang($id = null)
    {
        if (!empty($id)) {
            return $this->db->query("SELECT * FROM mst_buku WHERE id=$id ORDER BY id");
        }
    }

    public function simpan($data)
    {
        return $this->db->table($this->table)->insert($data);
    }

    public function ubah($data, $id)
    {
        return $this->db->table($this->table)->update($data, ['id' => $id]);
    }

    public function nonaktif($data, $id)
    {
        return $this->db->table($this->table)->update($data, ['id' => $id]);
    }

    public function aktif($data, $id)
    {
        return $this->db->table($this->table)->update($data, ['id' => $id]);
    }
}
