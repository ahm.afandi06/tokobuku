<?php

namespace App\Models;

use CodeIgniter\Model;

class Kasir extends Model
{
	public function getPenjualanAll()
	{
		$query = "SELECT a.* FROM `penjualan` a";
		return  $this->db->query($query);
	}

	public function getSatuanAll()
	{
		$query = "SELECT * FROM `m_satuan`";
		return  $this->db->query($query);
	}

	public function getCustomerAll($term)
	{
		$query = "select nokartu, nama FROM `anggota` a
			WHERE a.nama LIKE '%" . $term . "%' or a.nik like '%" . $term . "%'";
		return  $this->db->query($query);
	}

	public function cekbykodeanggota($term)
	{
		$query = "select nokartu, nama FROM `anggota` a WHERE a.nokartu='" . $term . "'";
		return  $this->db->query($query);
	}

	public function getPenjualan($no_faktur)
	{
		$query = "SELECT id FROM `penjualan` a WHERE a.no_faktur='$no_faktur'";
		return  $this->db->query($query)->getRow()->id;
	}

	public function getIidPenjualan($no_faktur)
	{
		$query = "SELECT id FROM `penjualan` a WHERE a.no_faktur='$no_faktur'";
		return  $this->db->query($query)->getRow()->id;
	}

	public function getPenjualanbyId($id)
	{
		$query = "SELECT * FROM `penjualan` a WHERE a.id='$id'";
		return  $this->db->query($query);
	}

	public function getPenjualandetailbyIddetail($id)
	{
		$query = "SELECT * FROM `penjualan_detail` a WHERE a.id='$id'";
		return  $this->db->query($query);
	}

	public function getPenjualanDetailbyId($id)
	{
		$query = "SELECT a.*, b.untuk, b.kelas, b.kode, b.judul FROM `penjualan_detail` a LEFT JOIN mst_buku b ON a.barang=b.id WHERE a.penjualan='$id'";
		return  $this->db->query($query);
	}

	/* public function getProdukAll($term)
	{
		$query = "SELECT a.id, a.barang as idbarang, a.kd_barang, c.namabarang, a.jmlstok, e.namasatuan,
			IF(status_ppn = 'Y', ROUND(((SELECT persen FROM m_ppn WHERE status_aktif = 'Y')/100 * d.harga)+d.harga,0), d.harga) as harga,
			'N' as tipe_titipan, a.milik_gudang
			FROM stok_barang a
			JOIN m_barang c ON c.idbarang = a.barang
			JOIN m_barang_harga d ON d.idbarang = a.barang
			JOIN m_satuan e ON e.idsatuan = a.satuan
			WHERE a.status = 1
			UNION ALL
			SELECT a.id, a.barang_titipan as idbarang, c.kd_barang_titipan as kd_barang, c.nama_barang, a.jmlstok, d.namasatuan,
			IF(status_ppn = 'Y', ROUND(((SELECT persen FROM m_ppn WHERE status_aktif = 'Y')/100 * a.harga_jual)+a.harga_jual,0), a.harga_jual) as harga,
			'Y' as tipe_titipan, '' as milik_gudang
			FROM stok_barang_titipan a
			-- JOIN m_rak b ON b.id = a.rak_id
			JOIN m_barang_titipan c ON c.id = a.barang_titipan
			JOIN m_satuan d ON d.idsatuan = c.satuan
			WHERE a.kodebarang LIKE '%" . $term . "%' OR a.namabarang LIKE '%" . $term . "%' AND a.status=1 group by a.kodebarang";
		return  $this->db->query($query);
	} */

	public function getProdukAll($term, $zona, $kelas, $jenis)
	{
		/* $query = "SELECT
				a.*, b.harga, c.namakategori, d.namasatuan
			FROM
				`m_barang` a
				LEFT JOIN m_barang_harga b ON a.idbarang = b.idbarang
				LEFT JOIN m_kategori c ON a.idkategori = c.idkategori
				LEFT JOIN m_satuan d ON b.idsatuan= d.idsatuan
			WHERE (a.kodebarang LIKE '%" . $term . "%' OR a.namabarang LIKE '%" . $term . "%') AND a.status=1 group by a.kodebarang"; */
		return $this->cekbykode($term, $zona, $kelas, $jenis);
		// $query = "SELECT * FROM (
		// 				SELECT a.id, a.barang as idbarang, a.kd_barang as kodebarang, b. nama_rak, c.namabarang, a.jmlstok, e.namasatuan,
		// 				IF(status_ppn = 'Y', ROUND(((SELECT persen FROM m_ppn WHERE status_aktif = 'Y')/100 * d.harga)+d.harga,0), d.harga) as harga, status_ppn,
		// 				'N' as tipe_titipan, a.milik_gudang, (SELECT persen FROM m_ppn WHERE status_aktif = 'Y') as ppn, IF(status_ppn = 'Y', ROUND(((SELECT persen FROM m_ppn WHERE status_aktif = 'Y')/100 * d.harga),0), 0) as ppn_nominal,
		// 				'1' as jenis_sumber_barang
		// 				FROM stok_barang a
		// 				LEFT JOIN m_rak b ON b.id = a.rak_id
		// 				LEFT JOIN m_barang c ON c.idbarang = a.barang
		// 				LEFT JOIN m_barang_harga d ON d.idbarang = a.barang
		// 				LEFT JOIN m_satuan e ON e.idsatuan = a.satuan
		// 				WHERE a.status = 1
		// 				UNION
		// 				SELECT a.id, a.barang_titipan as idbarang, c.kd_barang_titipan as kodebarang, b.nama_rak, c.nama_barang, a.jmlstok, d.namasatuan,
		// 				IF(status_ppn = 'Y', ROUND(((SELECT persen FROM m_ppn WHERE status_aktif = 'Y')/100 * a.harga_jual)+a.harga_jual,0), a.harga_jual) as harga, status_ppn,
		// 				'Y' as tipe_titipan, b.milik_gudang, (SELECT persen FROM m_ppn WHERE status_aktif = 'Y') as ppn, IF(status_ppn = 'Y', ROUND(((SELECT persen FROM m_ppn WHERE status_aktif = 'Y')/100 * a.harga_jual),0), 0) as ppn_nominal,
		// 				'2' as jenis_sumber_barang
		// 				FROM stok_barang_titipan a
		// 				LEFT JOIN m_rak b ON b.id = a.rak_id
		// 				LEFT JOIN m_barang_titipan c ON c.id = a.barang_titipan
		// 				LEFT JOIN m_satuan d ON d.idsatuan = c.satuan
		// 				WHERE a.`status`=1 ) a
		// 				WHERE (a.kodebarang LIKE '%" . $term . "%' OR a.namabarang LIKE '%" . $term . "%') GROUP BY a.kodebarang";
		// return  $this->db->query($query);
	}

	public function cekbykode($term, $zona, $kelas, $jenis)
	{
		$query = "SELECT id, kode, judul, kelas, untuk, hal, $zona as harga, `status` FROM mst_buku WHERE kelas='$kelas' AND untuk='$jenis' AND (kode = '" . $term . "' OR judul LIKE '%" . $term . "%') GROUP BY judul ORDER BY id ASC";
		return  $this->db->query($query);
	}

	public function getHarga($kode, $zona)
	{
		$query = "SELECT id, kode, judul, kelas, untuk, hal, $zona as harga, `status` FROM mst_buku WHERE kode = '" . $kode . "' GROUP BY judul ORDER BY id ASC";
		return  $this->db->query($query);
	}

	/* public function getProdukSatuan($term)
	{
		$query = "SELECT
				a.*, b.harga, c.namakategori, d.namasatuan
			FROM
				`m_barang` a
				LEFT JOIN m_barang_harga b ON a.idbarang = b.idbarang
				LEFT JOIN m_kategori c ON a.idkategori = c.idkategori
				LEFT JOIN m_satuan d ON b.idsatuan= d.idsatuan
			WHERE a.namabarang LIKE '%" . $term . "%' AND a.status=1 group by a.kodebarang";
		return  $this->db->query($query);
	}

	public function ambilStokBarang($id, $sumber, $gudang)
	{
		if ($sumber == "1") {
			$query = "select id, jmlstok from stok_barang where barang = '" . $id . "'";
		} else {
			$query = "select id, jmlstok from stok_barang_titipan where barang_titipan = '" . $id . "' and gudang = '" . $gudang . "'";
		}

		return $this->db->query($query);
	}

	public function ubahstokbarang($stok, $sumber)
	{
		if ($sumber == "1") {
			return $this->db->table('stok_barang')
				->update(['jmlstok' => $stok[1]], ['id' => $stok[0]]);
		} else {
			return $this->db->table('stok_barang_titipan')
				->update(['jmlstok' => $stok[1]], ['id' => $stok[0]]);
		}
	} */

	/* public function insertStokOpname($data, $sumber)
	{
		if ($sumber == "1") {
			return $this->db->table('stokopname')->insert($data);
		} else {
			return $this->db->table('stokopname_titipan')->insert($data);
		}
	} */

	/* public function insertRiwayatBelanjaAnggota($data)
	{
		return $this->db->table('riwayat_belanja_anggota')->insert($data);
	} */

	public function insertPenjualanDetail($data)
	{
		return $this->db->table('penjualan_detail')->insert($data);
	}

	public function insertPenjualan($data)
	{
		return $this->db->table('penjualan')->insert($data);
	}

	public function updatePenjualan($data, $id)
	{
		return $this->db->table('penjualan')->update($data, ['id' => $id]);
	}

	function fetch_jenis($kelas)
	{
		$db = \Config\Database::connect('default', TRUE);
		$query = $db->query("SELECT * FROM mst_buku WHERE kelas='$kelas' GROUP BY untuk ORDER BY id ASC");
		$output = '<option value="">Pilih Jenis</option>';
		foreach ($query->getResult() as $row) {
			$output .= '<option value="' . $row->untuk . '">' . $row->untuk . '</option>';
		}
		return $output;
	}
}
