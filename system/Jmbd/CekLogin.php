<?php
namespace CodeIgniter\Jmbd;

class CekLogin {

  private $view, $data, $opt, $motd;

  public function __construct($view, $data, $opt, $motd){
    $this->view = $view;
    $this->data = $data;
    $this->opt = $opt;
    $this->motd = $motd;
  }

  public function render($motd = 'get', $char = '!'){
    $teks_error = "";
    $view_error = "../../system/Jmbd/error";

    if($this->cek($char) && $this->cek([$motd])){
      return view($this->view, $this->data, $this->opt);
    }else{
      if(!$this->cek([$motd])){
        $teks_error = "405 (Method Not Allowed).";
      }else if(!$this->cek($char)){
        $teks_error = "401 (Unauthorized).";
      }
    }

    // return view($view_error, ['teks_error' => $teks_error]);
    return mks()::to('auth/login');
  }

  public function mod($kelas = '', $char = '!'){
    $teks_error = "";
    $view_error = "../../system/Jmbd/error";
    $arr_kelas = explode("\\", $kelas);
    $mod = $arr_kelas[2];
    $arr_con = explode("Controller", end($arr_kelas));
    $dir = strtolower($arr_con[0]);

    if($this->cek($char)){
      $path = "../Modules/".$mod."/views/".$dir."/";
      $view = $path.$this->view;

      return view($view, $this->data, $this->opt);
    }else{
      $teks_error = "401 (Unauthorized).";
    }

    return view($view_error, ['teks_error' => $teks_error]);
  }

  private function cek($data){
    if(is_array($data)){
      return $this->motd === $data[0];
    }else{
      if($data === '@'){
        return session()->get('mksLogin') === true;
      }
    }

    return true;
  }
}
