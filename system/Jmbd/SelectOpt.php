<?php
namespace CodeIgniter\Jmbd;

class SelectOpt {

  private $arr, $label;
  public $data = [];

  function __construct($arr, $label){
    $this->arr = $arr;
    $this->label = $label;
  }

  public function pisahData(){
    $kode = date('YmdHis')."xxx";

    foreach($this->arr as $i => $a){
      if($a[$this->label] != $kode){
        $kode = $a[$this->label];
        $this->data[$i]['label'] = $kode;
        $this->grupData($kode, $i);
      }
    }
    return $this->data;
  }

  private function grupData($kode, $i){
    foreach($this->arr as $a){
      if($a[$this->label] == $kode){
        $this->data[$i]['data'][] = $a;
      }
    }
  }
}
