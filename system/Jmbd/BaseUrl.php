<?php
namespace CodeIgniter\Jmbd;

class BaseUrl {

  private $prot, $host, $path;

  function __construct($path){
    if (
        isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' ||
        $_SERVER['HTTPS'] == 1) ||
        isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
        $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'
      )
    {
      $this->prot = 'https://';
    }else {
      $this->prot = 'http://';
    }

    $this->host = $_SERVER['HTTP_HOST'];
    $this->path = $path;
  }

  public function get(){
    $path = $this->path !== '/' ? '/'.$this->path : $this->path;
    $url = $this->prot . $this->host . $path;
    
    return $url;
  }
}
