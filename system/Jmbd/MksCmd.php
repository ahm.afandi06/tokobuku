<?php
namespace CodeIgniter\Jmbd;

use CodeIgniter\CLI\BaseCommand;
use CodeIgniter\CLI\CLI;

class MksCmd extends BaseCommand {

  protected $group       = 'MKS Tools';
  protected $name        = 'mks:initdb';
  protected $description = 'Preparing initial data for pemi app.';

  public function run(array $params){
    if (PHP_OS == "WINNT") {
      self::motd();
      CLI::write('[!] Please Wait... [!]');
      exec('timeout 3');
      CLI::write('[-] Cleaning All Tables... [-]');
      exec('cd .. && php spark migrate:refresh');
      CLI::write('[+] Migrating Data... [+]');
      exec('timeout 3');
      CLI::write('[+] Seeding Data... [+]');
      exec('cd .. && php spark db:seed DatabaseSeeder');
      exec('timeout 1');
      echo "\n";
    }else{
      self::motd();
      CLI::write('[!] Please Wait... [!]');
      exec('sleep 3');
      CLI::write('[-] Cleaning All Tables... [-]');
      exec('cd .. && php spark migrate:refresh');
      CLI::write('[+] Migrating Data... [+]');
      exec('sleep 3');
      CLI::write('[+] Seeding Data... [+]');
      exec('cd .. && php spark db:seed DatabaseSeeder');
      exec('sleep 1');
      echo "\n";
    }
  }

  private static function motd(){
    CLI::clearScreen();
    CLI::write("===============================================");
    CLI::write("|                                             |");
    CLI::write("|           - Initial Data v1.0.0 -           |");
    CLI::write("|        Powered by Multi Karya Solusi        |");
    CLI::write("|                                             |");
    CLI::write("===============================================");
    CLI::write("\n");
  }
}
