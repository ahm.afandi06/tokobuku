<?php

namespace CodeIgniter\Jmbd;

class Keuangan extends \App\Controllers\BaseController
{

    private $db, $mks, $sesi;
    protected $request;
    private $listStatic = [
        'Penjualan Barang (Tunai)', 'Penjualan Barang (Kredit)',
    ];

    function __construct($arr)
    {
        $this->sesi = \Config\Services::session();
        extract($arr); // ekstrak array menjadi variabel

        // pindah alih fungsi BaseController
        // $this(Keuangan) = $coreMks = $this(BaseController)
        $this->db = $coreMks->db;
        $this->mks = $coreMks->mks;
        $this->request = $coreMks->request;
        // ************** Batas Pindah Alih **************

        // cek jenis kontroler submit berdasarkan model data
        // dd($this->request->getPost());
        $cekAwal = $this->cekArray($data);

        // jalankan fungsi sesuai hasil cekAwal
        call_user_func([$this, $cekAwal->fungsi]);
    }

    // kasus: Dinamis
    private function dinamisFungsi()
    {
        //
    }

    // kasus: Penjualan Barang (Tunai)
    private function mks_348426562b8719da1900196c755b2688()
    {
        $c_umum = $this->request->getPost('c_umum');
        $bayar = $this->request->getPost('bayar');
        // $datacoa = $this->getCoa(__FUNCTION__);

        if ($c_umum == "on") { // tunai
            // $this->insertPenjualan($datacoa, 'Penjualan Barang (Tunai)');
        } else {
            if (($bayar == "" || $bayar == "0")) { // kredit
                call_user_func([
                    $this,
                    'mks_fd5ef0ba3c32e8367a4a233db51914f8'
                ]);
            } else { // tunai
                // $this->insertPenjualan($datacoa, 'Penjualan Barang (Tunai)');
            }
        }
    }

    // kasus: Penjualan Barang (Kredit)
    private function mks_fd5ef0ba3c32e8367a4a233db51914f8()
    {
        // $datacoa = $this->getCoa(__FUNCTION__);
        // $this->insertPenjualan($datacoa, 'Penjualan Barang (Kredit)');
    }

    // cek jenis kontroler submit (statik / dinamis)
    // output: (Objek)
    // - kontroler => boolean
    // - fungsi => String (nama fungsi)
    private function cekArray($arr)
    {
        $kondisi = false;
        $fungsi = "dinamisFungsi";

        foreach ($arr as $a) {
            if (in_array($a['nmtrans'], $this->listStatic)) { // jika statik
                $kondisi = true;
                $fungsi = 'mks_' . md5($a['nmtrans']);
                break;
            }
        }
        return (object)['kontroler' => $kondisi, 'fungsi' => $fungsi];
    }

    /* private function getCoa($f){
    $q = "select sa.*,
    c4.idrek4, c4.koderek4, c4.namarek4,
    c3.idrek3, c3.koderek3, c3.namarek3,
    c2.idrek2, c2.koderek2, c2.namarek2,
    c1.idrek1, c1.koderek1, c1.namarek1
    from setakuntansi sa
    join m_keuangan k on k.id = sa.keuangan_id
    left join coa_rek4 c4 on c4.idrek4 = sa.coa_id
    left join coa_rek3 c3 on c3.idrek3 = c4.idrek3
    left join coa_rek2 c2 on c2.idrek2 = c3.idrek2
    left join coa_rek1 c1 on c1.idrek1 = c2.idrek1
    where k.fungsi = '".$f."' and k.status = 1 and sa.status = 1
    order by sa.relasi, sa.posisi";

    $qq = "select sa.relasi from setakuntansi sa
    join m_keuangan k on k.id = sa.keuangan_id
    where k.fungsi = '".$f."'
    group by sa.relasi";
    $relasi = $this->db->query($qq)->getResultArray();
    $tmp = $this->db->query($q)->getResultArray();

    $data = [];

    foreach($relasi as $k1 => $d1){
      foreach($tmp as $k => $d){
        if($d1['relasi'] == $d['relasi']){
          $data[$d['relasi']][] = $d;
        }
      }
    }
    return $data;
  } */

    // get hpp
    private function getHpp()
    {
        $items = $this->request->getPost('items');
        $hpp = 0;
        foreach ($items as $d) {
            if ($d['jenis_sumber_barang'] == "1") {
                $sql = "select harga_pokok from stok_barang where kd_barang = '" . $d['kode_barang'] . "' and milik_gudang = '" . session('gudang') . "' and status = 1";
                $hp = (int)$this->db->query($sql)->getRow()->harga_pokok;
                $hpp += $hp * (int)$d['qty'];
            }
        }
        return $hpp;
    }

    // insert data penjualan
    private function insertPenjualan($datacoa, $ket)
    {
        $m = new \App\Models\TbJurnal;
        $x = "21";
        $y = "42";
        $jenisiden = "ANGGOTA";
        $iden = $this->request->getPost('id_customer');
        if ($this->request->getPost('c_umum') == "on") {
            $jenisiden = "UMUM";
            $iden = 0;
        }
        foreach ($datacoa as $k => $da) {
            foreach ($datacoa[$k] as $d) {
                $tottrans = str_replace(".", "", $this->request->getPost('totaltransaksi'));
                $jml = ($d['idrek4'] == $x || $d['idrek4'] == $y) ? $this->getHpp() : $tottrans;
                $m->insert([
                    'no_bukti' => $this->request->getPost('no_faktur'),
                    'tanggal' => date('Y-m-d H:i:s'),
                    'debet' => $d['posisi'] == 'D' ? $d['idrek4'] : 0,
                    'kredit' => $d['posisi'] == 'K' ? $d['idrek4'] : 0,
                    'keterangan' => $ket,
                    'jumlah' => $jml,
                    'jenis_identitas' => $jenisiden,
                    'id_identitas' => $iden,
                    'tgl_input' => date('Y-m-d H:i:s'),
                    'user_input' => session('id'),
                    'idrek1' => $d['idrek1'],
                    'kdrek1' => $d['koderek1'],
                    'idrek2' => $d['idrek2'],
                    'kdrek2' => $d['koderek2'],
                    'idrek3' => $d['idrek3'],
                    'kdrek3' => $d['koderek3'],
                    'idrek4' => $d['idrek4'],
                    'kdrek4' => $d['koderek4'],
                    'namarek1' => $d['namarek1'],
                    'namarek2' => $d['namarek2'],
                    'namarek3' => $d['namarek3'],
                    'namarek4' => $d['namarek4'],
                    'id_jurnalumum' => $k,
                ]);
            }
        }
    }

    /* private function setData($arrCoa){
    extract($arrCoa)
    return [
      'no_bukti' => $no_bukti,
      'tanggal' => $tanggal,
      'debet' => $debet,
      'kredit' => $$kredit,
      'keterangan' => $ket,
      'jumlah' => $jml,
      'jenis_identitas' => $jenisiden,
      'id_identitas' => $iden,
      'tgl_input' => $tgl_input,
      'user_input' => $user_input,
      'idrek1' => $idrek1,
      'kdrek1' => $kdrek1,
      'idrek2' => $idrek2,
      'kdrek2' => $kdrek2,
      'idrek3' => $idrek3,
      'kdrek3' => $kdrek3,
      'idrek4' => $idrek4,
      'kdrek4' => $kdrek4,
      'namarek1' => $namarek1,
      'namarek2' => $namarek2,
      'namarek3' => $namarek3,
      'namarek4' => $namarek4,
      'id_jurnalumum' => $id_jurnalumum,
    ];
  } */
}
