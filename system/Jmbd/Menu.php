<?php

namespace CodeIgniter\Jmbd;

class Menu
{

    private $lvl, $db;

    function __construct()
    {
        $this->lvl = session('level');
        $this->db = \Config\Database::connect();
        // dd(uri_string());
    }

    public function parent()
    {
        $sql = "select * from v_menuparent where level = '" . $this->lvl . "'";
        return $this->db->query($sql)->getResultArray();
    }

    public function sub($parent)
    {
        $sql = "select * from v_menusub where level = '" . $this->lvl . "' and parent = '" . $parent . "'";
        return $this->db->query($sql)->getResultArray();
    }

    public function child($parent)
    {
        $sql = "select * from v_menuchild where level = '" . $this->lvl . "' and parent = '" . $parent . "'";
        return $this->db->query($sql)->getResultArray();
    }

    public function cekRoute($data, $route, $akhir = null)
    {
        $arr = explode("|", $data ?? "");
        $hasil = false;
        foreach ($arr as $a) {
            if ($akhir !== null) {
                $rtx = explode("/", $route);
                if (count($rtx) >= 2 && str_contains($route, $a)) {
                    $hasil = true;
                } else {
                    if ($a == $route) {
                        $hasil = true;
                    }
                }
            } else {
                $strx = explode("/", $a);
                $rtx = explode("/", $route);
                if (count($strx) >= 2 && count($rtx) >= 2 && end($strx) == "^") {
                    if (str_contains($route, substr($a, 0, -1))) {
                        $hasil = true;
                    }
                } else {
                    if ($a == $route) {
                        $hasil = true;
                    }
                }
            }
        }
        return $hasil;
    }
}
