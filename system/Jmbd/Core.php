<?php

namespace CodeIgniter\Jmbd;

/**
 * MKS Tools | By Jember Dev
 */

class Core
{

    public static $motd;
    public static $isPost, $isGet;
    private static $myUri;

    public function __construct($motd)
    {
        self::$motd = strtolower($motd);
        self::setRequestMethod(self::$motd);
        self::$myUri = uri_string();
    }

    public static function menu()
    {
        return new Menu;
    }

    private static function setRequestMethod($m)
    {
        self::$isPost = $m === "post";
        self::$isGet = $m === "get";
    }

    public static function view($view, array $data = [], array $opt = [])
    {
        return new CekLogin($view, $data, $opt, self::$motd);
    }

    public static function base($p = '/')
    {
        $base = new BaseUrl($p);
        return $base->get();
    }

    public static function NameSpace($uri)
    {
        $path = explode("/", $uri);
        $con = ucfirst($path[0]);

        return "\\App\\Modules\\" . $con . "\\Controllers";
    }

    public static function tglIndo($tanggal)
    {
        return date('d F Y', strtotime($tanggal));
    }

    public static function tglIndo2($tanggal)
    {
        $tgl = substr($tanggal, 8, 2);
        $bln = substr($tanggal, 5, 2);
        $thn = substr($tanggal, 0, 4);
        return $tgl . '/' . $bln . '/' . $thn;
    }

    public static function rupiah($angka, $kode = true)
    {
        $rp = $kode ? "Rp " : "";
        if (isset($angka)) {
            $hasil_rupiah = $rp . number_format($angka, 2, ',', '.');
            return substr($hasil_rupiah, 0, -3);
        }
    }

    public static function to($p = '/')
    {
        return redirect()->to($p);
    }

    public static function selectOpt($arr, $kode)
    {
        $so = new SelectOpt($arr, $kode);
        return $so->pisahData();
    }

    /* public static function initKeuangan($mks){
    $sql = "select k.*, s.controller
    from m_keuangan k
    join z_submit s on s.id = k.submit_id
    where k.status = 1 and s.controller = '".self::$myUri."'";
    $data = $mks->db->query($sql)->getResultArray();
    if(count($data) > 0){
      new Keuangan(['data' => $data, 'coreMks' => $mks]);
    }
  } */

    public function tes()
    {
        return "coba";
    }

    public static function initCoa($apa)
    {
        $coa = new Coa(["data" => $apa]);
    }
    function numberToRoman($number)
    {
        $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
        $returnValue = '';
        while ($number > 0) {
            foreach ($map as $roman => $int) {
                if ($number >= $int) {
                    $number -= $int;
                    $returnValue .= $roman;
                    break;
                }
            }
        }
        return $returnValue;
    }

    function penyebut($nilai)
    {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " " . $huruf[$nilai];
        } else if ($nilai < 20) {
            $temp = $this->penyebut($nilai - 10) . " belas";
        } else if ($nilai < 100) {
            $temp = $this->penyebut($nilai / 10) . " puluh" . $this->penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " seratus" . $this->penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = $this->penyebut($nilai / 100) . " ratus" . $this->penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . $this->penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = $this->penyebut($nilai / 1000) . " ribu" . $this->penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = $this->penyebut($nilai / 1000000) . " juta" . $this->penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = $this->penyebut($nilai / 1000000000) . " milyar" . $this->penyebut(fmod($nilai, 1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = $this->penyebut($nilai / 1000000000000) . " trilyun" . $this->penyebut(fmod($nilai, 1000000000000));
        }
        return $temp;
    }

    function terbilang($nilai)
    {
        if ($nilai < 0) {
            $hasil = "minus " . trim($this->penyebut($nilai));
        } else {
            $hasil = trim($this->penyebut($nilai));
        }
        return $hasil;
    }
}
